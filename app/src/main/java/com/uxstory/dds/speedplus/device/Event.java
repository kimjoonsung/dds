package com.uxstory.dds.speedplus.device;

import com.uxstory.dds.speedplus.message.type.EventType;

public class Event {

    private String Uri;
    private int bootIndex;
    private int workStep;
    private String type;
    private String time;

    /**
     * WorkInfo
     */
    private String patientName;
    private String block;
    private String restoration;
    private String leftBurName;
    private String rightBurName;

    /**
     * workFinish
     */
    private String millingStartTime;
    private String elapsedTime;

    private Event preEvent;

    public Event getPreEvent() {
        return preEvent;
    }

    public void setPreEvent(Event preEvent) {
        if(preEvent != null) {
            preEvent.setPreEvent(null);
        }
        this.preEvent = preEvent;
    }

    public EventType getEventType() {
        return EventType.getType(type);
    }

    public int getBootIndex() {
        return bootIndex;
    }

    public void setBootIndex(int bootIndex) {
        this.bootIndex = bootIndex;
    }

    public int getWorkStep() {
        return workStep;
    }

    public void setWorkStep(int workStep) {
        this.workStep = workStep;
    }

    public String getUri() {
        return Uri;
    }

    public void setUri(String uri) {
        Uri = uri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getRestoration() {
        return restoration;
    }

    public void setRestoration(String restoration) {
        this.restoration = restoration;
    }

    public String getLeftBurName() {
        return leftBurName;
    }

    public void setLeftBurName(String leftBurName) {
        this.leftBurName = leftBurName;
    }

    public String getRightBurName() {
        return rightBurName;
    }

    public void setRightBurName(String rightBurName) {
        this.rightBurName = rightBurName;
    }

    public String getMillingStartTime() {
        return millingStartTime;
    }

    public void setMillingStartTime(String millingStartTime) {
        this.millingStartTime = millingStartTime;
    }
}
