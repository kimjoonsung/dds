package com.uxstory.dds.speedplus.notification;

import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.notification.type.CustomEventType;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;

public class Notification {

    private long notiRowId;
    private Device device;
    private String title;
    private boolean backNoti;
    private boolean popNoti;
    private boolean toastNoti;
    private boolean recordNoti;
    private int msgResId;
    private int iconResId;
    private int iconToastId;
    private String time;
    private String serial;
    private String message;
    private String notificationTypeString;
    private NotificationKindType notificationKindType;

    private EventType eventType;
    private CustomEventType customEventType;
    private AppNotificationType appNotificationType;

    public Notification(Device device, Event event) {
        eventType = event.getEventType();
        if(device != null) {
            setDevice(device);
            setTitle(device.getName());
        }
        setBackNoti(eventType.isBackNoti());
        setPopNoti(eventType.isPopNoti());
        setToastNoti(eventType.isToastNoti());
        setRecordNoti(eventType.isRecordNoti());
        setMsgResId(eventType.getMsgResId());
        setIconResId(eventType.getIconResId());
        setIconToastId(eventType.getToastIconResId());
        setNotificationTypeString(eventType.getString());
        setNotificationKindType(NotificationKindType.EVENT);
        setTime(event.getTime());
    }

    public Notification(Device device, CustomEventType customEventType) {
        this.customEventType = customEventType;
        if(device != null) {
            setDevice(device);
            setTitle(device.getName());
        }
        setBackNoti(customEventType.isBackNoti());
        setPopNoti(customEventType.isPopNoti());
        setToastNoti(customEventType.isToastNoti());
        setRecordNoti(customEventType.isRecordNoti());
        setMsgResId(customEventType.getMsgResId());
        setIconResId(customEventType.getIconResId());
        setIconToastId(customEventType.getToastIconResId());
        setNotificationTypeString(customEventType.getString());
        setNotificationKindType(NotificationKindType.CUSTOM_EVENT);
        //time is db now()
    }

    public Notification(Device device, AppNotificationType appNotificationType) {
        this.appNotificationType = appNotificationType;
        if(device != null) {
            setDevice(device);
            setTitle(device.getName());
        }
        setBackNoti(appNotificationType.isBackNoti());
        setPopNoti(appNotificationType.isPopNoti());
        setToastNoti(appNotificationType.isToastNoti());
        setRecordNoti(appNotificationType.isRecordNoti());
        setMsgResId(appNotificationType.getMsgResId());
        setIconResId(appNotificationType.getIconResId());
        setIconToastId(appNotificationType.getToastIconResId());
        setNotificationTypeString(appNotificationType.getString());
        setNotificationKindType(NotificationKindType.APP);
        //time is db now()
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public CustomEventType getCustomEventType() {
        return customEventType;
    }

    public void setCustomEventType(CustomEventType customEventType) {
        this.customEventType = customEventType;
    }

    public AppNotificationType getAppNotificationType() {
        return appNotificationType;
    }

    public void setAppNotificationType(AppNotificationType appNotificationType) {
        this.appNotificationType = appNotificationType;
    }

    public long getNotiRowId() {
        return notiRowId;
    }

    public void setNotiRowId(long notiRowId) {
        this.notiRowId = notiRowId;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBackNoti() {
        return backNoti;
    }

    public void setBackNoti(boolean backNoti) {
        this.backNoti = backNoti;
    }

    public boolean isPopNoti() {
        return popNoti;
    }

    public void setPopNoti(boolean popNoti) {
        this.popNoti = popNoti;
    }

    public boolean isToastNoti() {
        return toastNoti;
    }

    public void setToastNoti(boolean toastNoti) {
        this.toastNoti = toastNoti;
    }

    public boolean isRecordNoti() {
        return recordNoti;
    }

    public void setRecordNoti(boolean recordNoti) {
        this.recordNoti = recordNoti;
    }

    public int getMsgResId() {
        return msgResId;
    }

    public void setMsgResId(int msgResId) {
        this.msgResId = msgResId;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public int getIconToastId() {
        return iconToastId;
    }

    public void setIconToastId(int iconToastId) {
        this.iconToastId = iconToastId;
    }

    public String getNotificationTypeString() {
        return notificationTypeString;
    }

    public void setNotificationTypeString(String notificationTypeString) {
        this.notificationTypeString = notificationTypeString;
    }

    public NotificationKindType getNotificationKindType() {
        return notificationKindType;
    }

    public void setNotificationKindType(NotificationKindType notificationKindType) {
        this.notificationKindType = notificationKindType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
