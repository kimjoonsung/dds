package com.uxstory.dds.speedplus.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LockScreenNotificationActivity extends AbstractBaseActivity {

    private final static String TAG = LockScreenNotificationActivity.class.getSimpleName();

    @BindView(R.id.cardview_layout)
    View layout;

    @BindView(R.id.icon_type)
    ImageView iconType;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.txt_msg)
    TextView txtMsg;


    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;

    Handler handler = new Handler();

    Runnable runnable = () -> finish();

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen_notification);
        ButterKnife.bind(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "speedPlusCareApp:lockScreenNotification");
//        wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "speedPlusCareApp:lockScreenNotification");
    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire(5000); // WakeLock 깨우기
        handler.postDelayed(runnable, 5000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        processIntent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        if(wakeLock != null) {
            wakeLock.release();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void processIntent() {
        Intent intent = getIntent();
        LogU.d(TAG, "processIntent() intent=" + intent);
        if(intent != null) {
            String action = intent.getAction();
//            String action = Constants.INTENT_ACTION_GOTO_BUR_STATE;
            LogU.d(TAG, "processIntent() action=" + action);

            String serial = intent.getStringExtra("serial");
//            String serial = "xxxxx-1234";

            String title = intent.getStringExtra("title");
            txtTitle.setText(title);
            txtTitle.requestLayout();

            String msg = intent.getStringExtra("msg");
            txtMsg.setText(msg);
            txtMsg.requestLayout();

            int iconId = intent.getIntExtra("iconId", 0);
            if(iconId > 0) {
                iconType.setImageResource(iconId);
            }

            layout.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    Intent notificationIntent = new Intent(LockScreenNotificationActivity.this, MainActivity.class);
                    notificationIntent.setAction(action);
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    notificationIntent.putExtra("serial", serial);
                    startActivity(notificationIntent);
                    finish();
                }
            });

            layout.invalidate();
            layout.requestLayout();
        }
        setIntent(null);
    }
}
