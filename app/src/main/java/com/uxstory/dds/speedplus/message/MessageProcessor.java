package com.uxstory.dds.speedplus.message;

import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.message.type.RequestType;
import com.uxstory.dds.speedplus.message.type.WaterSensingType;
import com.uxstory.dds.speedplus.message.type.ResponseType;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.device.MillingProgress;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.message.type.XmlTagType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;

public class MessageProcessor {

    final static String TAG = MessageProcessor.class.getSimpleName();

    public static void setWorkStateFromHistory(Device device) {
        ArrayList<Event> eventList = DbQuery.selectLastEventList(device.getSerial());

        if (eventList != null && eventList.size() > 0) {
            LogU.d(TAG, "eventHistory size: " + eventList.size());
            for(Event event : eventList) {
                device.setLastEvent(event);
                device.setWorkStateType(WorkStateType.getType(device.getLastEvent().getType()));
            }
        }
    }

    public static CommunicatorEvent processMessage(Device device, String xml) {

        ResponseType responseType = ResponseType.NULL;
        String serial = device.getSerial();

        XmlTagType xmlTagType = MessageParser.getXmlTagType(xml);
        Event event = null;

        if(xmlTagType == XmlTagType.EVENT) {
            event = MessageParser.getEvent(xml);
            if (event != null) {
                if(EventChecker.isMillingEndEvent(event)) {
                    event.setPatientName(device.getPatientName());
                    event.setBlock(device.getBlock());
                    event.setRestoration(device.getRestoration());
                    event.setLeftBurName(device.getLeftBurName());
                    event.setRightBurName(device.getRightBurName());
                    event.setMillingStartTime(device.getMillingStartTime());
                }
                DbQuery.insertEvent(serial, event, false);
                responseType = ResponseType.EVENT;
            }

        } else if(xmlTagType == XmlTagType.MILLING_PROGRESS) {
            MillingProgress millingProgress = MessageParser.getMillingProgress(xml);
            if (millingProgress != null) {
                device.setMillingProgress(millingProgress);
                responseType = ResponseType.MILLING_PROGRESS;
            }

        } else if(xmlTagType == XmlTagType.COMMAND_RESPONSE) {
            responseType = MessageParser.getResponseType(xml);

            if (responseType != ResponseType.NULL) {
                if (ResponseType.SYNC_EVENT_HISTORY == responseType) {
                    String ok = MessageParser.getResponseResult(responseType, xml);
                    /**
                     * 정상일때 ok 오지 않음.
                     */
                    if (ok == null || XmlAttr.VALUE_OK.equals(ok)) {
                        MessageParser.parseInsertEventHistory(device, xml);
                        LogU.d(TAG, "eventHistory parsing end");

                    } else {
                        responseType = ResponseType.SYNC_EVENT_HISTORY_ERROR;
                    }

                } else if (ResponseType.WATER_SENSING == responseType) {
                    String ok = MessageParser.getResponseResult(responseType, xml);
                    if (XmlAttr.VALUE_OK.equals(ok)) {
                        String waterSensing = MessageParser.getValue(RequestType.WATER_SENSING.getString(), XmlAttr.STATE, xml);
                        device.setWaterSensingType(WaterSensingType.getType(waterSensing));

                    } else {
                        device.setWaterSensingType(WaterSensingType.NOPE);
                    }

                } else if (ResponseType.BUR_INFO == responseType) {
                    /**
                     * BurInfo는 통신하지 않고 자체 카운트 함.
                     */
//                    BurInfo burInfo = MessageParser.getBurInfo(xml);
//                    if (burInfo != null) {
//                        device.setLeftBurName(burInfo.getLeftBurName());
//                        device.setRightBurName(burInfo.getRightBurName());
//                        device.setLeftBurUsingCount(burInfo.getLeftBurUsingCount());
//                        device.setRightBurUsingCount(burInfo.getRightBurUsingCount());
//                    }

                } else if (ResponseType.MILLING_PROGRESS == responseType) {
                    MillingProgress millingProgress = MessageParser.getMillingProgress(xml);
                    if (millingProgress != null) {
                        device.setMillingProgress(millingProgress);
                    }

                } else if (ResponseType.UNPAIRING == responseType) {
                    String ok = MessageParser.getResponseResult(responseType, xml);
                    if (XmlAttr.VALUE_OK.equals(ok)) {
                        device.setSession(null);
                        device.setEncryptKey(null);
                        DbQuery.updateDevice(device, false);

                    } else {
                        responseType = ResponseType.UNPAIRING_ERROR;
                    }
                }
            }
        }

        return new CommunicatorEvent(responseType, device, event);
    }
}
