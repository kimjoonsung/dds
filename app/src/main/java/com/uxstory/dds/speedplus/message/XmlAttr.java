package com.uxstory.dds.speedplus.message;

public class XmlAttr {

    public static final String URI = "uri";

    public static final String PRODUCT_URI = "productURI";

    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String RESPONSE = "response";
    public static final String TIME = "time";
    public static final String DATE = "date";

    public static final String STATE = "state";

    public static final String WORK_INFO = "workInfo";
    public static final String PATIENT_NAME = "patientName";
    public static final String BLOCK = "block";
    public static final String RESTORATION = "restoration";
    public static final String LEFT_BUR_NAME = "leftBurName";
    public static final String RIGHT_BUR_NAME = "rightBurName";
    public static final String LEFT_BUR_USING_COUNT = "leftBurUsingCount";
    public static final String RIGHT_BUR_USING_COUNT = "rightBurUsingCount";

    public static final String PERCENT = "percent";
    public static final String REMAINING_TIME = "remainingTime";

    public static final String WORK_FINISH = "workFinish";
    public static final String ELAPSED_TIME = "elapsedTime";

    public static final String VALUE_OK = "ok";
}
