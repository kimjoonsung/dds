package com.uxstory.dds.speedplus.message.type;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.notification.type.ErrorType;
import com.uxstory.dds.speedplus.config.Constants;

public enum EventType {

    NULL(""                                                 , false, false, false, false, null, -1),

    READY("ready"                                           , false, false, false, false, null,0),

    WARM_UP("warm_up"                                       , true, false, true, true, null, R.string.noti_warm_up),
    PUMP("pump"                                             , true, false, true, true, null, R.string.noti_pump),
    CHANGE_BUR_AND_BLOCK("change_bur_and_block"             , true, false, true, false, null, R.string.noti_change_bur_and_block),
    CALIBRATION("calibration"                               , false, false, false, false, null, -1),
    INSERT_BUR_AND_BLOCK("insert_bur_and_block"             , true, true, false, true, null, R.string.noti_insert_bur_and_block),
    CHANGE_BUR("change_bur"                                 , false, true, false, true, Constants.INTENT_ACTION_GOTO_BUR_STATE, R.string.noti_change_bur),
    SENSING("sensing"                                       , true, false, true, true, null, R.string.noti_sensing),
    MILLING_START("milling_start"                           , true, false, true, true, null, R.string.noti_milling_start),
    MILLING_CONTINUE("milling_continue"                     , true, false, true, true, null, R.string.noti_milling_continue),
    MILLING_RESTART("milling_restart"                       , true, false, true, true, null, R.string.noti_milling_restart),
    MILLING_FINISH("milling_finish"                         , true, false, true, true, null, R.string.noti_milling_finish),
    MILLING_STOP("milling_stop"                             , true, false, true, true, null, R.string.noti_milling_stop),
    PAUSE("pause"                                           , true, false, true, true, null, R.string.noti_pause),
    PAUSE_PUMP("pause_pump"                                 , true, false, true, true, null, R.string.noti_pause_pump),
    PAUSE_CHANGE_BUR("pause_change_bur"                     , false, true, false, true, Constants.INTENT_ACTION_GOTO_BUR_STATE, R.string.noti_pause_change_bur),
    PAUSE_SENSING("pause_sensing"                           , false, false, false, false, null, -1),
    CAM_RECONNECTED("cam_reconnected"                       , false, false, false, false, null, -1),

    STOP("stop"                                             , true, false, true, true, null, 0),

    CHANGE_LEFT_BUR("change_left_bur"                       , false, false, false, false, null, -1),
    CHANGE_RIGHT_BUR("change_right_bur"                     , false, false, false, false, null, -1),
    CHANGE_BOTH_BUR("change_both_bur"                       , false, false, false, false, null, -1),

    DOOR_OPEN("door_open"                                   , true, true, false, false, null, R.string.noti_door_open),
    CAM_DISCONNECTED("cam_disconnected"                     , true, true, false, true, null, R.string.noti_cam_disconnected),
    ERR_SENSING("err_sensing"                               , true, true, false, true, null, R.string.noti_err_sensing),
    NOT_WORK("not_work"                                     , true, true, false, true, Constants.INTENT_ACTION_GOTO_REQUEST_HELP, R.string.noti_not_work),
    COLLET_OPEN("collet_open"                               , true, true, false, false, null, R.string.noti_collet_open),
    LEFT_BUR_CHECK("left_bur_check"                         , false, false, false, false, null, -1),
    RIGHT_BUR_CHECK("right_bur_check"                       , false, false, false, false, null, -1),
    BOTH_BUR_CHECK("both_bur_check"                         , false, false, false, false, null, -1),
    LEFT_BUR_TYPE_WARNING("left_bur_type_warning"           , false, false, false, false, null, -1),
    RIGHT_BUR_TYPE_WARNING("right_bur_type_warning"         , false, false, false, false, null, -1),
    BOTH_BUR_TYPE_WARNING("both_bur_type_warning"           , false, false, false, false, null, -1),
    WATER_FLOW_WARNING("waterflow_warning"                  , true, true, false, true, Constants.INTENT_ACTION_GOTO_WATER_STATE, R.string.noti_waterflow_warning),
    ;

    private String value;

    boolean backNoti;
    boolean popNoti;
    boolean toastNoti;
    boolean recordNoti;
    String intentAction;
    int msgResId;

    /**
     *
     * @param value
     * @param backNoti
     * @param popNoti
     * @param toastNoti
     * @param recordNoti
     * @param intentAction
     * @param msgResId (>0 msg, =0 CustomEventType, =-1 NO msg)
     */
    EventType(String value, boolean backNoti, boolean popNoti, boolean toastNoti, boolean recordNoti, String intentAction, int msgResId) {
        this.value = value;
        this.backNoti = backNoti;
        this.popNoti = popNoti;
        this.toastNoti = toastNoti;
        this.recordNoti = recordNoti;
        this.intentAction = intentAction;
        this.msgResId = msgResId;
    }

    public String getString() {
        return value;
    }

    public boolean isBackNoti() {
        return backNoti;
    }

    public boolean isPopNoti() {
        return popNoti;
    }

    public boolean isToastNoti() {
        return toastNoti;
    }

    public boolean isRecordNoti() {
        return recordNoti;
    }

    public int getMsgResId() {
        return msgResId;
    }

    public int getIconResId() {
        if(isError()) {
            return R.drawable.icon_noti_error;

        } else {
            switch (this) {
                case WATER_FLOW_WARNING:
                    return R.drawable.icon_noti_water;
                case PAUSE_CHANGE_BUR:
                case CHANGE_BUR:
                    return R.drawable.icon_noti_bur;
                default:
                    return R.drawable.icon_noti_work;
            }
        }
    }

    public int getToastIconResId() {
        if(isError()) {
            return R.drawable.icon_noti_error_white;

        } else {
            switch (this) {
                case WATER_FLOW_WARNING:
                    return R.drawable.icon_noti_water_white;
                case PAUSE_CHANGE_BUR:
                case CHANGE_BUR:
                    return R.drawable.icon_noti_bur_white;
                default:
                    return R.drawable.icon_noti_work_white;
            }
        }
    }

    public String getIntentAction() {
        return intentAction;
    }

    public static EventType getType(String type) {
        for (EventType eventType : EventType.values()) {
            if (eventType.getString().equals(type)) {
                return eventType;
            }
        }
        return NULL;
    }

    public boolean isError() {
        return ErrorType.getType(getString()) != ErrorType.NULL;
    }
}
