package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.SettingDeviceRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.message.type.EventHistorySyncType;

import java.util.ArrayList;

import butterknife.BindView;

public class SettingProductInfoFragment extends AbstractBaseFragment {
    private final static String TAG = SettingProductInfoFragment.class.getSimpleName();

    @BindView(R.id.layout_no_device)
    View layoutNoDevice;

    @BindView(R.id.btn_all_sync)
    Button btnAllSync;

    @BindView(R.id.btn_goto_pairing)
    Button btnGoToPairing;

    @BindView(R.id.txt_version)
    TextView txtVersion;

    @BindView(R.id.recycler_devices)
    RecyclerView recyclerView;

    private ArrayList<Device> deviceList;

    private SettingDeviceRecycleAdapter recyclerAdapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_setting_product);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);

        deviceList = DbQuery.selectDeviceList(true);
//        deviceList = getDdsService().getDeviceList();
        recyclerAdapter = new SettingDeviceRecycleAdapter(getActivity(), deviceList);
        recyclerView.setAdapter(recyclerAdapter);

        btnGoToPairing.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                getMainAcivity().goToMenu(R.id.menu_dash);
            }
        });

        String versionName = "";
        try {
            PackageInfo info = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionInfo = String.format(getContext().getString(R.string.txt_setting_version_info), versionName);
        ViewUtil.highlightTagText(getContext(), txtVersion, versionInfo, R.color.colorBlueLight);
        txtVersion.requestLayout();

        btnAllSync.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                btnAllSync.setEnabled(false);
                recyclerAdapter.setAllSync(true);

                for(Device device: getDdsService().getDeviceList()) {
                    if(device.isConnected()) {
                        device.setEventHistorySyncType(EventHistorySyncType.READY);
                    }
                }

                nextSync();
            }
        });
    }

    private void nextSync() {
        for(Device device: getDdsService().getDeviceList()) {
            if(device.getEventHistorySyncType() == EventHistorySyncType.READY) {
                if(GlobalApplication.getDdsService().requestHistory(device)) {
                    recyclerAdapter.notifyDataSetChanged();
                    return;

                } else {
                    device.setEventHistorySyncType(EventHistorySyncType.FAILED);
                }
            }
        }

        recyclerAdapter.setAllSync(false);
        recyclerAdapter.notifyDataSetChanged();
        btnAllSync.setEnabled(true);
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(R.string.txt_setting_product_info, null);

        setLayout(recyclerAdapter.getItemCount());

        CommunicatorEvent communicatorEvent = getCommunicatorEvent();
        if(communicatorEvent != null && communicatorEvent.getDevice() != null) {
            int position = 0;
            SettingDeviceRecycleAdapter.ViewHolder holder;
            for(Device d : deviceList) {
                if(d.getSerial().equals(communicatorEvent.getDevice().getSerial())) {
                    holder = (SettingDeviceRecycleAdapter.ViewHolder)recyclerView.findViewHolderForLayoutPosition(position);
                    if(holder != null) {
                        holder.responseType = communicatorEvent.getResponseType();
                    }
                    break;
                }
                position++;
            }
        }

        if(recyclerAdapter.isAllSync()) {
            switch (communicatorEvent.getResponseType()) {
                case SYNC_EVENT_HISTORY:
                case SYNC_EVENT_HISTORY_ERROR:
                    nextSync();
                    break;
            }
        }

        recyclerView.getAdapter().notifyDataSetChanged();
    }

    private void setLayout(int deviceListSize) {
        ViewUtil.setVisibility(btnAllSync, deviceListSize > 0);
        ViewUtil.setVisibility(layoutNoDevice, deviceListSize == 0);
    }
}
