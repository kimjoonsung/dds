package com.uxstory.dds.speedplus.message;

import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.message.type.RequestType;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.NoSuchPaddingException;

import static com.uxstory.dds.speedplus.config.Constants.CHARSET_UTF_16LE;
import static com.uxstory.dds.speedplus.config.Constants.CHARSET_UTF_8;
import static com.uxstory.dds.speedplus.config.Constants.DDS_URI_PREFIX;

public class MessageGenerator {

    private final static String TAG = MessageGenerator.class.getSimpleName();

    private final static String DDS_ROOT = "<DDSControl version='0.1' productURI='" + DDS_URI_PREFIX + GlobalApplication.getAndroidDeviceSerial() + "'>\n";

    public static final int HEADER_PACKET_TYPE_SIZE = 4;
    public static final int HEADER_BODY_LENGTH_SIZE = 4;
    public static final int HEADER_SIZE = HEADER_PACKET_TYPE_SIZE + HEADER_BODY_LENGTH_SIZE;

    public static final int PACKET_TYPE_KEEP_ALIVE = 10;
    public static final int PACKET_TYPE_ECHO_KEEP_ALIVE = 20;
    public static final int PACKET_TYPE_XML = 40;
    public static final int PACKET_TYPE_WXML = 60;

    public static byte[] getLittleEndian(int v) {
        byte[] buf = new byte[4];
        buf[3] = (byte) ((v >>> 24) & 0xFF);
        buf[2] = (byte) ((v >>> 16) & 0xFF);
        buf[1] = (byte) ((v >>> 8) & 0xFF);
        buf[0] = (byte) (v & 0xFF);
        return buf;
    }

    public static int getBigEndian(byte[] v) {

        int val = -1;

        try {
            int[] arr = new int[4];
            for (int i = 0; i < 4; i++) {
                arr[i] = v[3 - i] & 0xFF;
            }
            val = ((arr[0] << 24) + (arr[1] << 16) + (arr[2] << 8) + arr[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return val;
    }

    public static Charset getCharset(int packetType) {
        switch (packetType) {
            case PACKET_TYPE_ECHO_KEEP_ALIVE:
            case PACKET_TYPE_XML:
                return Charset.forName(CHARSET_UTF_8);
            case PACKET_TYPE_WXML:
                return Charset.forName(CHARSET_UTF_16LE);
            default:
                return null;
        }
    }

    private static String getRandomKey(int size) {
        SecureRandom random = new SecureRandom();
        StringBuilder stringBuild = new StringBuilder();

        for (int i = 0; i < size; i++) {
            if (random.nextBoolean()) {
                stringBuild.append((char) ((random.nextInt(26)) + 97));
            } else {
                stringBuild.append((random.nextInt(10)));
            }
        }

        return stringBuild.toString();
    }

    public static String generateSession() {
        return getRandomKey(25);
    }

    public static String generateSessionKey(String encryptKey, String session) {
        String sessionKey = null;
        try {
            AESEncryption encryption = new AESEncryption(encryptKey);
            long time = System.currentTimeMillis() / 1000;
            sessionKey = encryption.encrypt(getRandomKey(4) + "_" + session + "_" + time);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return sessionKey;
    }

    public static byte[] generateEchoKeepAlivePacket(String sessionKey) {
        Charset charset = MessageGenerator.getCharset(PACKET_TYPE_XML);
        if(charset != null) {
            byte[] bodyByte = sessionKey.getBytes(charset);
            int bodySize = bodyByte.length;
            ByteBuffer sendByteBuffer = ByteBuffer.allocate(HEADER_SIZE + bodySize);
            sendByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            sendByteBuffer.putInt(PACKET_TYPE_ECHO_KEEP_ALIVE); //packet type
            sendByteBuffer.putInt(bodySize);
            sendByteBuffer.put(bodyByte);
            return sendByteBuffer.array();
        }
        return null;
    }

    public static byte[] generateMessagePacket(String message) {
        Charset charset = MessageGenerator.getCharset(PACKET_TYPE_WXML);
        if(charset != null) {
            byte[] bodyByte = message.getBytes(charset);
            int bodySize = bodyByte.length;
            ByteBuffer sendByteBuffer = ByteBuffer.allocate(HEADER_SIZE + bodySize);
            sendByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            sendByteBuffer.putInt(PACKET_TYPE_WXML); //xml type
            sendByteBuffer.putInt(bodySize);
            sendByteBuffer.put(bodyByte);
            return sendByteBuffer.array();
        }
        return null;
    }

    private final static String REQ_SESSION_MSG = DDS_ROOT + "<commandRequest name='session'/></DDSControl>";

    public static String getRequestSessionMessage() {
        return REQ_SESSION_MSG;
    }

    public static String getTransSessionMessage(String sessionKey) {
        return DDS_ROOT + "<session>\n<key>" + sessionKey + "</key>\n</session>\n</DDSControl>\n";
    }

    public static String getRequestMessage(String sessionKey, RequestType requestType) {
        return DDS_ROOT + "<commandRequest name='" + requestType.getString() + "'>\n" +
                "<session>" + sessionKey + "</session>\n" +
                "</commandRequest>\n" +
                "</DDSControl>\n";
    }
}
