package com.uxstory.dds.speedplus.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.ChangeGuideRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeBurGuideActivity extends AbstractBaseActivity {

    private final static String TAG = ChangeBurGuideActivity.class.getSimpleName();

    @BindView(R.id.view_close)
    View viewClose;

    @BindView(R.id.img_next)
    ImageView imgNext;

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.msg_1)
    TextView msg1;

    @BindView(R.id.msg_2)
    TextView msg2;

    @BindView(R.id.img_navi_dot_1)
    ImageView imgNaviDot1;

    @BindView(R.id.img_navi_dot_2)
    ImageView imgNaviDot2;

    @BindView(R.id.img_navi_dot_3)
    ImageView imgNaviDot3;

    @BindView(R.id.img_navi_dot_4)
    ImageView imgNaviDot4;

    @BindView(R.id.img_navi_dot_5)
    ImageView imgNaviDot5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chage_bur_guide);
        ButterKnife.bind(this);

        int[] imgGuideArray = {R.drawable.img_guide_bur_01, R.drawable.img_guide_bur_02, R.drawable.img_guide_bur_03, R.drawable.img_guide_bur_04, R.drawable.img_guide_bur_05};
        int[] msg1GuideArray = {R.string.txt_bur_change_msg_1, R.string.txt_bur_change_msg_2, R.string.txt_bur_change_msg_2, R.string.txt_bur_change_msg_3, R.string.txt_bur_change_msg_3};
        int[] msg2GuideArray = {R.string.txt_msg_bur_replace_guide_1, R.string.txt_msg_bur_replace_guide_1, R.string.txt_msg_bur_replace_guide_1, R.string.txt_msg_bur_replace_guide_2, R.string.txt_msg_bur_replace_guide_2};

        ImageView[] naviDots = {imgNaviDot1, imgNaviDot2, imgNaviDot3, imgNaviDot4, imgNaviDot5};

//        layoutNaviDots.setVisibility(View.GONE);

        viewClose.setOnClickListener(v -> finish());

        imgNext.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int i = pager.getCurrentItem() + 1;
                i = (i >= imgGuideArray.length) ? imgGuideArray.length - 1 : i;
                pager.setCurrentItem(i, true);
            }
        });

        ChangeGuideRecycleAdapter adapter = new ChangeGuideRecycleAdapter(this, imgGuideArray);
        pager.setAdapter(adapter);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                LogU.d(TAG, "onPageSelected()=" + position);
                ViewUtil.setVisibility(imgNext, position < imgGuideArray.length -1);

                msg1.setText(msg1GuideArray[position]);
                msg1.requestLayout();
                msg2.setText(msg2GuideArray[position]);
                msg2.requestLayout();

                ImageView imgDot = naviDots[position];
                for(ImageView view : naviDots) {
                    view.setSelected(view == imgDot);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
