package com.uxstory.dds.speedplus.activity.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.uxstory.dds.speedplus.R;

import java.util.HashSet;
import java.util.List;

public class CalendarRangeDecorator {


    private Context context;
    private MaterialCalendarView calendarView;

    public CalendarRangeDecorator(Context context, MaterialCalendarView calendarView) {
        this.context = context;
        this.calendarView = calendarView;
    }

    public void setRangeDecorators(List<CalendarDay> dateList) {
        setDecor(dateList.subList(0, 1), R.drawable.shape_circle_blue_half_left);
        if(dateList.size() > 2) {
            setDecor(dateList.subList(1, dateList.size()-1), R.drawable.shape_rect_blue_light);
        }
        setDecor(dateList.subList(dateList.size()-1, dateList.size()), R.drawable.shape_circle_blue_half_right);
    }

    void setDecor(List<CalendarDay> calendarDayList, int drawable) {
        calendarView.addDecorators(new EventDecorator(context, drawable, calendarDayList));
    }

    private class EventDecorator implements DayViewDecorator {
        Context context;
        private int drawable;
        private HashSet<CalendarDay> dates;

        public EventDecorator(Context context, int drawable, List<CalendarDay> calendarDays1) {
            this.context = context;
            this.drawable = drawable;
            this.dates = new HashSet<>(calendarDays1);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            // apply drawable to dayView

            Drawable d = context.getDrawable(drawable);
            view.setSelectionDrawable(d);
            // white text color
            view.addSpan(new ForegroundColorSpan(Color.WHITE));
        }
    }

}
