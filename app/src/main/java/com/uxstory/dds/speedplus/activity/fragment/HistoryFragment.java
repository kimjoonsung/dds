package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.ViewPagerFragmentStateAdapter;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends AbstractBaseFragment {

    private final static String TAG = HistoryFragment.class.getSimpleName();

    @BindView(R.id.txt_milling_history)
    TextView txtMillingHistory;

    @BindView(R.id.txt_milling_history_today)
    TextView txtMillingHistoryToday;

    @BindView(R.id.view_line_1)
    View lineMillingHistoryToday;

    @BindView(R.id.view_line_2)
    View lineMillingHistory;

    @BindView(R.id.pager)
    ViewPager2 pager;


    private FragmentManager fragmentManager;
    private HistoryTodayFragment historyTodayFragment = new HistoryTodayFragment();
    private HistoryStatisticsFragment historyStatisticsFragment = new HistoryStatisticsFragment();

    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_history);
    }

    private void setPager() {

        ViewPagerFragmentStateAdapter viewPagerFragmentStateAdapter = new ViewPagerFragmentStateAdapter(this);
        viewPagerFragmentStateAdapter.setList(fragments);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                toggleMenu();
                if(position == 1) {
                    historyStatisticsFragment.updateView();
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        pager.setAdapter(viewPagerFragmentStateAdapter);
        pager.setUserInputEnabled(false);
//        pager.setPageTransformer(new ZoomOutPageTransformer());
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(0);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragmentManager = getChildFragmentManager();

        fragments.add(historyTodayFragment);
        fragments.add(historyStatisticsFragment);

        txtMillingHistoryToday.setOnClickListener(v -> pager.setCurrentItem(0, true));
        txtMillingHistory.setOnClickListener(v -> pager.setCurrentItem(1, true));

        historyTodayFragment.setDevice(getDevice());
        historyStatisticsFragment.setDevice(getDevice());

        txtMillingHistoryToday.setSelected(false);
        lineMillingHistoryToday.setSelected(false);

        txtMillingHistory.setSelected(true);
        lineMillingHistory.setSelected(true);

        getMainAcivity().updateToolbarSub(0, getDevice());
//        moveHistoryMillingTodayFragment();
        setPager();
    }

    private void toggleMenu() {

        txtMillingHistoryToday.setSelected(!txtMillingHistoryToday.isSelected());
        lineMillingHistoryToday.setSelected(txtMillingHistoryToday.isSelected());

        txtMillingHistory.setSelected(!txtMillingHistory.isSelected());
        lineMillingHistory.setSelected(txtMillingHistory.isSelected());
    }

    private void removeChildFragments() {
        fragmentManager.beginTransaction().remove(historyTodayFragment).remove(historyStatisticsFragment).commitAllowingStateLoss();
    }

    @Override
    public void onDestroyView() {
        LogU.d(TAG, "onDestroyView() activity=" + getActivity() + ", this=" + this);
        removeChildFragments();
        super.onDestroyView();
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(0, getDevice());
    }

}
