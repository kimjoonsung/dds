package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PairingDeviceRecycleAdapter extends RecyclerView.Adapter<PairingDeviceRecycleAdapter.ViewHolder> {

    private final static String TAG = PairingDeviceRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<Device> items;
//    private CommunicatorEventListener communicatorEventListener;

    public PairingDeviceRecycleAdapter(Context context, List<Device> items) {
        this.context = context;
        this.items = items;
//        this.communicatorEventListener = communicatorEventListener;
    }

    public void setList(List<Device> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pairing_device, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        Device device = items.get(position);
        String index = String.valueOf(position + 1);

        if (device.getName() == null) {
//            String num = String.valueOf(pairedDeviceSize + position + 1);
//            String deviceName = context.getString(R.getString.txt_device_name_prefix) + " " + num;
            String deviceName = context.getString(R.string.txt_device_name_prefix) + " (" + device.getSerial() + ")";
            device.setName(deviceName);
        }

        holder.txtProductNum.setText(index);

//        holder.txtDeviceName.setText(device.getName());
        holder.txtDeviceName.setText(context.getString(R.string.txt_device_name_prefix));
        holder.txtSerial.setText(device.getSerial());

//        holder.txtConnectNo;
//        holder.buttonConnect;

        setButton(holder, device);
        holder.buttonConnect.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                for(Device device1 : items) {
                    if(device1.getWorkStateType() == WorkStateType.PAIRING) {
                        return;
                    }
                }
                goToNextStep(holder, position);
            }
        });
    }

    private void goToNextStep(ViewHolder holder, int position) {
        Device device = items.get(position);
        WorkStateType state = device.getWorkStateType();

        switch (state) {
            case ON_ADD:
            case DISCONNECTED:
                startPairing(device);
                break;
            case PAIRING:
            case CONNECTED:
            default:
                break;
        }
        setButton(holder, device);
    }

    private void setButton(ViewHolder holder, Device device) {
        View button = holder.buttonConnect;
        View loading = holder.loading;
        TextView txtConnect = holder.txtConnect;
        TextView txtConnectNo = holder.txtConnectNo;
        switch (device.getWorkStateType()) {
            case ON_ADD:
                button.setSelected(false);
                button.setActivated(false);
                txtConnect.setText(R.string.txt_connect);
                txtConnectNo.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                break;
            case PAIRING:
                button.setSelected(false);
                button.setActivated(true); //blue backgound
                txtConnect.setText(R.string.txt_connecting);
                loading.setVisibility(View.VISIBLE);
                txtConnectNo.setVisibility(View.GONE);
                txtConnect.setTextColor(context.getColor(R.color.colorWhite));
                break;
            case DISCONNECTED:
                button.setSelected(false);
                button.setActivated(true); //blue backgound
                txtConnect.setText(R.string.txt_retrying);
                txtConnect.setTextColor(context.getColor(R.color.colorWhite));
                txtConnectNo.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                break;
            default:
                if (device.isConnected()) {
                    button.setSelected(true);
                    txtConnect.setTextColor(context.getColor(R.color.colorBlue));
                    txtConnect.setText(R.string.txt_connected);
                    txtConnectNo.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                }
                break;
        }
    }

    private synchronized void startPairing(Device device) {
        if(device.getCommunicator() == null) {
            GlobalApplication.getDdsService().requestPairing(device);
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_product_num)
        TextView txtProductNum;

        @BindView(R.id.txt_device_name)
        TextView txtDeviceName;

        @BindView(R.id.txt_serial)
        TextView txtSerial;

        @BindView(R.id.txt_connect_no)
        TextView txtConnectNo;

        @BindView(R.id.button_02)
        View buttonConnect;

        @BindView(R.id.loading)
        View loading;

        @BindView(R.id.txt_connect)
        TextView txtConnect;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
