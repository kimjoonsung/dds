package com.uxstory.dds.speedplus.service;

import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;

import java.util.ArrayList;
import java.util.HashMap;

public interface DdsService {

    void startCommunicator(Device device);

    void loadDeviceList();

    void addDevice(Device device);

    void updateDeviceName(String serial, String name);

    void removeDevice(Device device);

    ArrayList<Device> getDeviceList();

    Device getPairedDevice(String serial);

    void requestPairing(Device device);

    boolean requestUnPairing(Device device);

    boolean requestWaterSensing(Device device);

    boolean requestMillingProgress(Device device);

    boolean requestHistory(Device device);

    boolean requestBurInfo(Device device);

    void checkBurState(Device device, boolean noti);

    void checkWaterTurbidState(Device device, boolean noti);
}
