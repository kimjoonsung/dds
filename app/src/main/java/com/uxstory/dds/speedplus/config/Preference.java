package com.uxstory.dds.speedplus.config;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {

    public static final String PREFER_NAME = "speed_plus_care_preference";
    public static final String PREFER_USE_APP_TIME = "USE_APP_TIME";

    public enum PreferenceType {
        NOTI_ALL(true),
        NOTI_MILLING(true),
        NOTI_BUR(true),
        NOTI_WATER(true),
        NOTI_LONG_NO_USE(true),
        ENTERED_TUTORIAL(false),
        ENTERED_GUIDE(false),
        FIRST_PAIRING(false),
        ;

        private boolean value;

        PreferenceType(boolean value) {
            this.value = value;
        }

        public void setValue(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    public static void loadPreferenceValue(Context context) {
        if(context != null) {
            for(PreferenceType preferenceType : PreferenceType.values()) {
                SharedPreferences pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
                boolean value = pref.getBoolean(preferenceType.name(), preferenceType.getValue());
                preferenceType.setValue(value);
            }
        }
    }

    public static void setPreferenceValue(Context context, PreferenceType preferenceType, boolean value) {
        if(context != null) {
            SharedPreferences pref = context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
            pref.edit().putBoolean(preferenceType.name(), value).apply();
            preferenceType.setValue(value);
        }
    }

    public static boolean getPreferenceValue(PreferenceType preferenceType) {
        return preferenceType.getValue();
    }
}
