package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.notification.type.CustomEventType;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;
import com.uxstory.dds.speedplus.util.DateUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationPopDeviceRecycleAdapter extends RecyclerView.Adapter<NotificationPopDeviceRecycleAdapter.ViewHolder> {

    private final static String TAG = NotificationPopDeviceRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<NotificationModel> items;
    private String terms;

    public NotificationPopDeviceRecycleAdapter(Context context, List<NotificationModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<NotificationModel> items) {
        this.items = items;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noti_pop_device, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        LogU.d(TAG, "onBindViewHolder() position=" + position);

        NotificationModel item = items.get(position);

        if (item.isHeader()) {
            ViewUtil.setVisibility(holder.viewTopBar, true);
            ViewUtil.setVisibility(holder.viewMsg, false);
            ViewUtil.setVisibility(holder.divLine, !item.isSelected() && position < items.size() - 1);

            holder.txtDeviceTitle.setText(item.getName());
            holder.viewTopBar.setSelected(item.isSelected());

            holder.viewTopBar.setOnClickListener(v -> {
                v.setSelected(!v.isSelected());
                item.setSelected(v.isSelected());
                if (v.isSelected()) {
                    ViewUtil.setVisibility(holder.divLine, false);
                    setNotiList(items.indexOf(item));

                } else {
                    items.removeAll(item.getChildrenList());
                    notifyItemRangeRemoved(items.indexOf(item) + 1, item.getChildrenList().size());
                    ViewUtil.setVisibility(holder.divLine, items.indexOf(item) < items.size() - 1);
                }
            });

        } else {
            ViewUtil.setVisibility(holder.viewTopBar, false);
            ViewUtil.setVisibility(holder.viewMsg, true);
            ViewUtil.setVisibility(holder.divLine, false);

            String notificationTypeString = item.getNotificationTypeString();
            NotificationKindType notificationKindType = item.getNotificationKindType();
            if (notificationKindType != null) {
                int iconResId = 0;
                int msgResId = 0;
                switch (notificationKindType) {
                    case EVENT:
                        EventType eventType = EventType.getType(notificationTypeString);
                        iconResId = eventType.getIconResId();
                        msgResId = eventType.getMsgResId();
                        break;
                    case CUSTOM_EVENT:
                        CustomEventType customEventType = CustomEventType.getType(notificationTypeString);
                        iconResId = customEventType.getIconResId();
                        msgResId = customEventType.getMsgResId();
                        break;
                    case APP:
                        AppNotificationType appNotificationType = AppNotificationType.getType(notificationTypeString);
                        iconResId = appNotificationType.getIconResId();
                        msgResId = appNotificationType.getMsgResId();
                        break;
                }

                holder.txtPreTime.setText(DateUtil.calculateTime(context, item.getRegTime()));

                String time = "(" + DateUtil.convertFormatDate("yyyy-MM-dd HH:mm", item.getRegTime()) + ")";
                holder.txtTime.setText(time);

                if (iconResId > 0) {
                    holder.iconType.setImageResource(iconResId);
                }

                if (msgResId > 0) {
                    holder.txtMsg.setText(msgResId);
                }
            }
        }
    }

    public void setNotiList(int position) {
        NotificationModel model = items.get(position);

        List<NotificationModel> list = model.getChildrenList();
        if (list == null) {
            list = DbQuery.selectNotiMessageList(model.getSerial(), terms);
        }
        model.setChildrenList(list);

        int index = position + 1;
        items.addAll(index, list);
        notifyItemRangeInserted(index, list.size());
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.layout_top_bar)
        View viewTopBar;

        @BindView(R.id.layout_noti_device_msg)
        View viewMsg;

        @BindView(R.id.txt_device_title)
        TextView txtDeviceTitle;

        @BindView(R.id.div_line)
        View divLine;

        @BindView(R.id.icon_type)
        ImageView iconType;

        @BindView(R.id.txt_pre_time)
        TextView txtPreTime;

        @BindView(R.id.txt_time)
        TextView txtTime;

        @BindView(R.id.txt_msg)
        TextView txtMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
