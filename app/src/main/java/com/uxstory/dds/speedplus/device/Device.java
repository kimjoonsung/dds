package com.uxstory.dds.speedplus.device;

import android.content.Context;

import com.uxstory.dds.speedplus.communicator.CommunicatorImpl;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.message.type.EventHistorySyncType;
import com.uxstory.dds.speedplus.notification.type.ErrorType;
import com.uxstory.dds.speedplus.message.type.WaterSensingType;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.StringUtil;

public class Device {
    private String name;
    private String ip;
    private int port;
    private String serial;
    private String session;
    private String encryptKey;
    private WaterSensingType waterSensingType;
    private Event lastEvent;

    /**
     * WorkInfo
     */
    private String patientName;
    private String block;
    private String restoration;

    private String leftBurName;
    private String rightBurName;
    private int leftBurUsingCount;
    private int rightBurUsingCount;
    private long waterWorkingTime;

    private MillingProgress millingProgress;

    /**
     * workFinish
     */
    private String millingStartTime;
    private String elapsedTime;

    private WorkStateType workState;
    private ErrorType errorType;

    private CommunicatorImpl communicator;

    private boolean isClosedCardView;

    private String millingFinishTime;

    private int notiCount;

    private int notiCountMonitoring;

    private int retryConnectingCount;

    private int millingFinishCount;

    private String lastEventHistorySyncDate;

    private EventHistorySyncType eventHistorySyncType;

    public EventHistorySyncType getEventHistorySyncType() {
        if (eventHistorySyncType == null) {
            return EventHistorySyncType.NULL;
        }
        return eventHistorySyncType;
    }

    public void setEventHistorySyncType(EventHistorySyncType eventHistorySyncType) {
        this.eventHistorySyncType = eventHistorySyncType;
    }

    public String getLastEventHistorySyncDateA() {
        if (lastEventHistorySyncDate != null && !lastEventHistorySyncDate.isEmpty()) {
            return StringUtil.convertTimeMillisToDate("yyyy-MM-dd a hh:mm", StringUtil.convertDateToTimeMillis(lastEventHistorySyncDate));
        }
        return null;
    }

    public String getLastEventHistorySyncDate() {
        return this.lastEventHistorySyncDate;
    }

    public void setLastEventHistorySyncDate(String lastEventHistorySyncDate) {
        this.lastEventHistorySyncDate = lastEventHistorySyncDate;
    }

    public int getMillingFinishCount() {
        return millingFinishCount;
    }

    public void setMillingFinishCount(int millingFinishCount) {
        this.millingFinishCount = millingFinishCount;
    }

    public CommunicatorImpl getCommunicator() {
        return communicator;
    }

    public int getRetryConnectingCount() {
        return retryConnectingCount;
    }

    public synchronized void plusRetryConnectingCount() {
        this.retryConnectingCount++;
    }

    public synchronized void clearRetryConnectingCount() {
        this.retryConnectingCount = 0;
    }

    public int getNotiCountMonitoring() {
        return notiCountMonitoring;
    }

    public synchronized void plusNotiCountMonitoring() {
        if(getWorkStateType().ordinal() >= WorkStateType.READY.ordinal()) {
            this.notiCountMonitoring++;
        }
    }

    public synchronized void minusNotiCountMonitoring() {
        if(getWorkStateType().ordinal() >= WorkStateType.READY.ordinal()) {
            this.notiCountMonitoring = (this.notiCountMonitoring <= 0) ? 0 : this.notiCountMonitoring - 1;
        }
    }

    public synchronized void clearNotiCountMonitoring() {
        this.notiCountMonitoring = 0;
    }

    public int getNotiCount() {
        return notiCount;
    }

    public void setNotiCount(int notiCount) {
        this.notiCount = (notiCount < 0) ? 0 : notiCount;
    }

    public synchronized void plusNotiCount() {
        this.notiCount++;
    }

    public synchronized void minusNotiCount() {
        this.notiCount = (this.notiCount <= 0) ? 0 : this.notiCount - 1;
    }

    public synchronized void clearNotiCount() {
        this.notiCount = 0;
    }

    public void setCommunicator(CommunicatorImpl communicator) {
        this.communicator = communicator;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public String getMillingFinishTime() {
        return millingFinishTime;
    }

    public void setMillingFinishTime(String millingFinishTime) {
        this.millingFinishTime = millingFinishTime;
    }

    public boolean isClosedCardView() {
        return isClosedCardView;
    }

    public void setClosedCardView(boolean closedCardView) {
        isClosedCardView = closedCardView;
    }

    public long getWaterWorkingTime() {
        return waterWorkingTime;
    }

    public void setWaterWorkingTime(long waterWorkingTime) {
        this.waterWorkingTime = waterWorkingTime;
    }

    public boolean isWaterWarning() {
        return getWaterSensingType() == WaterSensingType.SHORTAGE || waterWorkingTime > Constants.WATER_WARNING_TIME * 3600;
    }

    public boolean isBurWarning() {
        return leftBurUsingCount > Constants.BUR_WARNING_COUNT || rightBurUsingCount > Constants.BUR_WARNING_COUNT;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getEncryptKey() {
        return encryptKey;
    }

    public void setEncryptKey(String encryptKey) {
        this.encryptKey = encryptKey;
    }

    public Event getLastEvent() {
        return lastEvent;
    }

    public void clearMillingWorkInfo() {
        setMillingProgress(null);
        setPatientName(null);
        setBlock(null);
        setRestoration(null);
        setLeftBurName(null);
        setRightBurName(null);
        setMillingProgress(null);
        setMillingStartTime(null);
        setMillingFinishTime(null);
    }

    public void setLastMillingFinishInfo(Event event) {
        this.lastEvent = event;
        if (event != null) {
            setWorkInfo(event);
            setMillingStartTime(event.getMillingStartTime());
            setElapsedTime(event.getElapsedTime());
            MillingProgress millingProgress = getMillingProgress();
            if (millingProgress == null) {
                millingProgress = new MillingProgress();
            }
            millingProgress.setPercent(100);
            millingProgress.setRemainingTime("");
            setMillingProgress(millingProgress);
            setMillingFinishTime(event.getTime());
        }
    }

    public void setLastEvent(Event lastEvent) {
        if (lastEvent != null) {
            lastEvent.setPreEvent(this.lastEvent);
            this.lastEvent = lastEvent;
            MillingProgress millingProgress = getMillingProgress();

            if (lastEvent.getEventType() != EventType.NULL) {
                switch (lastEvent.getEventType()) {
                    case READY:
                    case INSERT_BUR_AND_BLOCK:
                    case SENSING:
                        clearMillingWorkInfo();
                        break;
                    case MILLING_START:
                        setWorkInfo(lastEvent);
                        setMillingStartTime(lastEvent.getTime());
                        break;
                    case MILLING_RESTART:
                        setMillingStartTime(lastEvent.getTime());
                        break;
                    case PAUSE:
                        if (millingProgress != null) {
                            millingProgress.setPauseTime();
                        }
                        break;
                    case MILLING_CONTINUE:
                        if (millingProgress != null) {
                            millingProgress.setContinueTime();
                        }
                        break;
                    case MILLING_FINISH:
                        if (millingProgress == null) {
                            millingProgress = new MillingProgress();
                        }
                        millingProgress.setPercent(100);
                        millingProgress.setRemainingTime("");
                        setMillingProgress(millingProgress);
                        setMillingFinishTime(lastEvent.getTime());
                    case MILLING_STOP:
                        setElapsedTime(lastEvent.getElapsedTime());
                        break;
                }
                checkError(lastEvent);
            }
        }
    }

    private void checkError(Event event) {
        if (lastEvent.getEventType().isError() && millingProgress != null) {
            millingProgress.setPauseTime();
        }
        switch (event.getEventType()) {
            case WATER_FLOW_WARNING:
                setWaterSensingType(WaterSensingType.SHORTAGE);
                return;
            case LEFT_BUR_TYPE_WARNING:
            case RIGHT_BUR_TYPE_WARNING:
            case BOTH_BUR_TYPE_WARNING:
//                setBurWarning(true);
                return;
            default:
                break;
        }
    }

//    public void setErrorType(ErrorType errorType) {
//        if(errorType != null) {
//            switch (errorType) {
//                case ERR_WATER_FLOW_WARNING:
//                    setWaterSensingType(WaterSensingType.SHORTAGE);
//                    return;
//                case ERR_LEFT_BUR_TYPE_WARNING:
//                case ERR_RIGHT_BUR_TYPE_WARNING:
//                case ERR_BOTH_BUR_TYPE_WARNING:
//                    setBurWarning(true);
//                    return;
//                default:
//                    break;
//            }
//        }
//        this.errorType = errorType;
//    }

    public WaterSensingType getWaterSensingType() {
        return waterSensingType;
    }

    public void setWaterSensingType(WaterSensingType waterSensingType) {
        this.waterSensingType = waterSensingType;
    }

    public MillingProgress getMillingProgress() {
        return millingProgress;
    }

    public void setMillingProgress(MillingProgress millingProgress) {
        this.millingProgress = millingProgress;
    }

    public boolean isConnected() {
        return getWorkStateType().ordinal() >= WorkStateType.CONNECTED.ordinal();
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getRestoration() {
        return restoration;
    }

    public void setRestoration(String restoration) {
        this.restoration = restoration;
    }

    public String getLeftBurName() {
        return leftBurName;
    }

    public void setLeftBurName(String leftBurName) {
        this.leftBurName = leftBurName;
    }

    public String getRightBurName() {
        return rightBurName;
    }

    public void setRightBurName(String rightBurName) {
        this.rightBurName = rightBurName;
    }

    public void setWorkInfo(Event event) {
        setPatientName(event.getPatientName());
        setBlock(event.getBlock());
        setRestoration(event.getRestoration());
        setLeftBurName(event.getLeftBurName());
        setRightBurName(event.getRightBurName());
    }

    public String getMillingStartTime() {
        return millingStartTime;
    }

    public void setMillingStartTime(String millingStartTime) {
        this.millingStartTime = millingStartTime;
    }

    public int getMillingProgressPercent() {
        if (millingProgress != null) {
            return millingProgress.getLocalPercent(getWorkStateType());
        }
        return 0;
    }

    public String getMillingRemainingTime() {
        if (millingProgress != null) {
            return millingProgress.getLocalRemainingTime(getWorkStateType());
        }
        return "00:00";
    }

    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public WorkStateType getWorkStateType() {
        if (workState == null) {
            workState = WorkStateType.ON_ADD;
        }
        return workState;
    }

    public void setWorkStateType(WorkStateType workState) {
        if (workState != WorkStateType.NULL) {
            this.workState = workState;
        }
    }

    public int getLeftBurUsingCount() {
        return leftBurUsingCount;
    }

    public void setLeftBurUsingCount(int leftBurUsingCount) {
        this.leftBurUsingCount = leftBurUsingCount;
    }

    public int getRightBurUsingCount() {
        return rightBurUsingCount;
    }

    public void setRightBurUsingCount(int rightBurUsingCount) {
        this.rightBurUsingCount = rightBurUsingCount;
    }

    public RestorationType getRestorationType() {
        return RestorationType.getType(restoration);
    }
}
