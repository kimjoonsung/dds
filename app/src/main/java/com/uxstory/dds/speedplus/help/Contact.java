package com.uxstory.dds.speedplus.help;

import android.content.Context;
import android.content.Intent;

import com.uxstory.dds.speedplus.R;

public class Contact {

    public static void sendEmail(Context context, String emailAddress) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("plain/Text");
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
        email.putExtra(Intent.EXTRA_SUBJECT, "[" + context.getString(R.string.app_name) + "] " + context.getString(R.string.txt_use_info_title_help));
        email.putExtra(Intent.EXTRA_TEXT, "");
        email.setType("message/rfc822");
        context.startActivity(email);
    }
}
