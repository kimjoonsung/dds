package com.uxstory.dds.speedplus.device;

public class BurInfo {

    private String leftBurName;
    private String rightBurName;
    private int leftBurUsingCount;
    private int rightBurUsingCount;

    public String getLeftBurName() {
        return leftBurName;
    }

    public void setLeftBurName(String leftBurName) {
        this.leftBurName = leftBurName;
    }

    public String getRightBurName() {
        return rightBurName;
    }

    public void setRightBurName(String rightBurName) {
        this.rightBurName = rightBurName;
    }

    public int getLeftBurUsingCount() {
        return leftBurUsingCount;
    }

    public void setLeftBurUsingCount(int leftBurUsingCount) {
        this.leftBurUsingCount = leftBurUsingCount;
    }

    public int getRightBurUsingCount() {
        return rightBurUsingCount;
    }

    public void setRightBurUsingCount(int rightBurUsingCount) {
        this.rightBurUsingCount = rightBurUsingCount;
    }
}
