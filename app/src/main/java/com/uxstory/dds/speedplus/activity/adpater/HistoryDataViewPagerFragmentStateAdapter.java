package com.uxstory.dds.speedplus.activity.adpater;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.uxstory.dds.speedplus.activity.fragment.HistoryStatisticsPageDetailFragment;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.model.DataCountModel;

import java.util.ArrayList;

public class HistoryDataViewPagerFragmentStateAdapter extends FragmentStateAdapter {

    private ArrayList<DataCountModel> list = new ArrayList<>();
    private Device device;

    public HistoryDataViewPagerFragmentStateAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    public void setList(ArrayList<DataCountModel> list) {
        this.list = list;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return HistoryStatisticsPageDetailFragment.newInstance(device, list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
