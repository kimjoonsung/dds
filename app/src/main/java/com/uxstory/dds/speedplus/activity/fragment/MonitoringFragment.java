package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.airbnb.lottie.LottieAnimationView;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.device.MillingProgress;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import butterknife.BindView;

public class MonitoringFragment extends AbstractBaseFragment {

    private final static String TAG = MonitoringFragment.class.getSimpleName();

    @BindView(R.id.scale_layout)
    View viewBg;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.txt_milling)
    TextView txtMilling;

    @BindView(R.id.txt_status)
    TextView txtStatus;

    @BindView(R.id.txt_percent)
    TextView txtPercent;

    @BindView(R.id.txt_large_remain_time)
    TextView txtLargeRemainTime;

    @BindView(R.id.txt_time_small)
    TextView txtTimeSmall;

    @BindView(R.id.txt_min)
    TextView txtMin;

    @BindView(R.id.txt_sec)
    TextView txtSec;

    @BindView(R.id.icon_milling)
    ImageView iconMilling;

    @BindView(R.id.txt_patient_name)
    TextView txtPatientName;

    @BindView(R.id.icon_type_02_e)
    ImageView iconMillingSmall;

    @BindView(R.id.icon_block_02)
    ImageView iconBlockSmall;

    @BindView(R.id.txt_block)
    TextView txtBlock;

    @BindView(R.id.txt_restoration)
    TextView txtRestoration;

    @BindView(R.id.btn_detail_02_n)
    ImageView btnDetail;

    @BindView(R.id.btn_state_bur_n)
    ImageView btnBur;

    @BindView(R.id.btn_state_water_n)
    ImageView btnStateWater;

    @BindView(R.id.lottie_animation)
    LottieAnimationView lottieAnimation;

    @BindView(R.id.text_today)
    TextView txtToday;


    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogU.d(TAG, "onConfigurationChanged() " + this);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    this.setLayoutId(R.layout.fragment_monitoring_landscape);
//                    if(getDevice().getWorkStateType().isMillingState()) {
//                        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//                    }

                } else {
                    this.setLayoutId(R.layout.fragment_monitoring);
                }

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setReorderingAllowed(false);
                ft.detach(this).attach(this).commitNow();

                initView();

                addBackStackChangedListener();
                registerRxBus();

                new Handler().post(this::onUpdateView);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setOrientation() {
        getMainAcivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        btnAdd.setOnClickListener(view1 -> {
//            startActivityForResult(new Intent(getActivity(), PairingActivity.class), 0);
//        });
        initView();
    }

    private void initView() {
        lottieAnimation.setOnClickListener(view -> {
            super.setOrientation();
            new Handler().post(() -> {
                HistoryFragment fragment = new HistoryFragment();
                fragment.setDevice(getDevice());
                addFragment(getActivity(), fragment);
            });
        });

        btnDetail.setOnClickListener(view -> {
            super.setOrientation();
            new Handler().post(() -> {
                HistoryFragment fragment = new HistoryFragment();
                fragment.setDevice(getDevice());
                addFragment(getActivity(), fragment);
            });
        });

        btnBur.setOnClickListener(view -> {
            super.setOrientation();
            new Handler().post(() -> {
                StateBurFragment fragment = new StateBurFragment();
                fragment.setDevice(getDevice());
                addFragment(getActivity(), fragment);
            });
        });

        btnStateWater.setOnClickListener(view -> {
            super.setOrientation();
            new Handler().post(() -> {
                StateWaterFragment fragment = new StateWaterFragment();
                fragment.setDevice(getDevice());
                addFragment(getActivity(), fragment);
            });
        });

        if (getLayoutId() == R.layout.fragment_monitoring_landscape) {
            viewBg.setOnClickListener(v -> getMainAcivity().toggleNavi());
        }
    }

    private Thread elapsedThread;

    private void elapsedTimeCount() {
        elapsedThread = new Thread(() -> {
            try {
                while (!elapsedThread.isInterrupted()) {
//                    LogU.d(TAG, "elapsedTimeCount()");
                    Thread.sleep(1000);
                    WorkStateType workStateType = getDevice().getWorkStateType();
                    switch (workStateType) {
                        case MILLING_START:
                        case MILLING_RESTART:
                        case MILLING_CONTINUE:
                            if (!getDevice().getLastEvent().getEventType().isError() && MonitoringFragment.this.getActivity() != null) {
                                MonitoringFragment.this.getActivity().runOnUiThread(() -> {
                                    MillingProgress millingProgress = getDevice().getMillingProgress();
                                    if (millingProgress != null) {
                                        setDefaultValues();
                                    }
                                });
                            }
                            break;
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        elapsedThread.setDaemon(true);
        elapsedThread.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        LogU.d(TAG, "onResume(): ddsService=" + getDdsService());
//        LogU.d(TAG, "onResume(): milling count=" + DbQuery.selectLastMillingFinishCount(1));
        elapsedTimeCount();
        lottieAnimation.playAnimation();
    }

    @Override
    public void onPause() {
        super.onPause();
        LogU.d(TAG, "onPause()");
        if (elapsedThread != null) {
            elapsedThread.interrupt();
            elapsedThread = null;
        }
        lottieAnimation.pauseAnimation();
    }

    @Override
    protected void onUpdateView() {
        super.onUpdateView();
        LogU.d(TAG, "onUpdateView() device = " + getDevice());

//        if(getMainAcivity().isLandscape()) {
//            if (getDevice().getWorkStateType().isMillingState()) {
//                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//            } else if (getDevice().getWorkStateType().isMillingEndState()) {
//                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//            }
//        }

        getMainAcivity().updateToolbarSub(0, getDevice());

        if (getDevice().getLastEvent() != null) {
            txtToday.setText(StringUtil.convertDateToDisplayFormat(StringUtil.convertDateTimeToDate(getDevice().getLastEvent().getTime())));
        } else {
            txtToday.setText(StringUtil.getCurrentDateDisplayFormat());
        }

        txtMilling.setTextColor(getActivity().getColor(R.color.colorBlueLight));

        setDefaultValues();

        WorkStateType workStateType = getDevice().getWorkStateType();

        if (!getDevice().isConnected()) {
            if (getDevice().getLastEvent() != null) {
                setWorkFinish();

            } else {
                setWorkDisconnected();
            }

        } else {
            switch (workStateType) {
                case SENSING:
                    setWorkSensing();
                    break;
                case MILLING_START:
                case MILLING_CONTINUE:
                case MILLING_RESTART:
                    setWorkMilling();
                    break;
                case MILLING_FINISH:
                    setWorkFinish();
                    break;
                case PAUSE:
                    setWorkPause();
                    break;
                case MILLING_STOP:
                    setWorkStop();
                    break;
                case READY:
                case PREPARE:
                    setWorkReady();
                    break;
            }
        }

        setWarningBur(getDevice().isBurWarning());
        setWarningWater(getDevice().isWaterWarning());

//        new Handler().post(() -> ViewUtil.requestLayout((ViewGroup) getView()));
    }

    private void setDefaultValues() {
//        txtPercent.setText(String.valueOf(device.getMillingProgressPercent()));

        progressBar.setProgress(getDevice().getMillingProgressPercent());

        String remain = getDevice().getMillingRemainingTime();
        txtLargeRemainTime.setText(remain);
        txtLargeRemainTime.requestLayout();

        txtPatientName.setText(StringUtil.replaceNullToHyphen(getDevice().getPatientName()));
        txtPatientName.requestLayout();
        txtBlock.setText(StringUtil.replaceNullToHyphen(getDevice().getBlock()));
//        txtRestoration.setText(replaceNullInfo(device.getRestoration()));
        setRestorationInfo();

        WorkStateType workStateType = getDevice().getWorkStateType();
//        LogU.d(TAG, "device.getWorkStateType()=" + workStateType);

        int stateNameResid = workStateType.getNameResId();
        if (workStateType.getNameResId() == R.string.txt_last_milling_info) {
            stateNameResid = R.string.txt_last_milling_info_short;
        }
        String state = getString(stateNameResid);

        switch (workStateType) {
            case MILLING_START:
            case MILLING_RESTART:
            case MILLING_CONTINUE:
            case MILLING_STOP:
            case PAUSE:
                state += "  " + getDevice().getMillingProgressPercent();
                break;
            default:
                break;
        }
//        LogU.d(TAG, "state=" + state);

//        SpannableString spannableString = new SpannableString(content);
//        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6702")), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        spannableString.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        spannableString.setSpan(new RelativeSizeSpan(1.3f), start, end, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtStatus.setText(state);
        txtStatus.requestLayout();

        int millingFinishCount = getDevice().getMillingFinishCount();
        if (getDevice().getWorkStateType().ordinal() >= WorkStateType.READY.ordinal() && getDevice().getWorkStateType().ordinal() < WorkStateType.MILLING_FINISH.ordinal()) {
            millingFinishCount++;
        }
        txtMilling.setText(String.format(getContext().getString(R.string.txt_milling), String.valueOf(millingFinishCount)));
    }

    private void setWorkDisconnected() {
        txtPercent.setText("");
        txtPercent.requestLayout();

        txtStatus.setTextColor(getActivity().getColor(R.color.colorRed));
        txtStatus.setText(R.string.txt_disconnected);
        txtStatus.requestLayout();
    }

    private void setWorkReady() {
        iconMilling.setVisibility(View.VISIBLE);
        iconMilling.setImageResource(R.drawable.icon_block_03);
        txtLargeRemainTime.setVisibility(View.GONE);
        txtMin.setVisibility(View.GONE);
        txtSec.setVisibility(View.GONE);

        txtTimeSmall.setText("");
        txtTimeSmall.requestLayout();

        txtStatus.setTextColor(getActivity().getColor(R.color.colorBlueLight));
        txtPercent.setText("");
        txtPercent.requestLayout();
    }

    private void setWorkSensing() {
        iconMilling.setVisibility(View.VISIBLE);
        iconMilling.setImageResource(R.drawable.icon_block_03);
        txtLargeRemainTime.setVisibility(View.GONE);
        txtMin.setVisibility(View.GONE);
        txtSec.setVisibility(View.GONE);

        txtTimeSmall.setText("");
        txtTimeSmall.requestLayout();

        txtStatus.setTextColor(getActivity().getColor(R.color.colorBlueLight));
        txtPercent.setText("");
        txtPercent.requestLayout();
    }

    private void setWorkMilling() {
        iconMilling.setVisibility(View.GONE);
        txtLargeRemainTime.setVisibility(View.VISIBLE);
        txtMin.setVisibility(View.VISIBLE);
        txtSec.setVisibility(View.VISIBLE);

        txtTimeSmall.setText("");
        txtTimeSmall.requestLayout();

        txtStatus.setTextColor(getActivity().getColor(R.color.colorBlueLight));
        txtPercent.setText(" %");
        txtPercent.requestLayout();

        setRestorationInfo();
    }

    private void setWorkFinish() {
        iconMilling.setVisibility(View.VISIBLE);
        iconMilling.setImageResource(R.drawable.icon_type_03_e);
        txtLargeRemainTime.setVisibility(View.GONE);
        txtMin.setVisibility(View.GONE);
        txtSec.setVisibility(View.GONE);

        String startTime = StringUtil.cutDateTimeToHourMin(getDevice().getMillingStartTime());
        String endTime = StringUtil.cutDateTimeToHourMin(getDevice().getMillingFinishTime());

        String timeSmall = startTime + " ~ " + endTime + " {#(" + StringUtil.convertTimeToMinSec(getDevice().getElapsedTime()) + ")#}";
        ViewUtil.highlightTagText(getContext(), txtTimeSmall, timeSmall, R.color.colorDark);

        txtStatus.setTextColor(getActivity().getColor(R.color.colorBlueLight));
        txtPercent.setText("");
        txtPercent.requestLayout();

        setRestorationInfo();
    }

    private void setWorkPause() {
        iconMilling.setVisibility(View.VISIBLE);
        txtPercent.setVisibility(View.VISIBLE);
        txtLargeRemainTime.setVisibility(View.GONE);
        txtMin.setVisibility(View.GONE);
        txtSec.setVisibility(View.GONE);

        String timeSmall = "{#" + getDevice().getMillingRemainingTime() + "#} sec";
        ViewUtil.highlightTagText(getContext(), txtTimeSmall, timeSmall, R.color.colorDark);

        txtStatus.setTextColor(getActivity().getColor(R.color.colorBlueLight));
        txtPercent.setText(" %");
        txtPercent.requestLayout();

        setRestorationInfo();
        iconMilling.setImageResource(R.drawable.icon_pause);
    }

    private void setWorkStop() {

        txtMilling.setTextColor(getActivity().getColor(R.color.colorRed));

        iconMilling.setVisibility(View.GONE);
        txtLargeRemainTime.setVisibility(View.VISIBLE);
        txtMin.setVisibility(View.VISIBLE);
        txtSec.setVisibility(View.VISIBLE);

        txtTimeSmall.setText("");
        txtTimeSmall.requestLayout();

        txtStatus.setTextColor(getActivity().getColor(R.color.colorRed));
        txtPercent.setText(" %");
        txtPercent.requestLayout();

        setRestorationInfo();
    }

    private void setRestorationInfo() {
        txtRestoration.setText(RestorationType.getRestorationString(getContext(), getDevice().getRestoration()));
        txtRestoration.requestLayout();
        RestorationType restorationType = getDevice().getRestorationType();
        iconMilling.setImageResource(restorationType.getIconLargeResId());
        iconMillingSmall.setImageResource(restorationType.getIconNormalResId());
    }

    private void setWarningWater(boolean warningWater) {
        if (warningWater) {
            btnStateWater.setImageResource(R.drawable.selector_btn_state_water_e);
        } else {
            btnStateWater.setImageResource(R.drawable.selector_btn_state_water);
        }
    }

    private void setWarningBur(boolean warningBur) {
        if (warningBur) {
            btnBur.setImageResource(R.drawable.selector_btn_state_bur_e);
        } else {
            btnBur.setImageResource(R.drawable.selector_btn_state_bur);
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == 0) {
//            onUpdateView();
//        }
//    }
}
