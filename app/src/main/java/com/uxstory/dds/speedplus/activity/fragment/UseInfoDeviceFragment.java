package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.UseInfoRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.help.HelpRenderer;
import com.uxstory.dds.speedplus.help.type.HelpUseInfoDeviceType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class UseInfoDeviceFragment extends AbstractBaseFragment {

    private final static String TAG = UseInfoDeviceFragment.class.getSimpleName();

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.layout_help_install)
    View viewHelpInstall;
    @BindView(R.id.txt_help_install)
    TextView txtHelpInstall;

    @BindView(R.id.layout_help_milling)
    View viewHelpMilling;
    @BindView(R.id.txt_help_milling)
    TextView txtHelpMilling;

    @BindView(R.id.layout_help_water)
    View viewHelpWater;
    @BindView(R.id.txt_help_water)
    TextView txtHelpWater;

    @BindView(R.id.layout_help_bur)
    View viewHelpBur;
    @BindView(R.id.txt_help_bur)
    TextView txtHelpBur;

    @BindView(R.id.layout_help_device)
    View viewHelpDevice;
    @BindView(R.id.txt_help_device)
    TextView txtHelpDevice;

    @BindView(R.id.layout_help_problem)
    View viewHelpProblem;
    @BindView(R.id.txt_help_problem)
    TextView txtHelpProblem;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.layout_no_data)
    View viewNoData;

    private LinkedHashMap<View, TextView> buttonMap = new LinkedHashMap<>();

    private UseInfoRecycleAdapter recyclerAdapter;

    private Typeface defaultTypeface;

    private View lastClickView;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_use_info_device);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(layoutManager);

        recyclerAdapter = new UseInfoRecycleAdapter(getContext(), new ArrayList<>());
        recyclerView.setAdapter(recyclerAdapter);

        defaultTypeface = txtHelpInstall.getTypeface();

        buttonMap.put(viewHelpInstall, txtHelpInstall);
        buttonMap.put(viewHelpMilling, txtHelpMilling);
        buttonMap.put(viewHelpWater, txtHelpWater);
        buttonMap.put(viewHelpBur, txtHelpBur);
        buttonMap.put(viewHelpDevice, txtHelpDevice);
        buttonMap.put(viewHelpProblem, txtHelpProblem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                LogU.d(TAG, "searchView onQueryTextSubmit = " + s);
                setUseInfoList(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                LogU.d(TAG, "searchView onQueryTextChange = " + s);
                if (recyclerAdapter.getSearchStr() != null && (s == null || s.trim().isEmpty())) {
                    if(lastClickView == null) {
                        lastClickView = viewHelpInstall;
                    }
                    lastClickView.performClick();
                }
                return false;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                LogU.d(TAG, "searchView OnQueryTextFocusChange()");
                v.setSelected(hasFocus);
            }
        });

        viewHelpInstall.setSelected(true);
        txtHelpInstall.setTypeface(defaultTypeface, Typeface.BOLD);
        setButtonOnClickListener();

        setUseInfoList(0);
    }

    private void setButtonOnClickListener() {
        for (Map.Entry<View, TextView> elem : buttonMap.entrySet()) {
            elem.getKey().setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    lastClickView = v;
                    onClickButton(v);
                }
            });
        }
    }

    private void onClickButton(View v) {
        int index = 0;
        for (Map.Entry<View, TextView> elem : buttonMap.entrySet()) {
            TextView textView = elem.getValue();
            if (v == elem.getKey()) {
                v.setSelected(true);
                textView.setTypeface(defaultTypeface, Typeface.BOLD);
                setUseInfoList(index);

            } else {
                elem.getKey().setSelected(false);
                textView.setTypeface(defaultTypeface, Typeface.NORMAL);
            }
            index++;
        }

    }

    private void setUseInfoList(int index) {
        if(getContext() != null) {
            List<String> list = new ArrayList<>(Arrays.asList(HelpUseInfoDeviceType.values()[index].getContentsArray(getContext())));
            recyclerAdapter = new UseInfoRecycleAdapter(getContext(), list);
            recyclerAdapter.setSearchStr(null);
            recyclerView.setAdapter(recyclerAdapter);
            recyclerAdapter.notifyDataSetChanged();

            viewNoData.setVisibility(View.GONE);
        }

    }

    private void setUseInfoList(String searchStr) {

        for (Map.Entry<View, TextView> elem : buttonMap.entrySet()) {
            elem.getKey().setSelected(false);
            elem.getValue().setTypeface(defaultTypeface, Typeface.NORMAL);
        }

        String[] splitSearchStr = searchStr.split("\\s");

        List<String> list = new ArrayList<>();
        recyclerAdapter = new UseInfoRecycleAdapter(getContext(), list);
        recyclerAdapter.setSearchStr(searchStr);

        for(HelpUseInfoDeviceType helpType : HelpUseInfoDeviceType.values()) {
            for(String content : helpType.getContentsArray(getContext())) {
                String str = HelpRenderer.clearHelpTags(content);
                LogU.d(TAG, "str="+str);

                for (String sss : splitSearchStr) {
                    if (sss.isEmpty()) {
                        continue;
                    }
                    if(str.toLowerCase().contains(sss.toLowerCase())) {
                        list.add(content);
                        break;
                    }
//                    Pattern pattern = Pattern.compile("(?i)" + sss);
//                    Matcher matcher = pattern.matcher(str);
//                    if (matcher.find()) {
//                        fragments.add(content);
//                        break;
//                    }
                }
            }
        }
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.notifyDataSetChanged();

        ViewUtil.setVisibility(viewNoData, list.size() == 0);
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(R.string.txt_use_info_title_device, null);
    }
}
