package com.uxstory.dds.speedplus.message.type;

import com.uxstory.dds.speedplus.R;

public enum WorkStateType {

    NULL("", -1),
    ON_ADD("on_add", -1),
    CONNECTING("connecting", -1),
    RETRY_UPNP("retry_upnp", -1),
    RETRY_CONNECTING("retry_connecting", R.string.txt_connecting_retry),

    PAIRING("pairing", R.string.txt_pairing),
    DISCONNECTED("disconnected", R.string.txt_connect_error),
    CONNECTED("connected", R.string.txt_connected),

    READY(EventType.READY.getString(), R.string.txt_milling_wait),
    PREPARE(EventType.INSERT_BUR_AND_BLOCK.getString(), R.string.txt_milling_ready),
    SENSING(EventType.SENSING.getString(), R.string.txt_milling_sensing),
    MILLING_START(EventType.MILLING_START.getString(), R.string.txt_milling_ing),
    MILLING_CONTINUE(EventType.MILLING_CONTINUE.getString(), R.string.txt_milling_ing),
    MILLING_RESTART(EventType.MILLING_RESTART.getString(), R.string.txt_milling_ing),
    PAUSE(EventType.PAUSE.getString(), R.string.txt_milling_pause),
    MILLING_STOP(EventType.MILLING_STOP.getString(), R.string.txt_milling_stop),
    MILLING_FINISH(EventType.MILLING_FINISH.getString(), R.string.txt_milling_finish),
    ;

    private String value;
    private int stringResId;

    WorkStateType(String value, int stringResId) {
        this.value = value;
        this.stringResId = stringResId;
    }

    public String getString() {
        return value;
    }

    public int getNameResId() {
        if(this.ordinal() < WorkStateType.READY.ordinal()) {
            return R.string.txt_last_milling_info;
        }
        return this.stringResId;
    }

    public static WorkStateType getType(String stateName) {
        for (WorkStateType state : WorkStateType.values()) {
            if (state.getString().equals(stateName)) {
                return state;
            }
        }
        return NULL;
    }

    public boolean isMillingState() {
        switch (this) {
            case MILLING_START:
            case MILLING_CONTINUE:
            case MILLING_RESTART:
            case PAUSE:
                return true;
        }
        return false;
    }

    public boolean isMillingEndState() {
        switch (this) {
            case MILLING_STOP:
            case MILLING_FINISH:
                return true;
        }
        return false;
    }
}
