package com.uxstory.dds.speedplus.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.service.DdsService;
import com.uxstory.dds.speedplus.service.DdsServiceImpl;
import com.uxstory.dds.speedplus.util.LogU;

public class SplashActivity extends AbstractBaseActivity {

    private final static String TAG = SplashActivity.class.getSimpleName();
    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalApplication.bindDdsService(getApplicationContext(), serviceConnection);
        startTime = System.currentTimeMillis();

        GlobalApplication.setDefualtUiOptions(getWindow().getDecorView().getSystemUiVisibility());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class SplashHandler implements Runnable{
        public void run(){
            if(!Preference.getPreferenceValue(Preference.PreferenceType.ENTERED_TUTORIAL)) {
                startActivity(new Intent(SplashActivity.this, TutorialActivity.class));

            } else if(!Preference.getPreferenceValue(Preference.PreferenceType.ENTERED_GUIDE)){
                startActivity(new Intent(SplashActivity.this, GuideActivity.class));

            } else {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }

//            startActivity(new Intent(SplashActivity.this, TutorialActivity.class));
        }
    }

    public void onDdsServiceConnected() {
        LogU.d(TAG, "onDdsServiceConnected()");
        long interval = 1000 - (startTime - System.currentTimeMillis());
        interval = (interval < 0) ? 0 : interval;
        new Handler().postDelayed(new SplashHandler(), interval);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LogU.d(TAG, "onServiceConnected()");
            DdsServiceImpl.ServiceBinder binder = (DdsServiceImpl.ServiceBinder) service;
            DdsService ddsService = binder.getService();
            LogU.d(TAG, "ddsService = " + ddsService);
            if(ddsService != null) {
                GlobalApplication.setDdsService(ddsService);
                onDdsServiceConnected();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogU.d(TAG, "onServiceDisconnected()");
        }
    };

    @Override
    public void onBackPressed() {
    }
}
