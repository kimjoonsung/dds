package com.uxstory.dds.speedplus.util;

import android.util.Log;

import com.uxstory.dds.speedplus.BuildConfig;

public class LogU {

//    private static final Logger LOG = LoggerFactory.getLogger("LogU");

    public static void d(String tag, String msg) {
//        Log.d("LogU", ""+msg);
        if(BuildConfig.DEBUG) {
            Log.d(tag, msg + "");
        }
    }

    public static void e(String tag, String msg) {
//        Log.d("LogU", ""+msg);
        if(BuildConfig.DEBUG) {
            Log.e(tag, msg + "");
        }
    }
}
