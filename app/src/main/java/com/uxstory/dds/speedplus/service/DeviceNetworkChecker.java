package com.uxstory.dds.speedplus.service;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.upnp.ControlPointListener;
import com.uxstory.dds.speedplus.util.LogU;

import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.UPnP;

import java.util.ArrayList;
import java.util.List;

public class DeviceNetworkChecker {

    private final static String TAG = DeviceNetworkChecker.class.getSimpleName();

    private static DeviceNetworkChecker instance;
    private DdsService ddsService;
    private CheckThread checkThread;

    public static DeviceNetworkChecker getInstance(DdsService ddsService) {
        if (instance == null) {
            instance = new DeviceNetworkChecker(ddsService);
            instance.init();
        }
        return instance;
    }

    private DeviceNetworkChecker(DdsService ddsService) {
        this.ddsService = ddsService;
    }

    private void init() {
        checkThread = new CheckThread();
        checkThread.setDaemon(true);
    }

    public void start() {
        if (checkThread != null) {
            checkThread.start();
        }
    }

    public void stop() {
        if (checkThread != null) {
            checkThread.interrupt();
        }
    }

    private class CheckThread extends Thread {
        @Override
        public void run() {
            super.run();
            perform();
        }

        private void perform() {
            try {

                final int sleepTime = BuildConfig.INTERVAL_CHECK_DEVICE_NETWORK * 1000;
                if (sleepTime <= 0) {
                    return;
                }

                while (!isInterrupted()) {
//                    LogU.d(TAG, "checkDeviceNetwork()");

                    Thread.sleep(sleepTime);

                    try {
                        List<Device> deviceList = ddsService.getDeviceList();
                        for (Device device : deviceList) {
                            LogU.d(TAG, ">> checkDeviceNetwork() : " + device.getSerial() + ", " + device.getWorkStateType() + ", communicator=" + device.getCommunicator());
                            if (device.getWorkStateType() == WorkStateType.DISCONNECTED && device.getCommunicator() == null) {
                                if (device.getRetryConnectingCount() > BuildConfig.RETRY_CONNECT_COUNT) {
                                    device.clearRetryConnectingCount();
                                    device.setWorkStateType(WorkStateType.RETRY_UPNP);
                                    new Thread(() -> {
                                        List<Device> uPnpDeviceList = findUPnpDevice();
                                        LogU.d(TAG, ">> checkDeviceNetwork() : findUPnpDevice()");
                                        for (Device d : uPnpDeviceList) {
                                            if (device.getSerial().equals(d.getSerial()) && !device.getIp().equals(d.getIp())) {
                                                device.setIp(d.getIp());
                                                DbQuery.updateDevice(device, false);
                                                device.setWorkStateType(WorkStateType.RETRY_CONNECTING);
                                                ddsService.startCommunicator(device);
                                                break;
                                            }
                                        }
                                        if (device.getWorkStateType() == WorkStateType.RETRY_UPNP) {
                                            device.setWorkStateType(WorkStateType.DISCONNECTED);
                                        }
                                    }).start();

                                } else {
                                    device.plusRetryConnectingCount();
                                    device.setWorkStateType(WorkStateType.RETRY_CONNECTING);
                                    ddsService.startCommunicator(device);
                                }
                            }
                        }
                    } catch (Exception e) {
                        break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Device> findUPnpDevice() {

        UPnP.setEnable(UPnP.USE_ONLY_IPV4_ADDR);
        ControlPoint controlPoint = new ControlPoint();

        List<Device> uPnpDeviceList = new ArrayList<>();

        ControlPointListener listener = new ControlPointListener();
        listener.setDeviceBinder(new ControlPointListener.DeviceBinder() {

            @Override
            public void onAdd(String ip, int port, String serial) {
                LogU.d(TAG, "Device onAdd -----------");
                LogU.d(TAG, "ip: " + ip);
                LogU.d(TAG, "port: " + port);
                LogU.d(TAG, "serial: " + serial);

                if (!isAlreadyAddDevice(serial)) {
                    Device item = new Device();
                    item.setIp(ip);
                    item.setSerial(serial);
                    uPnpDeviceList.add(item);
                }
            }

            @Override
            public void onRemove(String serial) {
                for (Device item : uPnpDeviceList) {
                    if (serial.equals(item.getSerial())) {
                        uPnpDeviceList.remove(item);
                        break;
                    }
                }
            }

            private boolean isAlreadyAddDevice(String serial) {
                for (Device item : uPnpDeviceList) {
                    if (serial.equals(item.getSerial())) {
                        return true;
                    }
                }
                return false;
            }

        });

        controlPoint.addSearchResponseListener(listener);
        controlPoint.addNotifyListener(listener);

        controlPoint.start();
        try {
            Thread.sleep(BuildConfig.UPNP_TIMEOUT * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        controlPoint.stop();

        return uPnpDeviceList;
    }
}
