package com.uxstory.dds.speedplus.util;

import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringUtil {


    public static String replaceNullToHyphen(String info) {
        return info == null ? "-" : info;
    }

    public static String inputStreamToString(final InputStream stream) {
        final BufferedReader br = new BufferedReader(new InputStreamReader(
                stream));

        final StringBuffer sb = new StringBuffer(10240);
        String line = null;
        final String linesep = System.getProperty("line.separator"); // "\n"
        try {
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(linesep);
            }
        } catch (final IOException e) {
            sb.append("");
        }

        try {
            br.close();
        } catch (final IOException e) {

        }

        return sb.toString();
    }

    public static String getDateDbFormat(String date) {
        if(date != null) {
            String pattern = "^[0-9]{4}-[0-9]{2}-[0-9]{2}$";
            if (Pattern.matches(pattern, date)) {
                return "%Y-%m-%d";
            }

            pattern = "^[0-9]{4}-[0-9]{2}$";
            if (Pattern.matches(pattern, date)) {
                return "%Y-%m";
            }

            pattern = "^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$";
            if (Pattern.matches(pattern, date)) {
                return "%Y-%m-%d %H:%M:%S";
            }
        }
        return null;
    }

    public static String convertDateToRawFormat(String date) {

        if (date != null) {
            date = date.replaceAll("\\.", "-");
        }
        return date;
    }

    public static String convertDateToDisplayFormat(String date) {

        if (date != null) {
            date = date.replaceAll("\\-", ".");
        }
        return date;
    }

    /**
     * 매개변수 str 이 null 이면, "" 문자열로 리턴한다.
     *
     * @param str
     * @return
     */
    public static String nullToBlank(final String str) {

        if (str == null) {
            return "";
        }
        return str;
    }

    /**
     * 매개변수 str 이 null 이거나, "" 문자열이면 true 를 리턴한다.
     *
     * @param str
     * @return
     */
    public static boolean isBlank(final String str) {

        final String temp = nullToBlank(str);
        if (temp.equals("")) {
            return true;
        }
        return false;
    }

    /**
     * 매개변수 str 이 null 이거나, "" 문자열이면 true 를 리턴한다.
     *
     * @param str
     * @return
     */
    public static boolean isBlank(final byte[] str) {

        if (str.length > 0) {
            return true;
        }
        return false;
    }

    /**
     * SpaceRemove(String srcStr) 원본 문자열에서 공백을 제거 한 후 공백이 제거된 문자열을 리턴한다.
     *
     * @param srcStr 원본 문자열
     * @return 공백제거된 문자열
     */
    public static String spaceRemove(final String srcStr) {
        if (srcStr != null) {
            return srcStr.replaceAll("\\p{Space}", "");
        }
        return "";
    }

    /**
     * Replace(String str) 원본 문자열에서하이픈을 제거 한 후 하이픈이 제거된 문자열을 리턴한다.
     *
     * @param srcStr 원본 문자열
     * @return 하이픈제거된 문자열
     */
    public static String replaceHyphensToBlank(final String srcStr) {
        return srcStr.replaceAll("-", "");
    }

    /**
     * 파일 사이즈를 String형식으로 변환하여 반환한다.
     *
     * @param total
     * @return String
     */
    public static String convertFilesizeToString(final long total) {

        String strSize = null;

        try {
            if (total < 1024) {
                strSize = total + "byte";
            } else if (total < 1024 * 1024) {
                strSize = total / 1024 + "KB";
            } else if (total < 1024 * 1024 * 1024) {
                strSize = total / (1024 * 1024) + "MB";
            } else {
                // GB 단위를 소수점 둘째 자리까지 표시
                double nSize = (total / ((double) 1024 * 1024 * 1024));
                strSize = (new DecimalFormat("#.##").format(nSize)) + "GB";
            }
        } catch (Exception e) {
            strSize = "";
        }

        return strSize;
    }

    /**
     * 파일 사이즈를 String형식으로 변환하여 반환한다.
     * 소수점 둘째자리까지
     *
     * @param total
     * @return String
     */
    public static String convertFilesizeToStringBelowDecimal(final long total) {
        String strSize = null;
        double nSize = 0;
        try {
            if (total < 1024) {
                strSize = total + "byte";
            } else if (total < 1024 * 1024) {
                nSize = total / (double) 1024;
                strSize = (new DecimalFormat("#.##").format(nSize)) + "KB";
            } else if (total < 1024 * 1024 * 1024) {
                nSize = total / ((double) 1024 * 1024);
                strSize = (new DecimalFormat("#.##").format(nSize)) + "MB";
            } else {
                // GB 단위를 소수점 둘째 자리까지 표시
                nSize = (total / ((double) 1024 * 1024 * 1024));
                strSize = (new DecimalFormat("#.##").format(nSize)) + "GB";
            }
        } catch (Exception e) {
            strSize = "";
        }
        return strSize;

    }

    /**
     * Duration 값을 시간 표시 포맷으로 변환
     *
     * @param duration - Long
     * @return - String
     */
    public static String convertDurationToString(final long duration) {
        String result = null;
        long time = duration / 1000;
        long hour = time / (60 * 60);
        long min = time % (60 * 60) / 60;
        long sec = time % 60;
        result = String.format("%02d:%02d:%02d", hour, min, sec);
        return result;
    }


    /**
     * @param time yyyy-MM-dd HH:mm:ss
     * @return mm:ss
     */
    public static String cutDateTimeToHourMin(String time) {
        try {
            String[] t = time.split("\\s")[1].split(":");
            return t[0] + ":" + t[1];
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * convert HH:mm:ss to min
     *
     * @param time HH:mm:ss
     * @return min
     */
    public static String convertTimeToMin(String time) {
        try {
            String[] t = time.split(":");
            int h = Integer.valueOf(t[0]);
            int m = Integer.valueOf(t[1]);
            return String.valueOf(h * 60 + m) + "min";
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * convert HH:mm:ss to mm:ss
     *
     * @param time HH:mm:ss
     * @return min
     */
    public static String convertTimeToMinSec(String time) {
        try {
            String[] t = time.split(":");
            int h = Integer.valueOf(t[0]);
            int m = h * 60 + Integer.valueOf(t[1]);
            int s = Integer.valueOf(t[2]);
            return String.format(Locale.getDefault(), "%02dmin %02dsec", m, s);
        } catch (Exception e) {
            return "00:00";
        }
    }

    /**
     * convert HH:mm:ss to sec
     *
     * @param time HH:mm:ss
     * @return sec
     */
    public static int convertTimeSec(String time) {
        try {
            String[] t = time.split(":");
            int h = Integer.valueOf(t[0]);
            int m = h * 60 + Integer.valueOf(t[1]);
            int s = m * 60 + Integer.valueOf(t[2]);
            return s;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * convert sec to mm:ss
     *
     * @param sec
     * @return min
     */
    public static String convertSecToMinSec(int sec) {
        try {
            int m = sec / 60;
            int s = sec % 60;
            return String.format(Locale.getDefault(), "%02d:%02d", m, s);
        } catch (Exception e) {
            return "00:00";
        }
    }

    public static String convertDateToMonthDay(String date) {
        try {
            String[] t = date.split("-");
            return t[1]+"-"+t[2];
        } catch (Exception e) {
            return "";
        }
    }

    public static String convertDateToMonth(String date) {
        try {
            String[] t = date.split("-");
            return t[1];
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * convert sec to HH:mm:ss
     *
     * @param sec
     * @return min
     */
    public static String convertSecToHourMinSec(int sec) {
        try {
            long s = sec % 60;
            long m = (sec / 60) % 60;
            long h = (sec / (60 * 60)) % 24;
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", h, m, s);
        } catch (Exception e) {
            return "00:00:00";
        }
    }


    /**
     * milliseconds to yyyy-MM-dd
     *
     * @param milliseconds
     * @return
     */
    public static String convertTimeMillisToDate(final long milliseconds) {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return mSimpleDateFormat.format(new Date(milliseconds));
    }

    /**
     * milliseconds를 날짜(yyyy-MM-dd HH:mm:ss)로 변환
     *
     * @param milliseconds
     * @return (yyyy / MM / dd HH : mm : ss)
     */
    public static String convertTimeMillisToDateTime(final long milliseconds) {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return mSimpleDateFormat.format(new Date(milliseconds));
    }

    public static String getCurrentDateTime() {
        return convertTimeMillisToDateTime(System.currentTimeMillis());
    }

    public static String getCurrentDate() {
        return convertTimeMillisToDate(System.currentTimeMillis());
    }

    public static String getCurrentDateDisplayFormat() {
        return convertDateToDisplayFormat(getCurrentDate());
    }

    public static String getCurrentMonthDisplayFormat() {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy.MM", Locale.getDefault());
        return mSimpleDateFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * milliseconds를 날짜(format)로 변환
     *
     * @param format
     * @return (yyyy / MM / dd HH : mm : ss)
     * @parma milliseconds
     */
    public static String convertTimeMillisToDate(String format, final long milliseconds) {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return mSimpleDateFormat.format(new Date(milliseconds));
    }

    /**
     * 날짜(yyyy/MM/dd HH:mm:ss)를 milliseconds로 변환
     *
     * @param date (yyyy/MM/dd HH:mm:ss)
     * @return milliseconds
     */
    public static long convertDateToTimeMillis(String date) {
        try {
//            date = date.replace('T', ' ');
            final Date currentParsed = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(date);
            long milliseconds = currentParsed.getTime();
            return milliseconds;
        } catch (ParseException e) {
            return 0L;
        }
    }

    public static String convertDateTimeToDate(String date) {
        try {
            String[] t = date.split("\\s");
            return t[0];
        } catch (Exception e) {
            return "";
        }
    }


    /**
     * date 값을 String형식으로 반환한다.
     *
     * @param milliseconds
     * @return String
     */
    public static String convertLongToVisibleDate(long milliseconds) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault());
        return format.format(new Date(milliseconds));
    }

    public static String convertLongToDateTime(long milliseconds) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd", Locale.getDefault());
        return format.format(new Date(milliseconds));
    }

    public final static String nullToHyphen(String info) {
        return info == null ? "-" : info;
    }
}
