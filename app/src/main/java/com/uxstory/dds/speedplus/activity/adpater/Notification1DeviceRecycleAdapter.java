package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnScrollValueListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification1DeviceRecycleAdapter extends RecyclerView.Adapter<Notification1DeviceRecycleAdapter.ViewHolder> {

    private final static String TAG = Notification1DeviceRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<NotificationModel> items;
    private OnScrollValueListener onScrollValueListener;

    public Notification1DeviceRecycleAdapter(Context context, List<NotificationModel> items, OnScrollValueListener onScrollValueListener) {
        this.context = context;
        this.items = items;
        this.onScrollValueListener = onScrollValueListener;
    }

    public void setList(List<NotificationModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noti_device, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        NotificationModel item = items.get(position);
//        String index = String.valueOf(items.size() - position);
        holder.txtDeviceTitle.setText(item.getName());

        holder.viewTopBar.setSelected(item.isSelected());
        if (item.isSelected()) {
            searchNotiDateList(item, holder, null);
        }
        setBodyVisible(holder);

        holder.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                LogU.d(TAG, "searchView onQueryTextSubmit = " + s);
                searchNotiDateList(item, holder, s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                LogU.d(TAG, "searchView onQueryTextChange = " + s);
                if (holder.searchNotiTypeList != null && holder.searchNotiTypeList.size() > 0 && (s == null || s.trim().isEmpty())) {
                    searchNotiDateList(item, holder, null);
                }
                return false;
            }
        });

        holder.searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                LogU.d(TAG, "searchView OnQueryTextFocusChange()");
                v.setSelected(hasFocus);
            }
        });

        holder.viewTopBar.setOnClickListener(v -> {
            v.setSelected(!v.isSelected());
            item.setSelected(v.isSelected());
            if (v.isSelected()) {
                searchNotiDateList(item, holder, holder.search);
            }
            setBodyVisible(holder);
        });

    }

    private void setBodyVisible(ViewHolder holder) {
        ViewUtil.setVisibility(holder.viewBody, holder.viewTopBar.isSelected());
    }

    private void searchNotiDateList(NotificationModel notificationModel, ViewHolder holder, String search) {
        Set<String> notiTypeList = NotificationPublisher.searchNotificationTypeString(context, search);
        if (search == null || notiTypeList.size() > 0) {
            holder.search = search;
            holder.searchNotiTypeList = notiTypeList;
            List<String> list = DbQuery.selectNotiDates(notificationModel.getSerial(), notiTypeList, null);

            Notification2DateRecycleAdapter recyclerAdapter = new Notification2DateRecycleAdapter(context, list, onScrollValueListener);
            holder.recyclerView.setAdapter(recyclerAdapter);

            recyclerAdapter.setSerial(notificationModel.getSerial());
            recyclerAdapter.setTitle(notificationModel.getName());
            recyclerAdapter.setSearch(search);
            recyclerAdapter.setSearchNotiTypeList(notiTypeList);
            recyclerAdapter.notifyDataSetChanged();

        } else {
            Device device = new Device();
            device.setName(notificationModel.getName());
            NotificationPublisher.showNotification(context, device, AppNotificationType.SEARCH_NO_DATA);
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.layout_top_bar)
        View viewTopBar;

        @BindView(R.id.layout_body)
        View viewBody;

        @BindView(R.id.txt_device_title)
        TextView txtDeviceTitle;

        @BindView(R.id.recycler_noti_device_day)
        RecyclerView recyclerView;

        @BindView(R.id.search_view)
        SearchView searchView;

        String search;
        Set<String> searchNotiTypeList;

//        private List<NotificationModel> list = new ArrayList<>();
//        private Notification2DateRecycleAdapter recyclerAdapter;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(layoutManager);
        }
    }
}
