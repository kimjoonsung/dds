package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.notification.type.CustomEventType;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification3MessageRecycleAdapter extends RecyclerView.Adapter<Notification3MessageRecycleAdapter.ViewHolder> {

    private final static String TAG = Notification3MessageRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<NotificationModel> items;

    private String search;

    public void setSearch(String search) {
        this.search = search;
    }

    public String getSearch() {
        return search;
    }

    public Notification3MessageRecycleAdapter(Context context, List<NotificationModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<NotificationModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noti_device_day_msg, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        LogU.d(TAG, "onBindViewHolder() position=" + position);

        NotificationModel item = items.get(position);

        String notificationTypeString = item.getNotificationTypeString();
        NotificationKindType notificationKindType = item.getNotificationKindType();
        int iconResId = 0;
        int msgResId = 0;
        switch (notificationKindType) {
            case EVENT:
                EventType eventType = EventType.getType(notificationTypeString);
                iconResId = eventType.getIconResId();
                msgResId = eventType.getMsgResId();
                break;
            case CUSTOM_EVENT:
                CustomEventType customEventType = CustomEventType.getType(notificationTypeString);
                iconResId = customEventType.getIconResId();
                msgResId = customEventType.getMsgResId();
                break;
            case APP:
                AppNotificationType appNotificationType = AppNotificationType.getType(notificationTypeString);
                iconResId = appNotificationType.getIconResId();
                msgResId = appNotificationType.getMsgResId();
                break;
        }

        holder.txtTime.setText(item.getRegTime());

        if(iconResId > 0) {
            holder.iconType.setImageResource(iconResId);
        }

        if(iconResId == R.drawable.icon_noti_error) {
            holder.cardviewNoti.setSelected(true);
            holder.txtMsg.setTextColor(context.getColor(R.color.colorRed));
            holder.txtTime.setTextColor(context.getColor(R.color.colorRed));

        } else {
            holder.cardviewNoti.setSelected(false);
            holder.txtMsg.setTextColor(context.getColor(R.color.colorDark));
            holder.txtTime.setTextColor(context.getColor(R.color.colorDark));
        }

        if(msgResId > 0) {
            if (search != null && !search.isEmpty()) {
                ViewUtil.highlightSearchText(context, holder.txtMsg, context.getString(msgResId), search);

            } else {
                holder.txtMsg.setText(msgResId);
            }
        }

        holder.txtTime.requestLayout();
        holder.txtMsg.requestLayout();

    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_type)
        ImageView iconType;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_msg)
        TextView txtMsg;
        @BindView(R.id.cardview_noti)
        View cardviewNoti;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
