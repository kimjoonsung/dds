package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ssomai.android.scalablelayout.ScalableLayout;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ChangeBurGuideActivity;
import com.uxstory.dds.speedplus.activity.adpater.RestorationCountRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StateBurFragment extends AbstractBaseFragment {

    private final static String TAG = StateBurFragment.class.getSimpleName();

    @BindView(R.id.recycler_left_bur)
    RecyclerView recyclerLeftBur;

    @BindView(R.id.recycler_right_bur)
    RecyclerView recyclerRightBur;

    @BindView(R.id.btn_change_guide)
    Button btnChangeGuide;

    @BindView(R.id.txt_left_bur_name)
    TextView txtLeftBurName;

    @BindView(R.id.txt_right_bur_name)
    TextView txtRightBurName;

    @BindView(R.id.txt_left_bur_replace_date)
    TextView txt_left_bur_replace_date;
    @BindView(R.id.txt_right_bur_replace_date)
    TextView txt_right_bur_replace_date;

    @BindView(R.id.img_bur_left_state)
    ImageView img_bur_left_state;
    @BindView(R.id.img_bur_left_alert)
    ImageView img_bur_left_alert;

    @BindView(R.id.txt_left_bur_msg)
    TextView txt_left_bur_msg;
    @BindView(R.id.txt_left_bur_alert_msg)
    TextView txt_left_bur_alert_msg;
    @BindView(R.id.txt_left_bur_state_point)
    TextView txt_left_bur_state_point;

    @BindView(R.id.img_bur_right_state)
    ImageView img_bur_right_state;
    @BindView(R.id.txt_right_bur_msg)
    TextView txt_right_bur_msg;
    @BindView(R.id.img_bur_right_alert)
    ImageView img_bur_right_alert;
    @BindView(R.id.txt_right_bur_alert_msg)
    TextView txt_right_bur_alert_msg;
    @BindView(R.id.txt_right_bur_state_point)
    TextView txt_right_bur_state_point;


    @BindView(R.id.txt_left_bur_total_count)
    TextView txt_left_bur_total_count;
    @BindView(R.id.txt_right_bur_total_count)
    TextView txt_right_bur_total_count;


    @BindView(R.id.scalelayout_left_bur)
    ScalableLayout scalableLayoutLeftBur;
    @BindView(R.id.scalelayout_right_bur)
    ScalableLayout scalableLayoutRightBur;

    @BindView(R.id.txt_very_good)
    TextView txtVeryGood;

    private List<DataCountModel> leftBurCountList = new ArrayList<>();
    private RestorationCountRecycleAdapter recyclerAdapterLeft;

    private List<DataCountModel> rightBurCountList = new ArrayList<>();
    private RestorationCountRecycleAdapter recyclerAdapterRight;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_bur_state);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnChangeGuide.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivityForResult(new Intent(getActivity(), ChangeBurGuideActivity.class), 0);
            }
        });

        recyclerLeftBur.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapterLeft = new RestorationCountRecycleAdapter(getActivity(), leftBurCountList);
        recyclerLeftBur.setAdapter(recyclerAdapterLeft);


        recyclerRightBur.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapterRight = new RestorationCountRecycleAdapter(getActivity(), rightBurCountList);
        recyclerRightBur.setAdapter(recyclerAdapterRight);

//        getMainAcivity().updateToolbarMain(R.id.menu_dash, device);
//        onUpdateView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onUpdateView() {
        super.onUpdateView();

        getMainAcivity().updateToolbarSub(0, getDevice());

        ArrayList<DataCountModel> burInfos = DbQuery.selectLastBurInfo(getDevice().getSerial());

        if (burInfos != null && burInfos.size() > 0) {
            txtLeftBurName.setText(burInfos.get(0).getName());
            txtRightBurName.setText(burInfos.get(1).getName());

        } else {
            txtLeftBurName.setText("");
            txtRightBurName.setText("");
        }

        txtLeftBurName.requestLayout();
        txtRightBurName.requestLayout();

        List<Event> list = DbQuery.selectLastChangeBurDate(getDevice().getSerial());

        String leftBurChangeDate = "";
        String rightBurChangeDate = "";
        String bothBurChangeDate = "";

        for (Event event : list) {
            switch (event.getEventType()) {
                case CHANGE_LEFT_BUR:
                    leftBurChangeDate = event.getTime();
                    break;
                case CHANGE_RIGHT_BUR:
                    rightBurChangeDate = event.getTime();
                    break;
                case CHANGE_BOTH_BUR:
                    bothBurChangeDate = event.getTime();
                    break;
            }
        }

        long leftChangedTime = StringUtil.convertDateToTimeMillis(leftBurChangeDate);
        long rightChangedTime = StringUtil.convertDateToTimeMillis(rightBurChangeDate);
        long bothChangedTime = StringUtil.convertDateToTimeMillis(bothBurChangeDate);

        if(leftChangedTime < bothChangedTime) {
            leftBurChangeDate = bothBurChangeDate;
            leftChangedTime = bothChangedTime;
        }

        if(rightChangedTime < bothChangedTime) {
            rightBurChangeDate = bothBurChangeDate;
            rightChangedTime = bothChangedTime;
        }

        int normal = Constants.BUR_NORMAL_COUNT;
        int bad = Constants.BUR_WARNING_COUNT;

        leftBurCountList = DbQuery.selectMillingFinishRestorationCounts(getDevice().getSerial(), leftBurChangeDate);

        int leftCount = 0;
        for (DataCountModel model : leftBurCountList) {
            leftCount += model.getCount();
        }

        txt_left_bur_total_count.setText(String.valueOf(leftCount));

        setScalePointX(scalableLayoutLeftBur, txt_left_bur_state_point, leftCount);

        if (leftCount > bad) {
            img_bur_left_state.setImageResource(R.drawable.img_bur_l_03);
            txt_left_bur_msg.setVisibility(View.INVISIBLE);
            img_bur_left_alert.setVisibility(View.VISIBLE);
            txt_left_bur_alert_msg.setVisibility(View.VISIBLE);

        } else if (leftCount > normal) {
            img_bur_left_state.setImageResource(R.drawable.img_bur_l_02);
            txt_left_bur_msg.setText(R.string.txt_msg_bur_good);
            txt_left_bur_msg.setTextColor(getContext().getColor(R.color.colorGreen));
            txt_left_bur_msg.setVisibility(View.VISIBLE);
            img_bur_left_alert.setVisibility(View.INVISIBLE);
            txt_left_bur_alert_msg.setVisibility(View.INVISIBLE);

        } else {
            img_bur_left_state.setImageResource(R.drawable.img_bur_l_01);
            txt_left_bur_msg.setText(R.string.txt_msg_bur_very_good);
            txt_left_bur_msg.setTextColor(getContext().getColor(R.color.colorBlueLight));
            txt_left_bur_msg.setVisibility(View.VISIBLE);
            img_bur_left_alert.setVisibility(View.INVISIBLE);
            txt_left_bur_alert_msg.setVisibility(View.INVISIBLE);
        }

        txt_left_bur_msg.requestLayout();

        recyclerAdapterLeft.setList(leftBurCountList);
        recyclerAdapterLeft.notifyDataSetChanged();

        rightBurCountList = DbQuery.selectMillingFinishRestorationCounts(getDevice().getSerial(), rightBurChangeDate);

        int rightCount = 0;
        for (DataCountModel model : rightBurCountList) {
            rightCount += model.getCount();
        }

        txt_right_bur_total_count.setText(String.valueOf(rightCount));

        setScalePointX(scalableLayoutRightBur, txt_right_bur_state_point, rightCount);

        if (rightCount > bad) {
            img_bur_right_state.setImageResource(R.drawable.img_bur_r_03);
            txt_right_bur_msg.setVisibility(View.INVISIBLE);
            img_bur_right_alert.setVisibility(View.VISIBLE);
            txt_right_bur_alert_msg.setVisibility(View.VISIBLE);

        } else if (rightCount > normal) {
            img_bur_right_state.setImageResource(R.drawable.img_bur_r_02);
            txt_right_bur_msg.setText(R.string.txt_msg_bur_good);
            txt_right_bur_msg.setTextColor(getContext().getColor(R.color.colorGreen));
            txt_right_bur_msg.setVisibility(View.VISIBLE);
            img_bur_right_alert.setVisibility(View.INVISIBLE);
            txt_right_bur_alert_msg.setVisibility(View.INVISIBLE);

        } else {
            img_bur_right_state.setImageResource(R.drawable.img_bur_r_01);
            txt_right_bur_msg.setText(R.string.txt_msg_bur_very_good);
            txt_right_bur_msg.setTextColor(getContext().getColor(R.color.colorBlueLight));
            txt_right_bur_msg.setVisibility(View.VISIBLE);
            img_bur_right_alert.setVisibility(View.INVISIBLE);
            txt_right_bur_alert_msg.setVisibility(View.INVISIBLE);
        }

        txt_right_bur_msg.requestLayout();

        recyclerAdapterRight.setList(rightBurCountList);
        recyclerAdapterRight.notifyDataSetChanged();

        if (leftBurChangeDate.isEmpty()) {
            txt_left_bur_replace_date.setText("");
        } else {
            String date = getString(R.string.txt_replace_date) + "  " + StringUtil.convertLongToVisibleDate(leftChangedTime);
            txt_left_bur_replace_date.setText(date);
        }
        txt_left_bur_replace_date.requestLayout();

        if (rightBurChangeDate.isEmpty()) {
            txt_right_bur_replace_date.setText("");
        } else {
            String date = getString(R.string.txt_replace_date) + "  " + StringUtil.convertLongToVisibleDate(rightChangedTime);
            txt_right_bur_replace_date.setText(date);
        }
        txt_right_bur_replace_date.requestLayout();

        getDevice().setLeftBurUsingCount(leftCount);
        getDevice().setRightBurUsingCount(rightCount);
    }

    private void setScalePointX(ScalableLayout scaleLayout, TextView pointView, int orgCount) {

        final float maxLeft = 325f;
        final float minLeft = 11f;
        final float maxCount = 20f;

        float count = (orgCount > maxCount) ? maxCount : orgCount;
        float width = maxLeft - minLeft;
        float ratio = count / maxCount;
        float left = (width + minLeft) * ratio;
        left = (left < minLeft) ? minLeft : left;
        LogU.d(TAG, "left=" + left);

        scaleLayout.removeView(pointView);

        if (orgCount > Constants.BUR_WARNING_COUNT) {
            pointView.setBackgroundResource(R.drawable.img_point_03);

        } else if (orgCount > Constants.BUR_NORMAL_COUNT) {
            pointView.setBackgroundResource(R.drawable.img_point_02);

        } else {
            pointView.setBackgroundResource(R.drawable.img_point_01);
        }

        scaleLayout.addView(pointView, left, 411f, 26f, 52f);
        scaleLayout.setScale_TextSize(pointView, 14f);
        pointView.setText(String.valueOf(orgCount));
        pointView.requestLayout();
        scaleLayout.requestLayout();
    }

}
