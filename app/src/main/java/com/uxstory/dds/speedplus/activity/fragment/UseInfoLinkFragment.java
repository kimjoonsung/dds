package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.help.HelpRenderer;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import pe.warrenth.rxbus2.Subscribe;

/**
 * A simple {@link Fragment} subclass.
 */
public class UseInfoLinkFragment extends AbstractBaseFragment {

    private final static String TAG = UseInfoLinkFragment.class.getSimpleName();

    @BindView(R.id.layout_top_bar)
    ViewGroup layoutTopBar;

    @BindView(R.id.btn_accordion_01_close)
    ImageView btnClose;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.layout_body)
    ViewGroup layoutBody;

    private int toolbarTitleStringId;
    private int helpContentStringId;

    public void setHelpContentStringId(int toolbarTitleStringId, int helpContentStringId) {
        this.toolbarTitleStringId = toolbarTitleStringId;
        this.helpContentStringId = helpContentStringId;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_use_info_link);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        layoutTopBar.setSelected(true);
        layoutTopBar.setForeground(null);
        layoutTopBar.setClickable(false);
        btnClose.setVisibility(View.GONE);

        HelpRenderer.render(getContext(), getContext().getString(helpContentStringId), txtTitle, layoutBody, null);
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(toolbarTitleStringId, null);
    }
}
