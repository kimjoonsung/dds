package com.uxstory.dds.speedplus.model;

import com.uxstory.dds.speedplus.model.type.BurType;
import com.uxstory.dds.speedplus.model.type.RestorationType;

public class DataCountModel {

    RestorationType restorationType;
    BurType burType;
    String name;
    int count;
    String time;
    int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public BurType getBurType() {
        return burType;
    }

    public void setBurType(BurType burType) {
        this.burType = burType;
    }

    public RestorationType getRestorationType() {
        return restorationType;
    }

    public void setRestorationType(RestorationType restorationType) {
        this.restorationType = restorationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
