package com.uxstory.dds.speedplus.activity.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ChangeWaterGuideActivity;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.message.type.WaterSensingType;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StateWaterFragment extends AbstractBaseFragment {

    private final static String TAG = StateWaterFragment.class.getSimpleName();


    @BindView(R.id.button_water_change)
    Button btnWaterChange;

    @BindView(R.id.icon_flow_water)
    ImageView icon_flow_water;

    @BindView(R.id.icon_state_water)
    ImageView icon_state_water;

    @BindView(R.id.txt_flow_msg)
    TextView txt_flow_msg;

    @BindView(R.id.txt_flow_msg_add)
    TextView txt_flow_msg_add;

    @BindView(R.id.txt_replace_date)
    TextView txt_replace_date;

    @BindView(R.id.txt_water_state)
    TextView txtWaterState;

    @BindView(R.id.txt_water_state_msg)
    TextView txtWaterStateMsg;

    @BindView(R.id.txt_water_state_msg1)
    TextView txtWaterStateMsg1;

    @BindView(R.id.btn_change_guide)
    Button btnChangeGuide;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_water_state);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnChangeGuide.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivityForResult(new Intent(getActivity(), ChangeWaterGuideActivity.class), 0);
            }
        });

        ViewUtil.highlightTagText(getContext(), txtWaterStateMsg1, txtWaterStateMsg1.getText().toString(), R.color.colorBlueLight);
        btnWaterChange.setOnClickListener(v -> confirmWaterChange());

        getDevice().setWaterSensingType(WaterSensingType.NOPE);

//        onUpdateView();
    }

    @Override
    protected void onPopStack() {
        super.onPopStack();

        if (getDdsService() != null && getDdsService().requestWaterSensing(getDevice())) {
            icon_flow_water.setImageResource(R.drawable.img_water_03);
            txt_flow_msg.setTextColor(getContext().getColor(R.color.colorDark));
            txt_flow_msg.setText(R.string.txt_msg_water_flow_wait);
            txt_flow_msg_add.setText(R.string.txt_msg_water_flow_wait_add);
        }
    }

    private void confirmWaterChange() {
        AlertDialog.Builder builder = ViewUtil.getDialogBuilder(getContext(), getDevice().getName(), R.string.txt_msg_water_replace_question, R.drawable.icon_noti_water);
        builder.setPositiveButton(R.string.txt_yes, (dialog, i) -> {
            DbQuery.insertWaterChange(getDevice().getSerial());
            notiWaterChange();
            onUpdateView();
        });
        builder.setNegativeButton(R.string.txt_no, (dialog, i) -> dialog.dismiss());
        ViewUtil.showDialog(getContext(), builder);
    }

    private void notiWaterChange() {
        NotificationPublisher.showNotification(getContext(), getDevice(), AppNotificationType.WATER_CHANGED);
    }

    @Override
    protected void onUpdateView() {
        super.onUpdateView();
        getMainAcivity().updateToolbarSub(0, getDevice());
        updateWaterFlow();
        updateWaterTurbid();
    }

    private void updateWaterFlow() {
        WaterSensingType waterSensingType = getDevice().getWaterSensingType();
        switch (waterSensingType) {
            case ADEQUACY:
                icon_flow_water.setImageResource(R.drawable.img_water_01);
                txt_flow_msg.setTextColor(getContext().getColor(R.color.colorBlueLight));
                txt_flow_msg.setText(R.string.txt_msg_water_flow_adequacy);
                txt_flow_msg_add.setText("");
                break;
            case SHORTAGE:
                icon_flow_water.setImageResource(R.drawable.img_water_02);
                txt_flow_msg.setTextColor(getContext().getColor(R.color.colorRed));
                txt_flow_msg.setText(R.string.txt_msg_water_flow_warning);
                txt_flow_msg_add.setText("");
                break;
            default:
                setWaterFlowNope();
                break;
        }
    }

    private void setWaterFlowNope() {
        icon_flow_water.setImageResource(R.drawable.img_water_03);
        txt_flow_msg.setTextColor(getContext().getColor(R.color.colorDark));
        txt_flow_msg.setText(R.string.txt_msg_water_flow_nope);
        txt_flow_msg_add.setText(R.string.txt_msg_water_flow_nope_add);
    }

    private void updateWaterTurbid() {
        String date = DbQuery.selectLastWaterChangeDate(getDevice().getSerial());

        if (date != null && !date.isEmpty()) {
            txt_replace_date.setText(getString(R.string.txt_replace_date) + "  " + StringUtil.convertLongToVisibleDate(StringUtil.convertDateToTimeMillis(date)));
        } else {
            txt_replace_date.setText("");
        }
        txt_replace_date.requestLayout();

        long time = DbQuery.selectWorkingTimeAfterWaterChange(getDevice().getSerial(), date);
        LogU.d(TAG, "time=" + time);
        long normal = Constants.WATER_NORMAL_TIME * 3600;
        long bad = Constants.WATER_WARNING_TIME * 3600;

        if (time > bad) {
            icon_state_water.setImageResource(R.drawable.img_state_water_03);
            txtWaterState.setText(R.string.txt_water_mud);
            txtWaterState.setTextColor(getContext().getColor(R.color.colorRed));
            txtWaterStateMsg.setText(R.string.txt_msg_water_state_3);
            txtWaterStateMsg.setTextColor(getContext().getColor(R.color.colorRed));
        } else if (time > normal) {
            icon_state_water.setImageResource(R.drawable.img_state_water_02);
            txtWaterState.setText(R.string.txt_bur_normal);
            txtWaterState.setTextColor(getContext().getColor(R.color.colorGreen));
            txtWaterStateMsg.setText(R.string.txt_msg_water_state_2);
            txtWaterStateMsg.setTextColor(getContext().getColor(R.color.colorGreen));
        } else {
            icon_state_water.setImageResource(R.drawable.img_state_water_01);
            txtWaterState.setText(R.string.txt_water_good);
            txtWaterState.setTextColor(getContext().getColor(R.color.colorBlueLight));
            txtWaterStateMsg.setText(R.string.txt_msg_water_state_1);
            txtWaterStateMsg.setTextColor(getContext().getColor(R.color.colorBlueLight));
        }

        txtWaterState.requestLayout();
        txtWaterStateMsg.requestLayout();

        getDevice().setWaterWorkingTime(time);
    }

}
