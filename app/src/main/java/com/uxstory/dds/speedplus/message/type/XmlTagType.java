package com.uxstory.dds.speedplus.message.type;

public enum XmlTagType {
    NULL(""),
    DDS_CONTROL("DDSControl"),
    SESSION("session"),
    ENCRYPTION("encryption"),
    KEY("key"),
    EVENT("event"),
    COMMAND_RESPONSE("commandResponse"),
    MILLING_PROGRESS("millingProgress"),
    ;

    private String value;

    XmlTagType(String value) {
        this.value = value;
    }

    public String getString(){
        return value;
    }

    public static XmlTagType getType(String tag) {
        for (XmlTagType xmlTagType : XmlTagType.values()) {
            if (xmlTagType != DDS_CONTROL && xmlTagType.getString().equals(tag)) {
                return xmlTagType;
            }
        }
        return NULL;
    }
}
