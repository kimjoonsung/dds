package com.uxstory.dds.speedplus.application;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;

import androidx.multidex.MultiDexApplication;

import com.uxstory.dds.speedplus.activity.ui.ForegroundManager;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.database.DbOpenHelper;
import com.uxstory.dds.speedplus.service.DdsService;
import com.uxstory.dds.speedplus.service.DdsServiceImpl;
import com.uxstory.dds.speedplus.util.LogU;

public class GlobalApplication extends MultiDexApplication {

    private final static String TAG = GlobalApplication.class.getSimpleName();

    private static String deviceSerial;
    private static DdsService ddsService;

    private static float densityDpi;
    private static float density;
    private static float dpiMultiplier;

    private static int defualtUiOptions;

    private WifiManager.WifiLock wifiLock;
    private PowerManager.WakeLock wakeLock = null;

    @Override
    public void onCreate() {
        super.onCreate();

//        FileLogHandlerConfiguration fileHandler = LoggerConfiguration.fileLogHandler(this);
//        fileHandler.setFullFilePathPattern("/sdcard/SysLog/dds_log.log");
//        fileHandler.setRotateFilesCountLimit(9);
//        LoggerConfiguration.configuration().addHandlerToRootLogger(fileHandler);

        DbOpenHelper dbOpenHelper = new DbOpenHelper(this);
        if (!dbOpenHelper.open()) {
            System.exit(0);
            return;
        }

        GlobalApplication.loadAndroidDeviceSerial(this);
        ForegroundManager.init(this);

        Preference.loadPreferenceValue(getApplicationContext());

        startDdsService(this);

        setAcquireLock();
    }

    public void setAcquireLock() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock("speedPlusCareApp:wifilock");
        wifiLock.setReferenceCounted(true);
        wifiLock.acquire();

        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "speedPlusCareApp:wakelock");
        wakeLock.acquire();
    }

    public static void setDensityDpi(float densityDpi) {

        float mdpi = 160f;
        float defaultDpi = 320f;

        GlobalApplication.densityDpi = densityDpi;
        GlobalApplication.density = densityDpi / mdpi;
        GlobalApplication.dpiMultiplier = defaultDpi / densityDpi;
    }

    public static float getDensityDpi() {
        return densityDpi;
    }

    public static float getDensity() {
        return density;
    }

    public static float getDpiMultiplier() {
        return dpiMultiplier;
    }

    public static DdsService getDdsService() {
        return ddsService;
    }

    public static void setDdsService(DdsService ddsService) {
        GlobalApplication.ddsService = ddsService;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DbOpenHelper.close();

        if (wifiLock != null) {
            wifiLock.release();
            wifiLock = null;
        }

        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    public static void loadAndroidDeviceSerial(Context context) {
        deviceSerial = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getAndroidDeviceSerial() {
        return deviceSerial;
    }

    public static void startDdsService(Context context) {
        if (!DdsServiceImpl.isRunning()) {
            Intent intent = new Intent(context, DdsServiceImpl.class);
            context.startForegroundService(intent);

            GlobalApplication.bindDdsService(context, new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    DdsServiceImpl.ServiceBinder binder = (DdsServiceImpl.ServiceBinder) service;
                    DdsService ddsService = binder.getService();
                    LogU.d(TAG, "onServiceConnected() ddsService = " + ddsService);
                    if(ddsService != null) {
                        GlobalApplication.setDdsService(ddsService);
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {

                }
            });
        }
    }

    public static void bindDdsService(Context context, ServiceConnection serviceConnection) {
        Intent intent = new Intent(context, DdsServiceImpl.class);
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public static int getDefualtUiOptions() {
        return defualtUiOptions;
    }

    public static void setDefualtUiOptions(int defualtUiOptions) {
        GlobalApplication.defualtUiOptions = defualtUiOptions;
    }

    public static class BootReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                //nothing to do
            }
        }
    }
}
