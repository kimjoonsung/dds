package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.help.Contact;
import com.uxstory.dds.speedplus.util.LogU;

import butterknife.BindView;
import pe.warrenth.rxbus2.Subscribe;

/**
 * A simple {@link Fragment} subclass.
 */
public class UseInfoContactFragment extends AbstractBaseFragment {

    private final static String TAG = UseInfoContactFragment.class.getSimpleName();


    @BindView(R.id.txt_help_msg)
    TextView txtHelpMsg;

    @BindView(R.id.button_mail)
    Button btnMail;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_use_info_contact);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnMail.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Contact.sendEmail(getContext(), BuildConfig.DDS_EMAIL);
            }
        });

        String msg = String.format(getString(R.string.txt_use_info_help_msg), BuildConfig.DDS_EMAIL);

        ViewUtil.highlightTagText(getContext(), txtHelpMsg, msg, R.color.colorBlueLight);
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(R.string.txt_use_info_title_help, null);
    }
}
