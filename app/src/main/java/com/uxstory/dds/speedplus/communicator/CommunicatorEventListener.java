package com.uxstory.dds.speedplus.communicator;

public interface CommunicatorEventListener {
    void onCommunicatorEvent(CommunicatorEvent communicatorEvent);
}
