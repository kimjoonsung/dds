package com.uxstory.dds.speedplus.config;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.application.GlobalApplication;

public class Constants {
    public static final String INTENT_ACTION_GOTO_WATER_STATE = "com.uxstory.dds.speedplus.action.GOTO_WATER_STATE";
    public static final String INTENT_ACTION_GOTO_BUR_STATE = "com.uxstory.dds.speedplus.action.GOTO_BUR_STATE";
    public static final String INTENT_ACTION_GOTO_DASHBOARD = "com.uxstory.dds.speedplus.action.GOTO_DASHBOARD";
    public static final String INTENT_ACTION_GOTO_REQUEST_HELP = "com.uxstory.dds.speedplus.action.GOTO_REQUEST_HELP";
    public static final String INTENT_ACTION_WARN_LONG_TIME_NO_USE_DEVICE = "com.uxstory.dds.speedplus.action.WARN_LONG_TIME_NO_USE_DEVICE";

    public static final String INTENT_EXTRA_NAME_SERIAL = "serial";
    public static final String INTENT_EXTRA_NAME_IS_RECORD_NOTI = "isRecordNoti";
    public static final String INTENT_EXTRA_NAME_NOTI_ROW_ID = "notiRowId";

    public static final String DDS_NOTIFICATION_CHANNEL_SERVICE_ID = "dds_notification_channel_id_service";
    public static final String DDS_NOTIFICATION_CHANNEL_SERVICE_NAME = "Speed+ Care Network Communication Service.";

    public static final String DDS_NOTIFICATION_CHANNEL_NOTI_ID = "dds_notification_channel_id_noti";
    public static final String DDS_NOTIFICATION_CHANNEL_NOTI_NAME = "Speed+ Care Notification";

    public static final int DDS_NOTIFICATION_ID_SERVICE = 1;
    public static final int DDS_NOTIFICATION_ID_NOTI = 2;

    public static final int DEVICE_PORT = BuildConfig.DEVICE_PORT;
    public final static String DDS_URI_PREFIX = BuildConfig.DDS_URI + "/";


    public static final String CHARSET_UTF_16LE = "UTF-16LE";
    public static final String CHARSET_UTF_8 = "UTF-8";

    public static final String RX_BUS_EVENT_DEVICE = "rx_event_device";
    public static final String RX_BUS_EVENT_USE_INFO_LINK = "rx_event_use_info_link";
    public static final String RX_BUS_EVENT_MAIL_TO = "rx_event_mail_to";

    public static final String RX_BUS_EVENT_NEXT_PAGE = "rx_event_next_page";
    public static final String RX_BUS_EVENT_CLOSE = "rx_event_close";

    public static final String USE_INFO_LINK_CONTACT = "_contact_";

    public static final int BUR_WARNING_COUNT = 14;
    public static final int BUR_NORMAL_COUNT = 7;

    public static final int WATER_WARNING_TIME = 9;
    public static final int WATER_NORMAL_TIME = 4;

}
