package com.uxstory.dds.speedplus.help;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ssomai.android.scalablelayout.ScalableLayout;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.ScaleUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelpRenderer {

    private final static String TAG = HelpRenderer.class.getSimpleName();

    private static final String HELP_TITLE_TAG = "{*title=";
    private static final String HELP_CONTENTS_TAG = "{*contents=";
    private static final String HELP_CONTENTS_CENTER_TAG = "{*contents_center=";
    private static final String HELP_DRAWABLE_TAG = "{*drawable=";
    private static final String HELP_END_TAG = "*}";

    private static final String HELP_DRAWABLE_PATTERN = "\\{\\*drawable=.+\\*\\}";

    private static final float SCALE_WIDTH = 752f;
    private static final float VERTICAL_MARGIN = 34f;
    private static final float TEXT_WIDTH = 640f;
    private static final float TEXT_WIDTH_EN = 680f;
    private static final float TEXT_LINESPACING_EXTRA = 12f;
    private static final float TEXT_SIZE = 16f;
    private static final float IMAGE_MARGIN_TOP = 10f;

    private static String clearHelpDrawble(String content) {
        Pattern pattern = Pattern.compile(HELP_DRAWABLE_PATTERN);
        Matcher matcher = pattern.matcher(content);
        while(matcher.find()) {
            String matchStr = matcher.group();
            LogU.d(TAG, "clearHelpDrawble() matchStr=" + matchStr);
            content = content.replace(Matcher.quoteReplacement(matchStr), "");
        }
        return content;
    }

    public static String clearHelpTags(String content) {
        content = clearHelpDrawble(content);
        content = content.replaceAll(Pattern.quote(HelpRenderer.HELP_TITLE_TAG), "");
        content = content.replaceAll(Pattern.quote(HelpRenderer.HELP_CONTENTS_TAG), "");
//        content = content.replaceAll(Pattern.quote(HelpRenderer.HELP_DRAWABLE_TAG), "");
        content = content.replaceAll(Pattern.quote(HelpRenderer.HELP_CONTENTS_CENTER_TAG), "");
        content = content.replaceAll(Pattern.quote(HelpRenderer.HELP_END_TAG), "");
        return content;
    }

    private static String getHelpTitle(String str) {
        return str.replace(Matcher.quoteReplacement(HELP_TITLE_TAG), "").trim();
    }

    private static String getHelpContents(String str) {
        return str.replace(Matcher.quoteReplacement(HELP_CONTENTS_TAG), "");
    }

    private static String getHelpCenterContents(String str) {
        return str.replace(Matcher.quoteReplacement(HELP_CONTENTS_CENTER_TAG), "");
    }

    private static Drawable getHelpDrawable(Context context, String str) {
        Drawable drawable = null;
        String drawableName = str.replace(Matcher.quoteReplacement(HELP_DRAWABLE_TAG), "").trim();
        int drawableId = context.getResources().getIdentifier(drawableName, "drawable", context.getPackageName());
        if (drawableId > 0) {
            drawable = context.getDrawable(drawableId);
        }
        return drawable;
    }

    public static void render(Context context, String help, TextView txtTitle, ViewGroup layoutBody, String searchStr) {
        ScalableLayout sl;

        String[] contentsArray = help.split(Pattern.quote(HELP_END_TAG));

        for (String content : contentsArray) {
//            content = content.replaceAll("^\\s+",""); //ltrim
            content = content.trim();

            if (content.indexOf(HELP_TITLE_TAG) == 0) {
//                ViewUtil.highlightTagText(context, txtTitle, getHelpTitle(content), searchStr);
                txtTitle.setText(getHelpTitle(content));

            } else if (content.indexOf(HELP_DRAWABLE_TAG) == 0) {
                Drawable drawable = getHelpDrawable(context, content);
                if(drawable != null) {
                    ImageView iv = new ImageView(context);
                    iv.setImageDrawable(drawable);

                    float height = ScaleUtil.convertValue4Scale(drawable.getIntrinsicHeight());
                    float width = ScaleUtil.convertValue4Scale(drawable.getIntrinsicWidth());

                    LogU.d(TAG, "image height=" + height + ", width=" + width);

                    sl = new ScalableLayout(context, SCALE_WIDTH, height + IMAGE_MARGIN_TOP);
                    sl.addView(iv, 0f, IMAGE_MARGIN_TOP, width, height);

                    ((ScalableLayout.LayoutParams) iv.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
                    iv.requestLayout();
                    layoutBody.addView(sl);
                }

            } else {
                sl = new ScalableLayout(context, SCALE_WIDTH, 0f);
                LogU.d(TAG, "content=" + content);
                TextView tv = new TextView(context);
                tv.setLineSpacing(TEXT_LINESPACING_EXTRA, 1);

                if (content.contains(HELP_CONTENTS_CENTER_TAG)) {
                    tv.setGravity(Gravity.CENTER_HORIZONTAL);
                    content = getHelpCenterContents(content);

                } else {
                    content = getHelpContents(content);
                }

                ViewUtil.highlightTagText(context, tv, content, searchStr, 0);
//                        tv.setText(content);
                float width = (ViewUtil.isKoreanLanguage()) ? TEXT_WIDTH : TEXT_WIDTH_EN;
                sl.addView(tv, (SCALE_WIDTH - width) / 2, 0f, width, 0f);
                sl.setTextView_WrapContent(tv, ScalableLayout.TextView_WrapContent_Direction.Bottom, true);
                sl.setScale_TextSize(tv, TEXT_SIZE);
                layoutBody.addView(sl);
                tv.requestLayout();
            }

        }

        sl = new ScalableLayout(context, SCALE_WIDTH, VERTICAL_MARGIN);
        layoutBody.addView(sl);
    }

}
