package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.TutorialModel;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialRecycleAdapter extends RecyclerView.Adapter<TutorialRecycleAdapter.ViewHolder> {

    private final static String TAG = TutorialRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<TutorialModel> items;

    public TutorialRecycleAdapter(Context context, List<TutorialModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<TutorialModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tutorial, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        TutorialModel item = items.get(position);
        holder.imageTutorial.setImageResource(item.getImgId());
        holder.txtTitle.setText(item.getTxtTitleId());
        holder.txtMsg.setText(item.getTxtMsgId());

//        ViewUtil.requestLayout((ViewGroup)holder.itemView);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_tutorial)
        ImageView imageTutorial;

        @BindView(R.id.txt_title)
        TextView txtTitle;

        @BindView(R.id.txt_msg)
        TextView txtMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
