package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.Notification1DeviceRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.OnScrollValueListener;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends AbstractBaseFragment implements OnScrollValueListener {

    private final static String TAG = NotificationFragment.class.getSimpleName();

    @BindView(R.id.recycler_noti_device)
    RecyclerView recyclerView;

    @BindView(R.id.view_no_noti)
    View noData;

    @BindView(R.id.global_top_bar)
    View notiTopBar;

    @BindView(R.id.global_top_bar_title)
    TextView notiTopBarTitle;

    @BindView(R.id.global_top_bar_sub_title)
    TextView notiTopBarSubInfo;

    @BindView(R.id.btn_top_bar_close)
    View btnTopBarClose;

    private RecyclerView.OnScrollListener onScrollListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_notification);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        getMainAcivity().updateToolbarMain(R.id.menu_noti);
    }

    @Override
    public void onStart() {
        super.onStart();
        LogU.d(TAG, "onStart() " + this);

        for (Device device : getDdsService().getDeviceList()) {
            device.clearNotiCount();
            NotificationPublisher.setCheckedAllNotification(device);
        }

        notiTopBar.setSelected(true);

        notiTopBar.setOnClickListener(v -> {
            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int position = layoutManager.findFirstVisibleItemPosition();

            if (position >= 0) {
                recyclerView.scrollToPosition(position);
                lastView = null;
                setNotiTopBar("");

                View view = layoutManager.findViewByPosition(position);
                View topBar = view.findViewById(R.id.layout_top_bar);
                if(topBar != null) {
                    topBar.setPressed(true);
                    topBar.setPressed(false);
                }
            }
        });

        btnTopBarClose.setOnClickListener(v -> {
            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int position = layoutManager.findFirstVisibleItemPosition();

            if (position >= 0) {
                recyclerView.scrollToPosition(position);
                lastView = null;
                setNotiTopBar("");

                View view = layoutManager.findViewByPosition(position);
                View topBar = view.findViewById(R.id.layout_top_bar);
                if(topBar != null) {
                    topBar.performClick();
                    topBar.setPressed(true);
                    topBar.setPressed(false);
                }
            }

        });


        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());

                View firstView = layoutManager.findViewByPosition(0);
                if (firstView != null && firstView.getTop() >= 0) {
                    setNotiTopBar("");
                    lastView = null;
                    return;
                }

                int position = layoutManager.findFirstVisibleItemPosition();
                View view = layoutManager.findViewByPosition(position);

                int top = view.getTop();
                int bottom = top + view.getHeight();

                if (top < 0 && bottom > 0) {
                    if (lastView != view) {
                        NotificationModel model = list.get(position);
                        if(model.isSelected()) {
                            setNotiTopBar(model.getName());
                            notiTopBarSubInfo.setText("");
                        } else {
                            setNotiTopBar("");
                        }
                        lastView = view;
                    }
                } else {
                    setNotiTopBar(null);
                    lastView = null;
                }
            }
        };

        setNotificationList();
    }


    @Override
    public void onStop() {
        super.onStop();
        recyclerView.removeOnScrollListener(onScrollListener);
    }

    public void setNotiTopBar(String title) {
//        LogU.d(TAG, "setNotiTopBar=" + visible);
        ViewUtil.setVisibility(notiTopBar, title != null && !title.isEmpty());
        notiTopBarTitle.setText(title);
//        if (!visible) {
//            notiTopBarSubInfo.setText("");
//        }
    }

    @Override
    public void onScrollValue(String value1, String value2) {
//        LogU.d(TAG, "setNotiTopBar value1=" + value1 + " ,value2=" + value2);
        if (value1.equals(notiTopBarTitle.getText())) {
            notiTopBarSubInfo.setText(value2);
        }
    }

    @Override
    protected void onUpdateView() {
    }

    private View lastView;
    List<NotificationModel> list = new ArrayList<>();

    private void setNotificationList() {
        list = DbQuery.selectNotiDeviceList(null);

        if (list.size() > 0) {
            noData.setVisibility(View.GONE);
            list.get(0).setSelected(true);
            Notification1DeviceRecycleAdapter recyclerAdapter = new Notification1DeviceRecycleAdapter(getActivity(), list, this);
            recyclerView.setAdapter(recyclerAdapter);
            recyclerAdapter.notifyDataSetChanged();

            recyclerView.scrollToPosition(0);

        } else {
            noData.setVisibility(View.VISIBLE);
        }

        lastView = null;
        recyclerView.addOnScrollListener(onScrollListener);
    }



}
