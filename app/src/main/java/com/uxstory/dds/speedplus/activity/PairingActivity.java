package com.uxstory.dds.speedplus.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.PairingDeviceRecycleAdapter;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.upnp.ControlPointListener;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.util.LogU;

import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.UPnP;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import pe.warrenth.rxbus2.RxBus;
import pe.warrenth.rxbus2.Subscribe;

public class PairingActivity extends AbstractBaseActivity {

    private final static String TAG = PairingActivity.class.getSimpleName();

    @BindView(R.id.layout_msg_wait)
    View viewMsgWait;

    @BindView(R.id.layout_product_link)
    View viewProductLink;

    @BindView(R.id.btn_close)
    Button btnClose;

    @BindView(R.id.btn_cancel)
    Button btnCancel;

    @BindView(R.id.btn_retry)
    Button btnRetry;

    @BindView(R.id.img_product_link)
    ImageView imgProductLink;

    @BindView(R.id.img_product_link_ani)
    LottieAnimationView imgProductLinkAni;

    @BindView(R.id.txt_msg_wait)
    TextView txtMsgWait;

    @BindView(R.id.txt_msg_find_device)
    TextView txtMsgFindDevice;

    @BindView(R.id.recycler_pairing_devices)
    RecyclerView recyclerView;

    List<Device> deviceList = new ArrayList<>();
    Thread searchThread;

    PairingDeviceRecycleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing);

        Window window = getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        recyclerView.setVisibility(View.GONE);
//        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        btnClose.setOnClickListener(view -> finish());

        btnCancel.setOnClickListener(view -> finish());

        btnRetry.setOnClickListener(view -> findDevice());
    }

    @Override
    protected void onStart() {
        super.onStart();
        RxBus.get().register(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        adapter = new PairingDeviceRecycleAdapter(this, deviceList);
        recyclerView.setAdapter(adapter);
        findDevice();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        RxBus.get().unResister(this);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (searchThread != null) {
            searchThread.interrupt();
            searchThread = null;
        }

//        for (Device device : deviceList) {
//            if (!device.isConnected()) {
//                Communicator communicator = device.getCommunicator();
//                if (communicator != null) {
//                    communicator.disconnect();
//                    communicator.setCommunicatorEventListener(null);
//                }
//            }
//        }

        finish();
    }

    private void findDevice() {

        btnClose.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
        txtMsgFindDevice.setText(R.string.txt_msg_find_device);

        imgProductLink.setVisibility(View.INVISIBLE);
        imgProductLinkAni.setVisibility(View.VISIBLE);
        txtMsgWait.setText(R.string.txt_msg_wait);

        btnCancel.setText(R.string.txt_cancel);
        btnCancel.setVisibility(View.VISIBLE);

        UPnP.setEnable(UPnP.USE_ONLY_IPV4_ADDR);
        ControlPoint controlPoint = new ControlPoint();

        ControlPointListener listener = new ControlPointListener();
        listener.setDeviceBinder(new ControlPointListener.DeviceBinder() {

            List<Device> pairedDeviceList;

            @Override
            public void onAdd(String ip, int port, String serial) {
                LogU.d(TAG, "Device onAdd -----------");
                LogU.d(TAG, "ip: " + ip);
                LogU.d(TAG, "port: " + port);
                LogU.d(TAG, "serial: " + serial);

                pairedDeviceList = DbQuery.selectDeviceList(true);

                if (!isAlreadyAddDevice(serial) && !isExistPairedDevice(serial)) {
                    Device item = new Device();
                    item.setIp(ip);
                    item.setPort(Constants.DEVICE_PORT);
                    item.setSerial(serial);
                    item.setWorkStateType(WorkStateType.ON_ADD);
                    deviceList.add(item);
                }
            }

            @Override
            public void onRemove(String serial) {
                for (Device item : deviceList) {
                    if (serial.equals(item.getSerial())) {
                        deviceList.remove(item);
                        break;
                    }
                }
            }

            private boolean isAlreadyAddDevice(String serial) {
                for (Device item : deviceList) {
                    if (serial.equals(item.getSerial())) {
                        return true;
                    }
                }
                return false;
            }

            private boolean isExistPairedDevice(String serial) {
                for (Device item : pairedDeviceList) {
                    if (serial.equals(item.getSerial())) {
                        return true;
                    }
                }
                return false;
            }

        });

        controlPoint.addSearchResponseListener(listener);
        controlPoint.addNotifyListener(listener);

        searchThread = new Thread(() -> {
            controlPoint.start();
            try {
                Thread.sleep(BuildConfig.UPNP_TIMEOUT * 1000);
                runOnUiThread(() -> onEndFindDevice());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (controlPoint != null) {
                controlPoint.stop();
            }
        });
        searchThread.setDaemon(true);
        searchThread.start();
    }

    public void onEndFindDevice() {
        if (deviceList.size() > 0) {
            viewMsgWait.setVisibility(View.GONE);
            viewProductLink.setVisibility(View.GONE);
            btnCancel.setText(R.string.txt_close);
            txtMsgFindDevice.setText(R.string.txt_msg_select_device_connect);
            txtMsgWait.setVisibility(View.GONE);
            imgProductLink.setVisibility(View.GONE);
            imgProductLinkAni.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.getAdapter().notifyDataSetChanged();

        } else {
            imgProductLink.setVisibility(View.VISIBLE);
            imgProductLinkAni.setVisibility(View.INVISIBLE);
            txtMsgFindDevice.setText(R.string.txt_msg_no_device_pairing_retry);
            txtMsgWait.setText(R.string.txt_msg_wait_retry);
            btnClose.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.GONE);
        }
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_DEVICE)
    public void onRxBusEvent(CommunicatorEvent communicatorEvent) {
        LogU.d(TAG, "onRxBusEvent() = " + communicatorEvent.getResponseType() + "," + this);
        switch (communicatorEvent.getResponseType()) {
            case DISCONNECTED:
                for(Device device : deviceList) {
                    if(device.getSerial().equals(communicatorEvent.getDevice().getSerial())) {
                        txtMsgFindDevice.setText(R.string.txt_msg_fail_retry);
                        break;
                    }
                }
            case CONNECTED:
            case SYNC_EVENT_HISTORY:
                recyclerView.getAdapter().notifyDataSetChanged();
                break;
            case SESSION:
                txtMsgFindDevice.setText(R.string.txt_msg_confirm_device);
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
