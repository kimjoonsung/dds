package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.ssomai.android.scalablelayout.ScalableLayout;
import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.PairingActivity;
import com.uxstory.dds.speedplus.activity.adpater.DeviceRecyclerAdapter;
import com.uxstory.dds.speedplus.activity.ui.ComparatorDeviceLastEventTime;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.util.DateUtil;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.IntStream;

import butterknife.BindView;

public class DashBoardFragment extends AbstractBaseFragment {

    private final static String TAG = DashBoardFragment.class.getSimpleName();

    @BindView(R.id.recycler_devices)
    RecyclerView recyclerDevices;

    @BindView(R.id.btn_01_s)
    Button btnAdd;

    @BindView(R.id.txt_pre_date_milling_count_total)
    TextView txtPreMillingCount;

    @BindView(R.id.txt_date_milling_count_total)
    TextView txtMillingCount;

    @BindView(R.id.text_today)
    TextView txtToday;

    @BindView(R.id.dash_pie_chart)
    PieChart pieChart;

    @BindView(R.id.dash_bar_chart)
    HorizontalBarChart barChart;

    @BindView(R.id.layout_dash_pre_date)
    View viewPredayCount;

    @BindView(R.id.txt_msg_pre_date_milling_count_total)
    TextView txtPreDay;

    @BindView(R.id.view_no_data_pre_day)
    View noDataPreDay;

    @BindView(R.id.view_no_data_pie)
    View noDataPie;

    @BindView(R.id.layout_dash_to_date)
    View viewTodayCount;

    @BindView(R.id.cardview_today_count_chart)
    View barChartView;

    @BindView(R.id.layout_dash_bar_chart_scale)
    ScalableLayout layoutDashBarChartScale;

    private List<Device> deviceList = new ArrayList<>();
    private DeviceRecyclerAdapter deviceRecyclerAdapter;

    private Thread refreshThread;

    private float multiplier = 1f;

    private void startRefreshDashBoard() {
        if (BuildConfig.INTERVAL_REFRESH_DASHBOARD > 0) {
            refreshThread = new Thread(() -> {
                try {
                    while (!refreshThread.isInterrupted()) {
//                    LogU.d(TAG, "elapsedTimeCount()");
                        Thread.sleep(BuildConfig.INTERVAL_REFRESH_DASHBOARD * 1000);
                        if (DashBoardFragment.this.getActivity() != null) {
                            DashBoardFragment.this.getActivity().runOnUiThread(() -> {
                                updateData();
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            });
            refreshThread.setDaemon(true);
            refreshThread.start();
        }
    }

    private void stopRefreshDeashBoard() {
        if (refreshThread != null) {
            refreshThread.interrupt();
            refreshThread = null;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_dash);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerDevices.setLayoutManager(layoutManager);

        deviceRecyclerAdapter = new DeviceRecyclerAdapter(getActivity(), deviceList);
        deviceRecyclerAdapter.setBaseFragment(this);
        recyclerDevices.setAdapter(deviceRecyclerAdapter);

        btnAdd.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivityForResult(new Intent(getActivity(), PairingActivity.class), 0);
            }
        });

        String currentDate = StringUtil.getCurrentDate();
        txtToday.setText(StringUtil.convertDateToDisplayFormat(currentDate));

        String preDate = StringUtil.convertDateToMonthDay(DateUtil.getDateStringAddDate(Calendar.DATE, -1, DateUtil.getCalendar(currentDate)));
        txtPreDay.setText(String.format(getString(R.string.txt_milling_count_total), StringUtil.convertDateToDisplayFormat(preDate)));

        multiplier = GlobalApplication.getDpiMultiplier();

        onUpdateView();
    }

    @Override
    public void onStart() {
        super.onStart();
//        registerConfigs();
    }

    @Override
    public void onStop() {
        super.onStop();
//        unregisterConfigs();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        startRefreshDashBoard();

        if (!Preference.getPreferenceValue(Preference.PreferenceType.FIRST_PAIRING)) {
            Preference.setPreferenceValue(getContext(), Preference.PreferenceType.FIRST_PAIRING, true);
            NotificationPublisher.showNotification(getContext(), null, AppNotificationType.FIRST_PAIRING);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LogU.d(TAG, "onPause()");
        stopRefreshDeashBoard();
    }

//    @Override
//    @Subscribe(eventTag = Constants.RX_BUS_EVENT_DEVICE)
//    public void onRxBusEvent(CommunicatorEvent communicatorEvent) {
//        LogU.d(TAG, "onRxBusEvent() = " + communicatorEvent.getResponseType());
//    }

    @Override
    protected void onUpdateView() {
        LogU.d(TAG, "onUpdateView() " + this);
        getMainAcivity().updateToolbarMain(R.id.menu_dash);
        updateData();
    }

    private void updateData() {
        updateTotalCount();
        updateDeviceListView();
        updatePieChart();
        updateBarChart();
    }

    private int todayCount;

    private void updateTotalCount() {
        int preDayCount = DbQuery.selectLastMillingFinishCount(1);
        todayCount = DbQuery.selectLastMillingFinishCount(0);
        txtPreMillingCount.setText(String.valueOf(preDayCount));
        txtMillingCount.setText(String.valueOf(todayCount));

//        if (preDayCount > 0) {
//            noDataPreDay.setVisibility(View.GONE);
//            viewPredayCount.setVisibility(View.VISIBLE);
//        } else {
//            noDataPreDay.setVisibility(View.VISIBLE);
//            viewPredayCount.setVisibility(View.GONE);
//        }

        if (todayCount > 0) {
            noDataPie.setVisibility(View.GONE);
            viewTodayCount.setVisibility(View.VISIBLE);
        } else {
            noDataPie.setVisibility(View.VISIBLE);
            viewTodayCount.setVisibility(View.GONE);
        }
    }

    private void updateDeviceListView() {
        if (getDdsService() != null) {
            for (Device device : getDdsService().getDeviceList()) {
                getDdsService().checkWaterTurbidState(device, false);
                getDdsService().checkBurState(device, false);
            }
            deviceList.clear();
            deviceList.addAll(getDdsService().getDeviceList());
            deviceList.sort(new ComparatorDeviceLastEventTime());

            deviceRecyclerAdapter.setList(deviceList);
            deviceRecyclerAdapter.notifyItemRangeChanged(0, deviceList.size());

            if (deviceList.size() >= BuildConfig.MAXIMUM_DEVICE_PAIRING_COUNT) {
                btnAdd.setEnabled(false);
            }
        }
    }

    private void updatePieChart() {

        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(0, 0, 0, 0);
        pieChart.setDragDecelerationFrictionCoef(0.95f * multiplier);
        pieChart.setDrawEntryLabels(false);
        pieChart.setDrawHoleEnabled(false);
//        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(61f * multiplier);


        ArrayList<DataCountModel> list = DbQuery.selectRestorationCountList(StringUtil.getCurrentDate());
        ArrayList<PieEntry> yValues = new ArrayList<>();
        final int maxStrLength = 17;

        int[] colors = new int[list.size()];
        int i = 0;
        for (DataCountModel model : list) {
            colors[i++] = getContext().getColor(model.getRestorationType().getColorResId());
            String restoration = getString(model.getRestorationType().getNameResId());
            if (restoration.length() < maxStrLength) {
                restoration = String.format("%-" + (maxStrLength + 3) + "s", restoration);
//                restoration += "|";
            }
            yValues.add(new PieEntry(model.getCount(), restoration));
        }

//        pieChart.animateY(1000, Easing.EaseInOutCubic); //애니메이션

        PieDataSet dataSet = new PieDataSet(yValues, "");
//        dataSet.setSliceSpace(3f);
//        dataSet.setSelectionShift(5f);
        dataSet.setValueTextSize(13f * multiplier);
//        dataSet.setColors(getContext().getResources().getIntArray(R.array.chart_pie_color_array));
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueTextSize(10f * multiplier);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new LargeValueFormatter());

        Legend legend = pieChart.getLegend();
        legend.setDrawInside(false);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setTextSize(13f * multiplier);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setYOffset(-30f * multiplier);

        pieChart.setData(data);
        pieChart.invalidate();
    }

    private void updateBarChart() {

        if (barChart.getBarData() != null) {
            barChart.getBarData().clearValues();
            barChart.clearValues();
        }
        barChart.clear();
        barChart.notifyDataSetChanged();
        barChart.setFitBars(true);
        barChart.fitScreen();
        barChart.resetViewPortOffsets();
        barChart.resetZoom();
        barChart.invalidate();

        if (getDdsService() != null && getDdsService().getDeviceList().size() < 2 || todayCount < 1) {
            barChartView.setVisibility(View.GONE);
            return;

        } else {
            barChartView.setVisibility(View.VISIBLE);
        }

//        barChart.animateY(1000);

        ArrayList<DataCountModel> list = DbQuery.selectMillingFinishDeviceCounts(StringUtil.getCurrentDate());

//        list.clear();
//        for (int i = 0; i < 10; i++) {
//            DataCountModel m = new DataCountModel();
//            m.setCount((int)(Math.random() * 20) + 1);
//            m.setName("Speed+ " + (10 - i));
//            list.add(m);
//        }

        float chartHeight = 30f * list.size();
        layoutDashBarChartScale.setScaleHeight(150f + chartHeight);
        layoutDashBarChartScale.requestLayout();
        ((ScalableLayout.LayoutParams) barChart.getLayoutParams()).setScale_Height(80f + chartHeight);
        barChart.requestLayout();

        ArrayList<BarEntry> entries = new ArrayList<>();
        String[] labels = new String[list.size()];
        int i = 0;
        int maxCount = 0;
        for (DataCountModel model : list) {
            maxCount = (maxCount < model.getCount()) ? model.getCount() : maxCount;
            entries.add(new BarEntry(i, model.getCount()));
            labels[i] = model.getName();
            i++;
        }

        maxCount = (maxCount % 2 == 0) ? maxCount + 2 : maxCount + 1;
        int labelCount = (maxCount < 5) ? maxCount + 1 : 4;

        int[] colors = getContext().getResources().getIntArray(R.array.chart_device_color_array);
        int[] subColors = Arrays.copyOfRange(colors, 0, list.size());
        int[] newColors = IntStream.rangeClosed(1, subColors.length).map(operand -> subColors[subColors.length-operand]).toArray();

        BarDataSet barDataSet = new BarDataSet(entries, "");
        barDataSet.setValueFormatter(new LargeValueFormatter());
        barDataSet.setBarBorderWidth(0);
        barDataSet.setDrawValues(false);
        barDataSet.setColors(newColors);
        barDataSet.setValueTextSize(13f * multiplier);
        barDataSet.setValueTextColor(Color.DKGRAY);
        barDataSet.setHighlightEnabled(false);

        BarData barData = new BarData(barDataSet);
        barData.setValueTextSize(13f * multiplier);
        barData.setValueTextColor(Color.DKGRAY);
        barData.setDrawValues(true);
        barData.setBarWidth(0.5f);
//        barData.setValueFormatter(new LargeValueFormatter());
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return (value == 0) ? "" : String.valueOf((int) value);
            }
        };
        barData.setValueFormatter(formatter);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f);
//        xAxis.setGranularityEnabled(true);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        xAxis.setTextSize(13f * multiplier);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisLineColor(getContext().getColor(R.color.colorGrayLine1));
        xAxis.setAxisLineWidth(1.5f);
        xAxis.setDrawAxisLine(false);
        xAxis.setLabelCount(entries.size());
        xAxis.setAxisMaximum(entries.size());
        xAxis.setAvoidFirstLastClipping(false);
//        xAxis.setSpaceMin(1f * multiplier);
//        xAxis.setSpaceMax(1f * multiplier);
//        xAxis.setCenterAxisLabels(true);
//        xAxis.setXOffset(5f);
        xAxis.setYOffset(3f);

        YAxis yAxis = barChart.getAxisRight();
//        yAxis.setAxisMaximum(6f);
        yAxis.setDrawGridLines(true);
        yAxis.setGridColor(getContext().getColor(R.color.colorGrayLine1));
        yAxis.setGridLineWidth(1.5f * multiplier);
        yAxis.setTextSize(12f * multiplier);
        yAxis.setTextColor(getContext().getColor(R.color.colorGray));
        yAxis.setDrawZeroLine(true);
        yAxis.setZeroLineColor(getContext().getColor(R.color.colorGrayLine1));
        yAxis.setAxisMinimum(0f);
        yAxis.setDrawAxisLine(false);
//        yAxis.setValueFormatter(new ValueFormatter() {
//            @Override
//            public String getFormattedValue(float value) {
//                return String.valueOf((int)value);
//            }
//        });
        yAxis.setLabelCount(labelCount, maxCount < 5);
        yAxis.setAxisMaximum(maxCount);

        yAxis = barChart.getAxisLeft();
        yAxis.setDrawLabels(false);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawZeroLine(false);
        yAxis.setAxisMinimum(0f);
        yAxis.setDrawAxisLine(false);
//        yAxis.setValueFormatter(new ValueFormatter() {
//            @Override
//            public String getFormattedValue(float value) {
//                return String.valueOf((int)value);
//            }
//        });
        yAxis.setLabelCount(labelCount, maxCount < 5);
        yAxis.setAxisMaximum(maxCount);

        barChart.getDescription().setEnabled(false);
        barChart.setDrawGridBackground(false);
        barChart.getLegend().setEnabled(false);
        barChart.setPinchZoom(false);
        barChart.setScaleEnabled(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setData(barData);
        barChart.setFitBars(true);
//        barChart.setVisibleXRange(2, 10);
//        barChart.animateXY(5000, 5000);
        barChart.invalidate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0) {
            onUpdateView();
        }
    }


}
