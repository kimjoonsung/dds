package com.uxstory.dds.speedplus.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.NotificationPopDeviceRecycleAdapter;
import com.uxstory.dds.speedplus.activity.adpater.NotificationPopMessageRecycleAdapter;
import com.uxstory.dds.speedplus.activity.fragment.AbstractBaseFragment;
import com.uxstory.dds.speedplus.activity.fragment.DashBoardFragment;
import com.uxstory.dds.speedplus.activity.fragment.NoDeviceFragment;
import com.uxstory.dds.speedplus.activity.fragment.NotificationFragment;
import com.uxstory.dds.speedplus.activity.fragment.SettingFragment;
import com.uxstory.dds.speedplus.activity.fragment.StateBurFragment;
import com.uxstory.dds.speedplus.activity.fragment.StateWaterFragment;
import com.uxstory.dds.speedplus.activity.fragment.UseInfoContactFragment;
import com.uxstory.dds.speedplus.activity.fragment.UseInfoFragment;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.Notification;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.warrenth.rxbus2.RxBus;
import pe.warrenth.rxbus2.Subscribe;

public class MainActivity extends AbstractBaseActivity {

    private final static String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.navigation_view)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.app_bar)
    ViewGroup appBar;

    @BindView(R.id.toolbar_main)
    View toolbarMainView;

    @BindView(R.id.toolbar_sub)
    View toolbarSubView;

    @BindView(R.id.toolbar_main_icon)
    ImageView toolbarTitleIcon;

    @BindView(R.id.toolbar_sub_icon)
    View toolbarSubIcon;

    @BindView(R.id.toolbar_sub_icon_click)
    View toolbarSubIconClick;

    @BindView(R.id.toolbar_main_title)
    TextView toolbarTitleMain;

    @BindView(R.id.toolbar_noti)
    View toolbarNotiView;

    @BindView(R.id.txt_noti_count)
    TextView toolbarNotiCount;

    @BindView(R.id.noti_icon_click)
    View notiIconClick;

    @BindView(R.id.icon_app_link)
    TextView txtIconAppLink;

    @BindView(R.id.toolbar_sub_title)
    TextView toolbarSubTitle;

    @BindView(R.id.noti_layout)
    ViewGroup notiListLayout;

    @BindView(R.id.cardview_noti_no)
    View noNotiView;

    @BindView(R.id.cardview_noti)
    View notiListView;

    @BindView(R.id.recycler_noti)
    RecyclerView recyclerNoti;

    @BindView(R.id.cardview_noti_top_bar)
    View notiTopBar;

    @BindView(R.id.noti_top_bar_title)
    TextView notiTopBarTitle;

    @BindView(R.id.view_noti_top_bar_close)
    View btnNotiTopBarClose;

    private FragmentManager fragmentManager = getSupportFragmentManager();
    private DashBoardFragment dashBoardFragment = new DashBoardFragment();
    private NoDeviceFragment noDeviceFragment = new NoDeviceFragment();
    private UseInfoFragment useInfoFragment = new UseInfoFragment();
    private NotificationFragment notificationFragment = new NotificationFragment();
    private SettingFragment settingFragment = new SettingFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences pref = getSharedPreferences(Preference.PREFER_NAME, Context.MODE_PRIVATE);
        pref.edit().putLong(Preference.PREFER_USE_APP_TIME, System.currentTimeMillis()).apply();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int getDeviceHeight_Pixel = displayMetrics.heightPixels;
//        int getDeviceWidth_Pixel = displayMetrics.widthPixels;
        LogU.d(TAG, "deviceDpi=" + displayMetrics.densityDpi);
        GlobalApplication.setDensityDpi(displayMetrics.densityDpi);

//      갤럭시탭 s52 10.5 - Screen Zoom
//
//      1단계
//      getDeviceHeight_Pixel=2464
//      getDeviceWidth_Pixel=1600
//      getDeviceDpi=320
//      density=2.0
//      scaledDensity=2.0

//      2단계 (Default)
//      getDeviceHeight_Pixel=2452
//      getDeviceWidth_Pixel=1600
//      getDeviceDpi=360
//      density=2.25
//      scaledDensity=2.25

        initView();

        goToMenu(R.id.menu_dash);
    }

    private void initView() {

        aniSlowlyDisappear = AnimationUtils.loadAnimation(this, R.anim.slowly_fadeout);

        bottomNavigationView.setOnNavigationItemSelectedListener(new ItemSelectedListener());

//        notiIconClick.setOnClickListener(v -> goToMenu(R.id.menu_noti));

        notiIconClick.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (device != null) {
                    openNotiList4Device();
                } else {
                    openNotiAllList();
                }

//                startActivity(new Intent(MainActivity.this, LockScreenNotificationActivity.class));
            }
        });

        toolbarSubIconClick.setOnClickListener(v -> {
            if (mOnKeyBackPressedListener != null) {
                mOnKeyBackPressedListener.onBackPressed();
            }
        });

        notiTopBar.setSelected(true);
        notiTopBar.setOnClickListener(v -> {
            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerNoti.getLayoutManager());
            int position = layoutManager.findFirstVisibleItemPosition();

            if (position >= 0) {
                for (int i = position; i >= 0; i--) {
                    NotificationModel m = notiAllList.get(i);
                    if (m.isHeader()) {
                        recyclerNoti.scrollToPosition(i);
                        notiTopBar.setVisibility(View.INVISIBLE);
                        final int pos = i;
                        recyclerNoti.post(() -> {
                            View view = layoutManager.findViewByPosition(pos);
                            View topBar = view.findViewById(R.id.layout_top_bar);
                            if (topBar != null) {
                                topBar.setPressed(true);
                                topBar.setPressed(false);
                            }
                        });
                        break;
                    }
                }
            }
        });

        btnNotiTopBarClose.setOnClickListener(v -> {
            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerNoti.getLayoutManager());
            int position = layoutManager.findFirstVisibleItemPosition();

            if (position >= 0) {
                for (int i = position; i >= 0; i--) {
                    NotificationModel m = notiAllList.get(i);
                    if (m.isHeader()) {
                        recyclerNoti.scrollToPosition(i);
                        notiTopBar.setVisibility(View.INVISIBLE);
                        final int pos = i;
                        recyclerNoti.post(() -> {
                            View view = layoutManager.findViewByPosition(pos);
                            View topBar = view.findViewById(R.id.layout_top_bar);
                            if (topBar != null) {
                                topBar.performClick();
                                topBar.setPressed(true);
                                topBar.setPressed(false);
                            }
                        });
                        break;
                    }
                }
            }
        });

        notiListLayout.setVisibility(View.GONE);
        notiListLayout.setOnClickListener(v -> closeNotiView());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerNoti.setLayoutManager(layoutManager);

        onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());

                View firstView = layoutManager.findViewByPosition(0);
                if (firstView != null && firstView.getTop() >= 0) {
                    notiTopBar.setVisibility(View.INVISIBLE);
//                    lastView = null;
                    return;
                }

                int position = layoutManager.findFirstVisibleItemPosition();
                View view = layoutManager.findViewByPosition(position);

                int top = view.getTop();
                int bottom = top + view.getHeight();

                if (top < 0 && bottom > 0) {
                    NotificationModel model = notiAllList.get(position);
                    if (model.isHeader()) {
                        if (model.isSelected()) {
                            notiTopBarTitle.setText(model.getName());
                            notiTopBar.setVisibility(View.VISIBLE);
                            notiTopBarTitle.requestLayout();
                        } else {
                            notiTopBar.setVisibility(View.INVISIBLE);
                        }
//                        lastView = view;

                    } else {
                        for (NotificationModel m : notiTopBarDeviceList) {
                            if (m.getSerial().equals(model.getSerial())) {
                                notiTopBarTitle.setText(m.getName());
                                notiTopBar.setVisibility(View.VISIBLE);
                                notiTopBarTitle.requestLayout();
                                break;
                            }
                        }
                    }

                }

            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        RxBus.get().register(this);
        processIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        RxBus.get().unResister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void processIntent() {
        Intent intent = getIntent();
        LogU.d(TAG, "processIntent() intent=" + intent);
        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                String serial = intent.getStringExtra(Constants.INTENT_EXTRA_NAME_SERIAL);
                if (serial != null) {
                    Device device = getDdsService().getPairedDevice(serial);
                    if (device != null) {
                        if (action.equals(Constants.INTENT_ACTION_GOTO_WATER_STATE)) {
                            StateWaterFragment fragment = new StateWaterFragment();
                            fragment.setDevice(device);
                            dashBoardFragment.addFragment(this, fragment);

                        } else if (action.equals(Constants.INTENT_ACTION_GOTO_BUR_STATE)) {
                            StateBurFragment fragment = new StateBurFragment();
                            fragment.setDevice(device);
                            dashBoardFragment.addFragment(this, fragment);

                        }

                        boolean isRecordNoti = intent.getBooleanExtra(Constants.INTENT_EXTRA_NAME_IS_RECORD_NOTI, false);
                        if (isRecordNoti) {
                            device.minusNotiCount();
                            long notiRowId = intent.getLongExtra(Constants.INTENT_EXTRA_NAME_NOTI_ROW_ID, -1L);
                            if (notiRowId > -1) {
                                NotificationPublisher.setCheckedNotification(notiRowId);
                            }
                        }
                    }

                } else {
                    if (action.equals(Constants.INTENT_ACTION_GOTO_REQUEST_HELP)) {
                        dashBoardFragment.addFragment(this, new UseInfoContactFragment());

                    } else if (action.equals(Constants.INTENT_ACTION_WARN_LONG_TIME_NO_USE_DEVICE)) {
                        NotificationPublisher.showNotification(this, null, AppNotificationType.LONG_TIME_NO_USE_DEVICE_2);
                    }
                }
            }
            closeNotiView();
        }
        setIntent(null);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public void updateNotiCount(Device device) {
        if (getDdsService() != null) {
            int count = 0;

            if (device == null) {
                for (Device d : getDdsService().getDeviceList()) {
                    count += d.getNotiCount();
                }
            } else {
                count += device.getNotiCountMonitoring();
            }

            if (count > 0) {
                toolbarNotiCount.setVisibility(View.VISIBLE);
                toolbarNotiCount.setText(String.valueOf(count));
                toolbarNotiCount.requestLayout();

            } else {
                toolbarNotiCount.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void goToMenu(int menuItemId) {
        bottomNavigationView.setSelectedItemId(menuItemId);
    }

    private void popAllFragments() {
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    class ItemSelectedListener implements BottomNavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            popAllFragments();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            AbstractBaseFragment currentMainFragment = null;

            switch (menuItem.getItemId()) {
                case R.id.menu_dash:
                    currentMainFragment = selectMainDashFragment();
                    break;
                case R.id.menu_info:
                    currentMainFragment = useInfoFragment;
                    break;
                case R.id.menu_noti:
                    currentMainFragment = notificationFragment;
                    break;
                case R.id.menu_setting:
                    currentMainFragment = settingFragment;
                    break;
            }
            transaction.replace(R.id.frame_layout, currentMainFragment).commitAllowingStateLoss();
            currentMainFragment.registerRxBus();
            currentMainFragment.addBackStackChangedListener();
            setOnKeyBackPressedListener(null);
            updateToolbarMain(menuItem.getItemId());
            closeNotiView();
            return true;
        }
    }

    private AbstractBaseFragment selectMainDashFragment() {
        if (DbQuery.selectDeviceList(true).size() > 0) {
            return dashBoardFragment;
        } else {
            return noDeviceFragment;
        }
    }

    public boolean isDeviceView() {
        return device != null;
    }

    public Fragment getCurrentFragment() {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments.size() > 0) {
            return fragments.get(fragments.size() - 1);
        }
        return null;
    }

    public void updateToolbarMain(int menuItemId) {
        LogU.d(TAG, "updateToolbarMain()");

        this.device = null;
        this.menuId = menuItemId;

        switch (menuItemId) {
            case R.id.menu_dash:
                toolbarTitleIcon.setImageResource(R.drawable.icon_app_dash);
                toolbarTitleMain.setText(R.string.txt_menu_dash);
                updateNotiCount(null);
                toolbarNotiView.setVisibility(View.VISIBLE);
                break;
            case R.id.menu_info:
                toolbarTitleIcon.setImageResource(R.drawable.icon_app_info);
                toolbarTitleMain.setText(R.string.txt_menu_info);
                updateNotiCount(null);
                toolbarNotiView.setVisibility(View.VISIBLE);
                break;
            case R.id.menu_noti:
                toolbarTitleIcon.setImageResource(R.drawable.icon_app_notice);
                toolbarTitleMain.setText(R.string.txt_menu_noti);
                toolbarNotiView.setVisibility(View.INVISIBLE);
                break;
            case R.id.menu_setting:
                toolbarTitleIcon.setImageResource(R.drawable.icon_app_setting);
                toolbarTitleMain.setText(R.string.txt_menu_setting);
                updateNotiCount(null);
                toolbarNotiView.setVisibility(View.VISIBLE);
                break;
        }
        toolbarMainView.setVisibility(View.VISIBLE);

        toolbarSubView.setVisibility(View.INVISIBLE);
        toolbarSubTitle.setText("");
        toolbarSubTitle.requestLayout();
        txtIconAppLink.setText("");
        txtIconAppLink.requestLayout();

        toolbarTitleMain.requestLayout();
    }

    public void updateToolbarSub(int titleResId, Device device) {

        toolbarMainView.setVisibility(View.INVISIBLE);
        toolbarNotiView.setVisibility(View.VISIBLE);
        toolbarSubView.setVisibility(View.VISIBLE);

        this.device = device;
        this.menuId = 0;

        updateNotiCount(device);

        if (device != null) {
            if (titleResId > 0) {
                toolbarSubTitle.setText(titleResId);
            } else {
                toolbarSubTitle.setText(device.getName());
            }
            txtIconAppLink.setText("  ");
            txtIconAppLink.setSelected(device.isConnected());
//            toolbarSubTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_icon_app_link, 0, 0, 0);
//            toolbarSubTitle.setSelected(device.isConnected());
        } else {
            toolbarSubTitle.setText(titleResId);
            txtIconAppLink.setText("");
//            toolbarSubTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        toolbarSubView.postDelayed(() -> {
            txtIconAppLink.requestLayout();
            toolbarSubTitle.requestLayout();
        }, 100);
    }

    private Device device;
    private int menuId;

    private RecyclerView.OnScrollListener onScrollListener;
    private List<NotificationModel> notiTopBarDeviceList = new ArrayList<>();
    private List<NotificationModel> notiAllList = new ArrayList<>();

    public void closeNotiView() {
        notiListLayout.setVisibility(View.INVISIBLE);
        recyclerNoti.removeOnScrollListener(onScrollListener);
        notiAllList = null;
    }

    private void openNotiList4Device() {
        if (device != null) {
            notiTopBar.setVisibility(View.INVISIBLE);
            notiListLayout.setVisibility(View.VISIBLE);

            Event lastEvent = device.getLastEvent();

            if (lastEvent != null) {

                String date = StringUtil.getCurrentDateTime();
                if (lastEvent.getEventType() == EventType.MILLING_FINISH || !device.isConnected()) {
                    date = device.getLastEvent().getTime();
                }

                List<NotificationModel> list = DbQuery.selectLastNotiMessageList(device.getSerial(), date);

                NotificationPopMessageRecycleAdapter recyclerAdapter;

                if (list.size() > 0) {
                    notiListView.setVisibility(View.VISIBLE);
                    noNotiView.setVisibility(View.INVISIBLE);
                    recyclerAdapter = new NotificationPopMessageRecycleAdapter(this, list);
                    recyclerNoti.setAdapter(recyclerAdapter);
                    recyclerAdapter.notifyDataSetChanged();

                    recyclerNoti.scrollToPosition(0);

                } else {
                    notiListView.setVisibility(View.INVISIBLE);
                    noNotiView.setVisibility(View.VISIBLE);
                }

                recyclerNoti.removeOnScrollListener(onScrollListener);

                device.setNotiCount(device.getNotiCount() - device.getNotiCountMonitoring());
                device.clearNotiCountMonitoring();
                NotificationPublisher.setCheckedAllNotification(device);

                updateNotiCount(device);

            } else {
                notiListView.setVisibility(View.INVISIBLE);
                noNotiView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void openNotiAllList() {
        notiListLayout.setVisibility(View.VISIBLE);

        String terms = "-7 day";
        notiAllList = DbQuery.selectNotiDeviceList(terms);

        notiTopBarDeviceList.clear();
        notiTopBarDeviceList.addAll(notiAllList);

        if (notiAllList.size() > 0) {
            notiListView.setVisibility(View.VISIBLE);
            noNotiView.setVisibility(View.INVISIBLE);
            notiAllList.get(0).setSelected(true);
            NotificationPopDeviceRecycleAdapter recyclerAdapter = new NotificationPopDeviceRecycleAdapter(this, notiAllList);
            recyclerAdapter.setTerms(terms);
            recyclerNoti.setAdapter(recyclerAdapter);
            recyclerAdapter.setNotiList(0);
            recyclerAdapter.notifyDataSetChanged();

            recyclerNoti.scrollToPosition(0);

        } else {
            notiListView.setVisibility(View.INVISIBLE);
            noNotiView.setVisibility(View.VISIBLE);
        }

        recyclerNoti.addOnScrollListener(onScrollListener);

        for (Device device : getDdsService().getDeviceList()) {
            device.clearNotiCount();
            device.clearNotiCountMonitoring();
            NotificationPublisher.setCheckedAllNotification(device);
        }

        updateNotiCount(null);
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_DEVICE)
    public void onRxBusEvent(CommunicatorEvent communicatorEvent) {
        LogU.d(TAG, "onRxBusEvent() = " + communicatorEvent.getResponseType() + "," + this);
        if (this.device != null && this.device == communicatorEvent.getDevice()) {
            switch (communicatorEvent.getResponseType()) {
                case DISCONNECTED:
                case CONNECTED:
                    updateToolbarSub(0, device);
                    break;
            }
        }
    }

    private Animation aniSlowlyDisappear;
    Runnable mNavHider = this::hideNavi4Landscape;

    public boolean isLandscape() {
        Display display = getWindowManager().getDefaultDisplay();
        return display != null && (display.getRotation() == Surface.ROTATION_270 || display.getRotation() == Surface.ROTATION_90);
    }

    private void hideNavi4Landscape() {
        if (isLandscape()) {
            bottomNavigationView.setVisibility(View.GONE);
            bottomNavigationView.startAnimation(aniSlowlyDisappear);
        }
    }

    public void toggleNavi() {
        Handler handler = bottomNavigationView.getHandler();
        if (handler != null) {
            handler.removeCallbacks(mNavHider);
        }
        if (bottomNavigationView.getVisibility() == View.VISIBLE) {
            bottomNavigationView.setVisibility(View.GONE);
        } else {
            bottomNavigationView.setVisibility(View.VISIBLE);
            if (handler != null) {
                handler.postDelayed(mNavHider, 3000);
            }
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogU.d(TAG, "onConfigurationChanged() " + this);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            closeNotiView();

            int uiOptions = GlobalApplication.getDefualtUiOptions();

            String topBarLayoutName;
            String notiLayoutName;

            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                uiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
                uiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
                uiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                topBarLayoutName = "view_top_bar_landscape";
                notiLayoutName = "view_noti_list_pop_landscape";

                bottomNavigationView.setVisibility(View.GONE);

            } else {
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                topBarLayoutName = "view_top_bar";
                notiLayoutName = "view_noti_list_pop";

                bottomNavigationView.setVisibility(View.VISIBLE);
            }

            appBar.removeAllViews();
            int topBarViewId = getResources().getIdentifier(topBarLayoutName, "layout", getPackageName());
            LayoutInflater.from(this).inflate(topBarViewId, appBar, true);

            notiListLayout.removeAllViews();
            int notiViewId = getResources().getIdentifier(notiLayoutName, "layout", getPackageName());
            LayoutInflater.from(this).inflate(notiViewId, notiListLayout, true);

            ButterKnife.bind(this);
            initView();

            if (this.device != null) {
                new Handler().postDelayed(() -> {
                    updateToolbarSub(0, this.device);
                }, 500);

//                updateToolbarSub(0, this.device);

            } else if (this.menuId > 0) {
                updateToolbarMain(this.menuId);
            }

            getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
    }
}