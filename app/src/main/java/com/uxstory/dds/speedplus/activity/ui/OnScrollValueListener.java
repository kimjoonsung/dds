package com.uxstory.dds.speedplus.activity.ui;

public interface OnScrollValueListener {
    void onScrollValue(String value1, String value2);
}
