package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.notification.type.CustomEventType;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;
import com.uxstory.dds.speedplus.util.DateUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationPopMessageRecycleAdapter extends RecyclerView.Adapter<NotificationPopMessageRecycleAdapter.ViewHolder> {

    private final static String TAG = NotificationPopMessageRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<NotificationModel> items;

    public NotificationPopMessageRecycleAdapter(Context context, List<NotificationModel> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<NotificationModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_noti_pop_msg, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        LogU.d(TAG, "onBindViewHolder() position=" + position);

        NotificationModel item = items.get(position);

        String notificationTypeString = item.getNotificationTypeString();
        NotificationKindType notificationKindType = item.getNotificationKindType();
        int iconResId = 0;
        int msgResId = 0;
        switch (notificationKindType) {
            case EVENT:
                EventType eventType = EventType.getType(notificationTypeString);
                iconResId = eventType.getIconResId();
                msgResId = eventType.getMsgResId();
                break;
            case CUSTOM_EVENT:
                CustomEventType customEventType = CustomEventType.getType(notificationTypeString);
                iconResId = customEventType.getIconResId();
                msgResId = customEventType.getMsgResId();
                break;
            case APP:
                AppNotificationType appNotificationType = AppNotificationType.getType(notificationTypeString);
                iconResId = appNotificationType.getIconResId();
                msgResId = appNotificationType.getMsgResId();
                break;
        }

        holder.txtPreTime.setText(DateUtil.calculateTime(context, item.getRegTime()));

        String time = "("+DateUtil.convertFormatDate("yyyy-MM-dd HH:mm", item.getRegTime())+")";
        holder.txtTime.setText(time);

        if(iconResId > 0) {
            holder.iconType.setImageResource(iconResId);
        }

        if(msgResId > 0) {
            holder.txtMsg.setText(msgResId);
        }

//        ViewUtil.requestLayout((ViewGroup)holder.itemView);

    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_type)
        ImageView iconType;

        @BindView(R.id.txt_pre_time)
        TextView txtPreTime;

        @BindView(R.id.txt_time)
        TextView txtTime;

        @BindView(R.id.txt_msg)
        TextView txtMsg;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
