package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.config.Preference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SettingNotificationFragment extends AbstractBaseFragment {
    private final static String TAG = SettingNotificationFragment.class.getSimpleName();

    @BindView(R.id.setting_noti_all)
    View settingNotiAll;

    @BindView(R.id.setting_noti_milling)
    View settingNotiMilling;

    @BindView(R.id.setting_noti_bur)
    View settingNotiBur;

    @BindView(R.id.setting_noti_water)
    View settingNotiWater;

    @BindView(R.id.setting_noti_long_no_use)
    View settingNotiLongNoUse;

    @BindView(R.id.switch_setting_noti_all)
    SwitchCompat switchSettingNotiAll;

    @BindView(R.id.switch_setting_noti_milling)
    SwitchCompat switchSettingNotiMilling;

    @BindView(R.id.switch_setting_noti_bur)
    SwitchCompat switchSettingNotiBur;

    @BindView(R.id.switch_setting_noti_water)
    SwitchCompat switchSettingNotiWater;

    @BindView(R.id.switch_setting_noti_long_no_use)
    SwitchCompat switchSettingNotiLongNoUse;

    List<SwitchCompat> switchCompats = new ArrayList<>();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_setting_noti);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setPreferenceAction(settingNotiAll, switchSettingNotiAll, Preference.PreferenceType.NOTI_ALL);
        setPreferenceAction(settingNotiMilling, switchSettingNotiMilling, Preference.PreferenceType.NOTI_MILLING);
        setPreferenceAction(settingNotiBur, switchSettingNotiBur, Preference.PreferenceType.NOTI_BUR);
        setPreferenceAction(settingNotiWater, switchSettingNotiWater, Preference.PreferenceType.NOTI_WATER);
        setPreferenceAction(settingNotiLongNoUse, switchSettingNotiLongNoUse, Preference.PreferenceType.NOTI_LONG_NO_USE);

        initAllChecked();
    }

    private void initAllChecked() {

        boolean check = true;
        for(SwitchCompat switchCompat: switchCompats) {
            switchSettingNotiAll.setChecked(false);
            if(!switchCompat.isChecked()) {
                check = false;
            }
        }
        switchSettingNotiAll.setChecked(check);

        switchSettingNotiAll.setOnCheckedChangeListener((buttonView, isChecked) -> {
            for(SwitchCompat switchCompat: switchCompats) {
                switchCompat.setChecked(isChecked);
            }
        });
    }

    private void setPreferenceAction(View view,  SwitchCompat switchCompat, Preference.PreferenceType preferenceType) {

        view.setOnClickListener(v -> switchCompat.toggle());

        if(preferenceType != Preference.PreferenceType.NOTI_ALL) {
            switchCompats.add(switchCompat);
            switchCompat.setChecked(Preference.getPreferenceValue(preferenceType));
            switchCompat.setOnCheckedChangeListener((buttonView, isChecked) -> {
                Preference.setPreferenceValue(getContext(), preferenceType, isChecked);
            });
        }
    }

    private void setPreferenceFragment() {
        if(getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
//            transaction.replace(R.id.frame_layout_preference, new SettingNotificationPreferenceFragment()).commitAllowingStateLoss();
        }
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarSub(R.string.txt_setting_noti, null);
    }
}
