package com.uxstory.dds.speedplus.message;

import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.device.Event;

public class EventChecker {

    public static boolean isMillingStartEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_START == event.getEventType();
    }

    public static boolean isMillingContinueEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_CONTINUE == event.getEventType();
    }

    public static boolean isMillingReStartEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_RESTART == event.getEventType();
    }

    public static boolean isPreMillingEvent(Event event) {
        if (event == null) {
            return false;
        }

        switch (event.getEventType()) {
            case READY:
            case INSERT_BUR_AND_BLOCK:
            case SENSING:
                return true;
        }
        return false;
    }

    public static boolean isMillingInitEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.INSERT_BUR_AND_BLOCK == event.getEventType();
    }

    public static boolean isMillingPauseEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.PAUSE == event.getEventType();
    }

    public static boolean isMillingStopEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_STOP == event.getEventType();
    }

    public static boolean isMillingEndEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_FINISH == event.getEventType() || EventType.MILLING_STOP == event.getEventType();
    }

    public static boolean isMillingFinishEvent(Event event) {
        if (event == null) {
            return false;
        }
        return EventType.MILLING_FINISH == event.getEventType();
    }

    public static boolean isPossibleRequestWaterSensingEvent(Event event) {
        if (event == null) {
            return false;
        }

        switch (event.getEventType()) {
            case WARM_UP:
            case PUMP:
            case CALIBRATION:
            case SENSING:
            case MILLING_START:
            case MILLING_CONTINUE:
            case MILLING_RESTART:
            case PAUSE:
            case PAUSE_PUMP:
            case PAUSE_CHANGE_BUR:
            case PAUSE_SENSING:
            case CAM_RECONNECTED:
            case DOOR_OPEN:
            case NOT_WORK:
            case COLLET_OPEN:
                return false;
            default:
                return true;
        }
    }

    public static boolean isPossibleRequestBurInfoEvent(Event event) {
        if (event == null) {
            return false;
        }

        switch (event.getEventType()) {
            case WARM_UP:
//            case PUMP:
            case CALIBRATION:
            case SENSING:
            case MILLING_START:
            case MILLING_CONTINUE:
            case MILLING_RESTART:
            case PAUSE:
            case PAUSE_PUMP:
            case PAUSE_CHANGE_BUR:
            case PAUSE_SENSING:
            case CAM_RECONNECTED:
            case DOOR_OPEN:
            case NOT_WORK:
//            case COLLET_OPEN:
                return false;
            default:
                return true;
        }
    }

    public static boolean isPossibleRequestMillingProgressEvent(Event event) {
        if (event == null) {
            return false;
        }

        switch (event.getEventType()) {
            case MILLING_START:
            case MILLING_CONTINUE:
            case MILLING_RESTART:
                return true;
            default:
                return false;
        }
    }

}
