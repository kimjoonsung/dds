package com.uxstory.dds.speedplus.model;

import java.util.Comparator;

public class ComparatorDataCount implements Comparator<DataCountModel> {

    @Override
    public int compare(DataCountModel o1, DataCountModel o2) {
        return Integer.compare(o2.getCount(), o1.getCount());
    }
}
