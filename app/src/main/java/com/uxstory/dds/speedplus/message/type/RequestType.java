package com.uxstory.dds.speedplus.message.type;

public enum RequestType {

    SESSION("session"),
    RECONNECT("reconnect"),
    EVENT_HISTORY("eventHistory"),
    WATER_SENSING("waterSensing"),
    MILLING_PROGRESS("millingProgress"),
    ECHO_KEEP_ALIVE("echoKeepAlive"),
    BUR_INFO("burInfo"),
    UNPAIRING("connectComplete"),
    ;

    private String value;

    RequestType(String value) {
        this.value = value;
    }

    public String getString() {
        return value;
    }
}
