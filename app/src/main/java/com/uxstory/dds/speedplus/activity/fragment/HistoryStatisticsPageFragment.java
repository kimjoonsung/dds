package com.uxstory.dds.speedplus.activity.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.HistoryDataViewPagerFragmentStateAdapter;
import com.uxstory.dds.speedplus.activity.ui.CalendarRangeDecorator;
import com.uxstory.dds.speedplus.activity.ui.ChartMarkerView;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.model.type.BlockType;
import com.uxstory.dds.speedplus.util.DateUtil;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;

public class HistoryStatisticsPageFragment extends AbstractBaseFragment {

    private final static String TAG = HistoryStatisticsPageFragment.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager2 pager;

    @BindView(R.id.view_calendar)
    View viewCalender;

    @BindView(R.id.txt_count)
    TextView txtCount;

    @BindView(R.id.txt_select_date)
    TextView txtSelectDate;

    @BindView(R.id.bar_chart)
    BarChart barChart;

    @BindView(R.id.view_no_milling_history)
    View viewNoMillingHistory;

    @BindView(R.id.btn_prev)
    ImageButton btnPrev;

    @BindView(R.id.btn_next)
    ImageButton btnNext;

    @BindView(R.id.txt_calendar_day_start)
    TextView txtCalendarStartDay;

    @BindView(R.id.txt_calendar_day_end)
    TextView txtCalendarEndDay;

    @BindView(R.id.btn_cancel)
    Button btnCalendarCancel;

    @BindView(R.id.btn_apply)
    Button btnCalendarApply;

    @BindView(R.id.calendar)
    MaterialCalendarView calendarView;

    private ArrayList<DataCountModel> datesCounts = new ArrayList<>();

    private CalendarRangeDecorator calendarRangeDecorator;

    private int mode;

    public void setMode(int mode) {
        this.mode = mode;
    }

    private boolean isModeDay() {
        return this.mode == 0;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_history_statistics_page);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnPrev.setOnClickListener(v -> {
            if (btnPrev.isEnabled()) {
                closeCalendar();
                pager.setCurrentItem(pager.getCurrentItem() - 1, true);
            }
        });

        btnNext.setOnClickListener(v -> {
            if (btnNext.isEnabled()) {
                closeCalendar();
                pager.setCurrentItem(pager.getCurrentItem() + 1, true);
            }
        });

        btnCalendarCancel.setOnClickListener(v -> closeCalendar());

        DayViewDecorator dayViewDecorator = new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                LocalDate localDate = day.getDate();
                LocalDate maxDate = (calendarView.getMaximumDate() == null) ? null : calendarView.getMaximumDate().getDate();
                LocalDate minDate = (calendarView.getMinimumDate() == null) ? null : calendarView.getMinimumDate().getDate();
                return localDate.getDayOfWeek() == DayOfWeek.SUNDAY
                        && (maxDate == null || localDate.isEqual(maxDate) || localDate.isBefore(maxDate))
                        && (minDate == null || localDate.isEqual(minDate) || localDate.isAfter(minDate));
            }

            @Override
            public void decorate(DayViewFacade view) {
                view.addSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorRed)));
            }
        };
//        calendarView.addDecorator(dayViewDecorator);

        calendarView.setOnDateChangedListener((widget, date, selected) -> {
            if (selected) {
                String selectedDate = getCalendarDayToString(date);
                Calendar selectDayCalendar = DateUtil.getCalendar(selectedDate);

                String startDay = DateUtil.getDateStringAddDate(Calendar.MONTH, -1, selectDayCalendar);
                String endDay = DateUtil.getDateStringAddDate(Calendar.MONTH, +1, selectDayCalendar);
//                endDay = (DateUtil.getCalendar(endDay).getTimeInMillis() > System.currentTimeMillis()) ? StringUtil.getCurrentDate() : endDay;

                setCalendarLimitDay(startDay, endDay);

            } else {
                txtCalendarStartDay.setText("");
                txtCalendarEndDay.setText("");
                setCalendarLimitDay(null, "9999-12-31");
            }
            calendarView.removeDecorators();
//            calendarView.addDecorator(dayViewDecorator);
            calendarView.setCurrentDate(date);
        });

        calendarView.setOnRangeSelectedListener((widget, dates) -> {
            txtCalendarStartDay.setText(getCalendarDayToString(dates.get(0)));
            txtCalendarEndDay.setText(getCalendarDayToString(dates.get(dates.size() - 1)));
            calendarRangeDecorator.setRangeDecorators(dates);
        });

        btnCalendarApply.setOnClickListener(view -> {
            String startDay = txtCalendarStartDay.getText().toString();
            String endDay = txtCalendarEndDay.getText().toString();
            if (!startDay.isEmpty() && !endDay.isEmpty()) {
                setMillingData(txtCalendarStartDay.getText().toString(), txtCalendarEndDay.getText().toString());
                setMillingBarChart();
                closeCalendar();
            }
        });

        viewCalender.setOnClickListener(v -> closeCalendar());

        calendarRangeDecorator = new CalendarRangeDecorator(getContext(), calendarView);

        closeCalendar();
    }

    public void openCalendar() {
//        txtCalendarStartDay.performClick();
        viewCalender.setVisibility(View.VISIBLE);
    }

    public void closeCalendar() {
        viewCalender.setVisibility(View.INVISIBLE);
    }

    private void setPager() {
        HistoryDataViewPagerFragmentStateAdapter viewPagerFragmentStateAdapter = new HistoryDataViewPagerFragmentStateAdapter(this);
        viewPagerFragmentStateAdapter.setList(datesCounts);
        viewPagerFragmentStateAdapter.setDevice(getDevice());

        String strCount = getContext().getString(R.string.txt_total_milling_count_1);

        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                txtSelectDate.setText(StringUtil.convertDateToDisplayFormat(datesCounts.get(position).getTime()));
                String str = String.format(strCount, String.valueOf(datesCounts.get(position).getCount()));
                ViewUtil.highlightTagText(getContext(), txtCount, str, R.color.colorBlueLight);
                btnNext.setEnabled(datesCounts.size() - position > 1);
                btnPrev.setEnabled(position > 0);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        pager.setAdapter(viewPagerFragmentStateAdapter);
        pager.setUserInputEnabled(true);
//        pager.setPageTransformer(new ZoomOutPageTransformer());
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(datesCounts.size() - 1);
    }

    public void updateView() {
        if (pager.getAdapter() == null && getDevice() != null) {
            if (isModeDay()) {
                datesCounts = DbQuery.selectMillingFinishDatesCount(getDevice().getSerial(), null);
                if (datesCounts.size() == 0) {
                    DataCountModel dataCountModel = new DataCountModel();
                    dataCountModel.setTime(StringUtil.getCurrentDateDisplayFormat());
                    dataCountModel.setCount(0);
                    datesCounts.add(dataCountModel);
                }

            } else {
                datesCounts = DbQuery.selectMillingFinishMonthsCount(getDevice().getSerial(), null);
                if (datesCounts.size() == 0) {
                    DataCountModel dataCountModel = new DataCountModel();
                    dataCountModel.setTime(StringUtil.getCurrentMonthDisplayFormat());
                    dataCountModel.setCount(0);
                    datesCounts.add(dataCountModel);
                }
            }

            setPager();

            String endDay = StringUtil.getCurrentDate();
            String startDay = DateUtil.getDateStringAddDate(Calendar.MONTH, -1, DateUtil.getCalendar(endDay));
            setCalendarViewStartDate(startDay);
            setCalendarViewEndDate(endDay);
//            setCalendarLimitDay(null, endDay);

            setMillingBarChart();
        }
    }

    private void setCalendarLimitDay(String startDay, String endDay) {
        CalendarDay startCalendarDay = getCalendarDay(DateUtil.getCalendar(startDay));
        CalendarDay endCalendarDay = getCalendarDay(DateUtil.getCalendar(endDay));
        calendarView.state().edit().setMinimumDate(startCalendarDay).setMaximumDate(endCalendarDay).commit();
    }

    private void setCalendarViewStartDate(String date) {
//        CalendarDay calendarDay = getCalendarDay(DateUtil.getCalendar(date));
//        calendarView.clearSelection();
//        calendarView.setDateSelected(calendarDay, true);
//        calendarView.setCurrentDate(calendarDay);
        txtCalendarStartDay.setText(StringUtil.convertDateToDisplayFormat(date));
    }

    private void setCalendarViewEndDate(String date) {
//        CalendarDay calendarDay = getCalendarDay(DateUtil.getCalendar(date));
//        calendarView.clearSelection();
//        calendarView.setDateSelected(calendarDay, true);
//        calendarView.setCurrentDate(calendarDay);
        txtCalendarEndDay.setText(StringUtil.convertDateToDisplayFormat(date));
    }

    private String getCalendarDayToString(CalendarDay calendarDay) {
        return calendarDay.getYear() + "." + String.format(Locale.getDefault(), "%02d", calendarDay.getMonth()) + "." + String.format(Locale.getDefault(), "%02d", calendarDay.getDay());
    }

    private CalendarDay getCalendarDay(Calendar calendar) {
        if (calendar != null) {
            return CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE));
        }
        return null;
    }

    private void setMillingData(String startDate, String endDate) {
        datesCounts = DbQuery.selectMillingFinishDatesCount(getDevice().getSerial(), startDate, endDate);
        if (datesCounts.size() == 0) {
            DataCountModel dataCountModel = new DataCountModel();
            dataCountModel.setTime(txtCalendarEndDay.getText().toString());
            dataCountModel.setCount(0);
            datesCounts.add(dataCountModel);
        }

        setPager();
    }

    private void setMillingBarChart() {

        if (barChart.getBarData() != null) {
            barChart.getBarData().clearValues();
            barChart.clearValues();
        }
        barChart.clear();
        barChart.notifyDataSetChanged();
        barChart.setFitBars(true);
        barChart.fitScreen();
        barChart.resetViewPortOffsets();
        barChart.resetZoom();
        barChart.invalidate();

        ArrayList<DataCountModel> list;
        ArrayList<String> dates;
        ArrayList<String> blocks;

        if (isModeDay()) {
            list = DbQuery.selectMillingFinishBlockCounts(getDevice().getSerial(), txtCalendarStartDay.getText().toString(), txtCalendarEndDay.getText().toString());
            dates = DbQuery.selectMillingFinishDates(getDevice().getSerial(), txtCalendarStartDay.getText().toString(), txtCalendarEndDay.getText().toString());
            blocks = DbQuery.selectMillingFinishBlocks(getDevice().getSerial(), txtCalendarStartDay.getText().toString(), txtCalendarEndDay.getText().toString());

        } else {
            list = DbQuery.selectMillingFinishBlockCounts(getDevice().getSerial(), null);
            dates = DbQuery.selectMillingFinishMonths(getDevice().getSerial(), null);
            blocks = DbQuery.selectMillingFinishBlocks(getDevice().getSerial(), null);
        }

        if (list.size() > 0) {
            viewNoMillingHistory.setVisibility(View.GONE);
            barChart.setVisibility(View.VISIBLE);
        } else {
            viewNoMillingHistory.setVisibility(View.VISIBLE);
            barChart.setVisibility(View.GONE);
            return;
        }

        ArrayList<BarEntry> entries = new ArrayList<>();
        String[] labels = new String[dates.size()];

        int labelIndex = 0;
        String preYear = null;
        for (String date : dates) {

            String year = date.substring(0, 4);

            if (isModeDay()) {
                String label = (preYear != null && !year.equals(preYear)) ? date : StringUtil.convertDateToMonthDay(date);
                labels[labelIndex] = StringUtil.convertDateToDisplayFormat(label);

            } else {
                String label = (preYear != null && !year.equals(preYear)) ? date : StringUtil.convertDateToMonth(date);
                labels[labelIndex] = StringUtil.convertDateToDisplayFormat(label);
            }
            labelIndex++;

            preYear = year;
        }

        float multiplier = GlobalApplication.getDpiMultiplier();

        ArrayList<LegendEntry> legendEntries = new ArrayList<>();

        int colorIndex = 0;
        int[] chartColors = new int[blocks.size()];
        for (String block : blocks) {
            int color;
            int colorResId = BlockType.getColorResId(block);
            if(colorResId == 0) {
                color = getRandomColor();
            } else {
                color = getContext().getColor(colorResId);
            }

            chartColors[colorIndex] = color;
            legendEntries.add(new LegendEntry(block, Legend.LegendForm.CIRCLE, 12f * multiplier, 12f * multiplier, null, color));
            colorIndex++;
        }

        Legend legend = barChart.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setDrawInside(false);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setCustom(legendEntries);
        legend.setYOffset(20f * multiplier);
        legend.setXOffset(30f * multiplier);
        legend.setYEntrySpace(5f * multiplier);
        legend.setXEntrySpace(20f * multiplier);
        legend.setTextSize(12f * multiplier);

        String date = null;
        float[] counts = new float[blocks.size()];
        int x = 0;
        int overIndex = legendEntries.size();
        int maxCount = 0;
        for (DataCountModel model : list) {

            if (date == null) {
                date = model.getTime();
            }

            if (date.equals(model.getTime())) {
                int index = getLegendIndex(legendEntries, model.getName());
                if (index < 0) {
                    index = overIndex++;
                }
                LogU.d(TAG, "index=" + index);
                counts[index] = (float) model.getCount();

            } else {
                entries.add(new BarEntry(x, counts));
                counts = new float[blocks.size()];

                int index = getLegendIndex(legendEntries, model.getName());
                if (index < 0) {
                    index = overIndex++;
                }
                LogU.d(TAG, "index=" + index);
                counts[index] = (float) model.getCount();

                date = model.getTime();
                x++;
            }
        }

        if (list.size() > 0) {
            entries.add(new BarEntry(x, counts));
        }

        for(BarEntry entry : entries) {
            maxCount = (maxCount < entry.getPositiveSum()) ? (int)entry.getPositiveSum() : maxCount;
        }

        maxCount = (maxCount % 2 == 0) ? maxCount + 2 : maxCount + 1;
        int labelCount = (maxCount < 5) ? maxCount + 1 : 4;

        //todo test
//        for(int i=0; i<50; i++) {
//            entries.add(new BarEntry(++x, i));
//        }

//        barChart.animateY(500);

//        float barWidth = (0.3f * multiplier) + 0.1f;

        ChartMarkerView markerView = new ChartMarkerView(getContext(), R.layout.view_chart_marker_layout);

        barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                LogU.d(TAG, "Value: " + e.getY() + ", xIndex: " + e.getX() + ", DataSet index: " + h.getDataSetIndex());

                int index = (int) e.getX();
                String selectDate = dates.get(index);
                ArrayList<DataCountModel> list = DbQuery.selectBlockCountList(getDevice().getSerial(), selectDate);
                markerView.setData(list, legendEntries);

                RectF bounds = new RectF();
                barChart.getBarBounds((BarEntry) e, bounds);

                MPPointF position = barChart.getPosition(e, YAxis.AxisDependency.LEFT);

                LogU.d(TAG, "bounds=" + bounds.toString());
                LogU.d(TAG, "position=" + position.getX() + ", " + position.getY());

                LogU.d(TAG, "barChart.getHeight: " + barChart.getHeight() + ", barChart.getWidth(): " + barChart.getWidth());

                float offsetX = 20f * multiplier;
                if (markerView.getViewWidth() + position.getX() + offsetX > barChart.getWidth()) {
                    offsetX = -markerView.getViewWidth() - offsetX;
                }

//                float offsetY = - (list.size() * 22f * multiplier) - (60f * multiplier);
                float offsetY = -(50f * list.size()) - 100f;

//                if(-offsetY + markerView.getViewHeight() > barChart.getHeight()) {
//                    offsetY = -(barChart.getHeight() - markerView.getViewHeight());
//                }
                markerView.setOffset(offsetX, offsetY);

                MPPointF.recycleInstance(position);

                pager.setCurrentItem(index, true);
            }

            @Override
            public void onNothingSelected() {

            }
        });

        barChart.setMarker(markerView);
        barChart.getLegend().setWordWrapEnabled(true);

//        barChart.setOnTouchListener(new ChartTouchListener(barChart) {
//            @Override
//            public boolean onTouch(View v, MotionEvent e) {
//                LogU.d(TAG,"ChartTouchListener y:" + e.getY() + ", x:" + e.getX());
//                Highlight highlight = barChart.getHighlightByTouchPoint(e.getX(), e.getY());
////                LogU.d(TAG,"ChartTouchListener: " +highlight.getDataIndex() + ", xIndex: " + highlight.getX() + ", DataSet index: " + highlight.getY());
//                return false;
//            }
//        });

//        barChart.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent e) {
//                LogU.d(TAG,"OnTouchListener y:" + e.getY() + ", x:" + e.getX());
//                return false;
//            }
//        });


        barChart.setHighlightFullBarEnabled(true);

        BarDataSet barDataSet = new BarDataSet(entries, "");
//        colorIndex = 0;
        barDataSet.resetColors();
//        for (String block : blocks) {
//            barDataSet.addColor(chartColors[colorIndex++]);
//        }
        barDataSet.setColors(chartColors);

        barDataSet.setDrawIcons(false);
        barDataSet.setDrawValues(false);
        barDataSet.setHighlightEnabled(true);
        barDataSet.setHighLightColor(getContext().getColor(R.color.colorWhite));
        barDataSet.setHighLightAlpha(100);

        BarData barData = new BarData(barDataSet);
        barData.setValueFormatter(new LargeValueFormatter());
        barData.setBarWidth(0.25f);
//        barData.setHighlightEnabled(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(getContext().getColor(R.color.colorGray));
//        xAxis.setGranularity(1f);
//        xAxis.setGranularityEnabled(true);
        xAxis.setTextSize(12f * multiplier);
        xAxis.setLabelCount(10);
        xAxis.setAvoidFirstLastClipping(false);
        xAxis.setSpaceMin(0.5f);
        xAxis.setSpaceMax(0.5f);
//        xAxis.setCenterAxisLabels(true);
        xAxis.setXOffset(0f);
        xAxis.setYOffset(5f);
        if (entries.size() < 10) {
            xAxis.setAxisMaximum(9f);
        } else {
            xAxis.setAxisMaximum(entries.size());
        }
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(true);
        yAxis.setDrawZeroLine(true);
        yAxis.setAxisMinimum(0f);
        yAxis.setGridColor(getContext().getColor(R.color.colorGrayLine1));
        yAxis.setGridLineWidth(1.5f * multiplier);
        yAxis.setTextSize(12f * multiplier);
        yAxis.setTextColor(getContext().getColor(R.color.colorGray));
        yAxis.setLabelCount(labelCount, maxCount < 5);
        yAxis.setAxisMaximum(maxCount);
//        yAxis.setSpaceTop(20f);

        yAxis = barChart.getAxisRight();
        yAxis.setDrawLabels(false);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawZeroLine(false);
        yAxis.setAxisMinimum(0f);
        yAxis.setDrawAxisLine(false);
//        yAxis.setValueFormatter(new ValueFormatter() {
//            @Override
//            public String getFormattedValue(float value) {
//                return String.valueOf((int)value);
//            }
//        });
//        yAxis.setLabelCount(labelCount, maxCount < 5);
//        yAxis.setAxisMaximum(maxCount);

        barChart.getDescription().setEnabled(false);
        barChart.getDescription().setTextSize(0f);
        barChart.setPinchZoom(false);
        barChart.setScaleEnabled(false);

        barChart.setData(barData);

        barChart.setVisibleXRangeMaximum(10);
//        barChart.setVisibleXRangeMinimum(12f);
        if (entries.size() > 10) {
            barChart.moveViewToX(entries.size() - 1);
        }

        barChart.invalidate();
    }

    private int getLegendIndex(ArrayList<LegendEntry> legendEntries, String name) {
        int i = 0;
        for (LegendEntry legendEntry : legendEntries) {
            if (legendEntry.label.equals(name)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }


    @Override
    protected void onUpdateView() {

    }
}
