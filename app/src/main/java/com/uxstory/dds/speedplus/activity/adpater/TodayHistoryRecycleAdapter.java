package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TodayHistoryRecycleAdapter extends RecyclerView.Adapter<TodayHistoryRecycleAdapter.ViewHolder> {

    private final static String TAG = TodayHistoryRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<Event> items;

    public TodayHistoryRecycleAdapter(Context context, List<Event> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<Event> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_today, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

        Event event = items.get(position);
        String index = String.valueOf(items.size() - position);

        holder.event = event;

        holder.txtProductNum.setText(index);
        String time = StringUtil.cutDateTimeToHourMin(event.getMillingStartTime()) + " "+ context.getString(R.string.txt_start) +" ";
        if (event.getType().equals(EventType.MILLING_FINISH.getString()) && event.getElapsedTime() != null) {
            time += "(" + StringUtil.convertTimeToMinSec(event.getElapsedTime()) + ")";
        }
        holder.txtTime.setText(time);

        String workInfo = StringUtil.nullToBlank(event.getPatientName())
                + " - " + StringUtil.nullToBlank(RestorationType.getRestorationString(context, event.getRestoration()))
                + " - " + StringUtil.nullToBlank(event.getBlock());

        holder.txtWorkInfo.setText(workInfo);

        WorkStateType workStateType = WorkStateType.getType(event.getType());
        holder.txtWorkState.setText(workStateType.getNameResId());
        switch (workStateType) {
            case MILLING_STOP:
                holder.iconState.setImageResource(R.drawable.icon_error);
                holder.layoutView.setBackground(null);
                break;
            case MILLING_FINISH:
                holder.iconState.setImageResource(R.drawable.icon_finish);
                holder.layoutView.setBackground(null);
                break;
            default:
                holder.iconState.setImageResource(R.drawable.icon_work);
                holder.layoutView.setBackgroundResource(R.drawable.line_focus);
                break;
        }

        RestorationType restorationType = RestorationType.getType(event.getRestoration());
        holder.iconRestoration.setImageResource(restorationType.getIconNormalResId());

//        ViewUtil.requestLayout((ViewGroup)holder.itemView);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_num)
        TextView txtProductNum;

        @BindView(R.id.icon_type_02_e)
        ImageView iconRestoration;

        @BindView(R.id.txt_time)
        TextView txtTime;

        @BindView(R.id.txt_work_info)
        TextView txtWorkInfo;

        @BindView(R.id.txt_work_state)
        TextView txtWorkState;

        @BindView(R.id.icon_state)
        ImageView iconState;

        @BindView(R.id.layout_view)
        View layoutView;

        Event event;

        public Event getEvent() {
            return event;
        }

        public void setEvent(Event event) {
            this.event = event;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
