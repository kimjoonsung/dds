package com.uxstory.dds.speedplus.notification.type;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.config.Constants;

public enum AppNotificationType {
    NULL("",                                    false, false, false, false, null, -1),
    LONG_TIME_NO_USE_DEVICE_1("no_use_device_1",true, false, false, false, Constants.INTENT_ACTION_WARN_LONG_TIME_NO_USE_DEVICE, R.string.noti_long_no_use_device_warning),
    LONG_TIME_NO_USE_DEVICE_2("no_use_device_2",false, true, false, false, null, R.string.noti_long_no_use_device_warning_help),
    LONG_TIME_NO_USE_APP("no_use_app",          true, false, false, false, null, R.string.noti_long_no_use_app_warning),
    FIRST_PAIRING("entered_pairing",            false, true, false, false, null, R.string.noti_welcome),
    CONFIRM_PAIRING("confirm_pairing",          true, true, false, false, null, R.string.txt_msg_confirm_device),
    SEARCH_NO_DATA("search_no_data",            false, true, false, false, null, R.string.noti_search_no_data),
    DEVICE_DISCONNECTED("device_disconnected",  true, true, false, true, null, R.string.noti_network_warning),
    SYNC_FAIL("sync_fail",                      false, true, false, false, null, R.string.noti_pairing_warning),
    WATER_TURBID("water_turbid",                true, true, false, true, Constants.INTENT_ACTION_GOTO_WATER_STATE, R.string.noti_water_state_warning),
    WATER_CHANGED("water_changed",              true, false, true, true, Constants.INTENT_ACTION_GOTO_WATER_STATE, R.string.noti_confirm_water_change),
    BUR_LEFT_WARNING("bur_left_warning",        true, true, false, true, Constants.INTENT_ACTION_GOTO_BUR_STATE, R.string.noti_left_bur_warning),
    BUR_RIGHT_WARNING("bur_right_warning",      true, true, false, true, Constants.INTENT_ACTION_GOTO_BUR_STATE, R.string.noti_right_bur_warning),
    ;

    private String value;
    boolean backNoti;
    boolean popNoti;
    boolean toastNoti;
    boolean recordNoti;
    String intentAction;
    int msgResId;

    AppNotificationType(String value, boolean backNoti, boolean popNoti, boolean toastNoti, boolean recordNoti, String intentAction, int msgResId) {
        this.value = value;
        this.backNoti = backNoti;
        this.popNoti = popNoti;
        this.toastNoti = toastNoti;
        this.recordNoti = recordNoti;
        this.intentAction = intentAction;
        this.msgResId = msgResId;
    }

    public String getString() {
        return value;
    }

    public boolean isBackNoti() {
        return backNoti;
    }

    public boolean isPopNoti() {
        return popNoti;
    }

    public boolean isToastNoti() {
        return toastNoti;
    }

    public boolean isRecordNoti() {
        return recordNoti;
    }

    public int getMsgResId() {
        return msgResId;
    }

    public int getIconResId() {
        if (isError()) {
            return R.drawable.icon_noti_error;

        } else {
            switch (this) {
                case WATER_TURBID:
                case WATER_CHANGED:
                    return R.drawable.icon_noti_water;
                case BUR_LEFT_WARNING:
                case BUR_RIGHT_WARNING:
                    return R.drawable.icon_noti_bur;
                default:
                    return R.drawable.icon_noti_00;
            }
        }
    }

    public int getToastIconResId() {
        if (isError()) {
            return R.drawable.icon_noti_error;

        } else {
            switch (this) {
                case WATER_TURBID:
                case WATER_CHANGED:
                    return R.drawable.icon_noti_water_white;
                case BUR_LEFT_WARNING:
                case BUR_RIGHT_WARNING:
                    return R.drawable.icon_noti_bur_white;
                default:
                    return R.drawable.icon_noti_error_white;
            }
        }
    }

    public String getIntentAction() {
        return intentAction;
    }

    public static AppNotificationType getType(String type) {
        for (AppNotificationType eventType : AppNotificationType.values()) {
            if (eventType.getString().equals(type)) {
                return eventType;
            }
        }
        return NULL;
    }

    public boolean isError() {
        return ErrorType.getType(getString()) != ErrorType.NULL;
    }
}
