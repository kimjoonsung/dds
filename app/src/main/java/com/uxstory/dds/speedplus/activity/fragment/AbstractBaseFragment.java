package com.uxstory.dds.speedplus.activity.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.AbstractBaseActivity;
import com.uxstory.dds.speedplus.activity.MainActivity;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.help.Contact;
import com.uxstory.dds.speedplus.service.DdsService;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.ButterKnife;
import pe.warrenth.rxbus2.OnRxBusFindDataInterface;
import pe.warrenth.rxbus2.RxBus;
import pe.warrenth.rxbus2.RxBusHelper;
import pe.warrenth.rxbus2.Subscribe;

public abstract class AbstractBaseFragment extends Fragment implements AbstractBaseActivity.onKeyBackPressedListener, FragmentManager.OnBackStackChangedListener, OnRxBusFindDataInterface {

    private final static String TAG = AbstractBaseFragment.class.getSimpleName();

    private int layoutId;

    private Device device;

    private CommunicatorEvent communicatorEvent;

    public DdsService getDdsService() {
        return GlobalApplication.getDdsService();
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogU.d(TAG, "onConfigurationChanged() " + this);
    }

    public void setOrientation() {
        getMainAcivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        getMainAcivity().getWindow().getDecorView().setSystemUiVisibility(GlobalApplication.getDefualtUiOptions());
    }

    @Override
    public void onAttach(@NonNull Context context) {
        LogU.d(TAG, "onAttach() " + this);
        super.onAttach(context);
        setOrientation();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        LogU.d(TAG, "onCreate() " + this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogU.d(TAG, "onCreateView() " + this);
        View view = inflater.inflate(layoutId, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        LogU.d(TAG, "onActivityCreated() " + this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        LogU.d(TAG, "onStart() " + this);
        super.onStart();
    }

    @Override
    public void onResume() {
        LogU.d(TAG, "onResume() " + this);
        super.onResume();
    }

    @Override
    public void onPause() {
        LogU.d(TAG, "onPause() " + this);
        super.onPause();
    }

    @Override
    public void onStop() {
        LogU.d(TAG, "onStop() " + this);
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        LogU.d(TAG, "onDestroyView() " + this);
        super.onDestroyView();
        removeBackStackChangedListener();
        unregisterRxBus();
    }

    @Override
    public void onBackPressed() {
        LogU.d(TAG, "onBackPressed() activity=" + getActivity() + ", this=" + this);
        if (getActivity() != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            int backStackEntryCount = fragmentManager.getBackStackEntryCount();
            LogU.d(TAG, "onBackPressed() BackStackEntryCount=" + backStackEntryCount);
            if (backStackEntryCount > 0) {
                fragmentManager.popBackStack();
                fragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();

            } else {
                AbstractBaseActivity activity = (AbstractBaseActivity) getActivity();
                if (getActivity() != null) {
                    activity.setOnKeyBackPressedListener(null);
                    activity.onBackPressed();
                }
            }
        }
    }

    @Override
    public void onBackStackChanged() {
        LogU.d(TAG, "onBackStackChanged() " + this);
        if (getActivity() != null) {

            getMainAcivity().closeNotiView();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            LogU.d(TAG, "onBackStackChanged() BackStackEntryCount=" + fragmentManager.getBackStackEntryCount());
            List<Fragment> fragments = getFragmentManager().getFragments();
            if (fragments.size() > 0 && fragments.get(fragments.size() - 1) == this) {
                LogU.d(TAG, "onBackStackChanged() popStack=" + this);
                onPopStack();

            } else {
                LogU.d(TAG, "onBackStackChanged() backStack=" + this);
                onBackStack();
            }
        }
    }

    public void addFragment(Activity activity, AbstractBaseFragment fragment) {
        LogU.d(TAG, "addFragment() " + fragment.getClass().getSimpleName());

        getMainAcivity().closeNotiView();

        if (activity instanceof FragmentActivity) {
            FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
            if (fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName()) == null) {

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
                transaction.add(R.id.frame_layout, fragment, fragment.getClass().getSimpleName());
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();

                fragment.addBackStackChangedListener();
            }
        }
    }

    public MainActivity getMainAcivity() {
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            return (MainActivity) activity;
        }
        return null;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public int getLayoutId() {
        return layoutId;
    }

    protected CommunicatorEvent getCommunicatorEvent() {
        return communicatorEvent;
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_DEVICE)
    public void onRxBusEvent(CommunicatorEvent communicatorEvent) {
        LogU.d(TAG, "onRxBusEvent() = " + communicatorEvent.getResponseType() + "," + this);
        this.communicatorEvent = communicatorEvent;
        if(getDdsService() != null) {
            if(getDevice() == null || getDevice() == communicatorEvent.getDevice()) {
                onUpdateView();
//                ViewUtil.requestLayout((ViewGroup)getView());
            }
        }
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_USE_INFO_LINK)
    public void onRxBusUseInfoLinkEvent(String useInfoLink) {
        LogU.d(TAG, "onRxBusEvent() = " + useInfoLink + ", " + this);
        if(useInfoLink != null) {
            if(Constants.USE_INFO_LINK_CONTACT.equals(useInfoLink)) {
                addFragment(getActivity(), new UseInfoContactFragment());

            } else {
                int contentId = getContext().getResources().getIdentifier(useInfoLink, "string", getContext().getPackageName());
                if(contentId > 0) {
                    UseInfoLinkFragment useInfoLinkFragment = new UseInfoLinkFragment();
                    useInfoLinkFragment.setHelpContentStringId(R.string.txt_use_info_title_device, contentId);
                    addFragment(getActivity(), useInfoLinkFragment);
                }
            }
        }
    }

    @Subscribe(eventTag = Constants.RX_BUS_EVENT_MAIL_TO)
    public void onRxBusMailToEvent(String email) {
        LogU.d(TAG, "onRxBusEvent() = " + email + "," + this);
        Contact.sendEmail(getContext(), email);
    }

    public void setOnKeyBackPressedListener () {
        getMainAcivity().setOnKeyBackPressedListener(this);
    }

    public void addBackStackChangedListener() {
        if (getFragmentManager() != null) {
            getFragmentManager().addOnBackStackChangedListener(this);
        }
    }

    public void removeBackStackChangedListener() {
        if (getFragmentManager() != null) {
            getFragmentManager().removeOnBackStackChangedListener(this);
        }
    }

    public void registerRxBus() {
        RxBus.get().register(this, RxBusHelper.getRegistClass(this, AbstractBaseFragment.class));
//        RxBus.get().register(this);
    }

    public void unregisterRxBus() {
        RxBus.get().unResister(this, RxBusHelper.getRegistClass(this, AbstractBaseFragment.class));
//        RxBus.get().unResister(this);
    }

    protected void onPopStack() {
        setOrientation();

        onUpdateView();
//        ViewUtil.requestLayout((ViewGroup)getView());

        setOnKeyBackPressedListener();
        registerRxBus();
    }

    protected void onBackStack() {
        unregisterRxBus();
    }

    protected void onUpdateView() {
        LogU.d(TAG, "onUpdateView() "+ this);
        if(device != null && getDdsService() != null) {
            getDdsService().checkBurState(device, false);
            getDdsService().checkWaterTurbidState(device, false);
        }
    }

    @Override
    public Object getObject() {
        return this;
    }

    @Override
    public int getHashCode() {
        return System.identityHashCode(this);
    }

}
