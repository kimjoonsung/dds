package com.uxstory.dds.speedplus.activity.ui;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.uxstory.dds.speedplus.util.LogU;

import pe.warrenth.rxbus2.RxBus;

public class RxBusClickableSpan extends ClickableSpan {
    private static final String TAG = RxBusClickableSpan.class.getName();

    private boolean mIsPressed;

    private String rxBusEventTag;
    private String rxBusEventValue;

    public void setRxBusEvent(String rxBusEventTag, String rxBusEventValue) {
        this.rxBusEventTag = rxBusEventTag;
        this.rxBusEventValue = rxBusEventValue;
    }

    public void setPressed(boolean isPressed) {
        mIsPressed = isPressed;
    }

    @Override
    public void onClick(View view) {
        LogU.d(TAG, "onClick() " + rxBusEventTag + "=" + rxBusEventValue);
        RxBus.get().send(rxBusEventTag, rxBusEventValue);
    }

    @Override
    public void updateDrawState(TextPaint textPaint) {

        textPaint.bgColor = mIsPressed ? Color.LTGRAY : Color.TRANSPARENT;

        textPaint.setColor(Color.parseColor("#00b5fa"));
        textPaint.setUnderlineText(true);
//      textPaint.setFakeBoldText(true);
    }
}
