package com.uxstory.dds.speedplus.activity.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.help.HelpRenderer;
import com.uxstory.dds.speedplus.util.LogU;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UseInfoRecycleAdapter extends RecyclerView.Adapter<UseInfoRecycleAdapter.ViewHolder> {

    private final static String TAG = UseInfoRecycleAdapter.class.getSimpleName();

    private Context context;
    private List<String> items;
    private String searchStr;

    public UseInfoRecycleAdapter(Context context, List<String> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<String> items) {
        this.items = items;
    }


    public String getSearchStr() {
        return searchStr;
    }

    public void setSearchStr(String searchStr) {
        this.searchStr = searchStr;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_use_info, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);

//            String viewName = useInfoDeviceType.getViewName(context, position)
//                    .replaceAll("res/layout/", "").replaceAll(".xml", "");
//            LogU.d(TAG, "onBindViewHolder() viewName=" + viewName);
//            int viewId = context.getResources().getIdentifier(viewName, "layout", context.getPackageName());
//            LogU.d(TAG, "onBindViewHolder() viewId=" + viewId);
//            View v = LayoutInflater.from(context).inflate(viewId, holder.layoutBody, false);
//            ViewUtil.highlightTextView(context, (ViewGroup) v);
//            holder.layoutBody.addView(v);

//        if(searchStr == null || searchStr.isEmpty()) {
            holder.layoutTopBar.setSelected(false);
            ViewUtil.setVisibility(holder.layoutBody, holder.layoutTopBar.isSelected());
//        }

        HelpRenderer.render(context, items.get(position), holder.txtTitle, holder.layoutBody, searchStr);
    }


    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_top_bar)
        ViewGroup layoutTopBar;

        @BindView(R.id.txt_title)
        TextView txtTitle;

        @BindView(R.id.layout_body)
        ViewGroup layoutBody;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            layoutTopBar.setSelected(true);

            layoutTopBar.setOnClickListener(v -> {
                layoutTopBar.setSelected(!layoutTopBar.isSelected());
                ViewUtil.setVisibility(layoutBody, layoutTopBar.isSelected());
            });
        }
    }
}
