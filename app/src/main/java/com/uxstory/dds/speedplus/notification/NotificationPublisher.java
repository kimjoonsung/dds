package com.uxstory.dds.speedplus.notification;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.PowerManager;

import androidx.collection.ArraySet;
import androidx.core.app.NotificationCompat;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.LockScreenNotificationActivity;
import com.uxstory.dds.speedplus.activity.MainActivity;
import com.uxstory.dds.speedplus.activity.ui.ForegroundManager;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.EventChecker;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.notification.type.CustomEventType;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;

import java.util.Set;

public class NotificationPublisher {
    private final static String TAG = NotificationPublisher.class.getSimpleName();

    private static AlertDialog dialog;

    public static String getIntentAction(Notification notification) {
        NotificationKindType notificationKindType = notification.getNotificationKindType();
        String notificationTypeString = notification.getNotificationTypeString();
        String intentAction = null;
        switch (notificationKindType) {
            case EVENT:
                EventType eventType = EventType.getType(notificationTypeString);
                intentAction = eventType.getIntentAction();
                break;
            case CUSTOM_EVENT:
                CustomEventType customEventType = CustomEventType.getType(notificationTypeString);
                intentAction = customEventType.getIntentAction();
                break;
            case APP:
                AppNotificationType appNotificationType = AppNotificationType.getType(notificationTypeString);
                intentAction = appNotificationType.getIntentAction();
                break;
        }
        return intentAction;
    }

    public static String getNotificationTypeString(Event event) {

        String notificationTypeValue = null;

        Event preEvent = event.getPreEvent();
        EventType eventType = event.getEventType();

        if (eventType.getMsgResId() > 0) {
            notificationTypeValue = eventType.getString();

        } else if (eventType.getMsgResId() == 0 && preEvent != null) {

            if (eventType == EventType.READY) {
                switch (preEvent.getEventType()) {
                    case WARM_UP:
                        notificationTypeValue = CustomEventType.READY_WARM_UP.getString();
                        break;
                    case PUMP:
                        notificationTypeValue = CustomEventType.READY_PUMP.getString();
                        break;
                    case CHANGE_BUR_AND_BLOCK:
                        notificationTypeValue = CustomEventType.READY_CHANGE_BUR_AND_BLOCK.getString();
                        break;
                }

            } else if (eventType == EventType.STOP) {
                switch (preEvent.getEventType()) {
                    case WARM_UP:
                        notificationTypeValue = CustomEventType.STOP_WARM_UP.getString();
                        break;
                    case PUMP:
                        notificationTypeValue = CustomEventType.STOP_PUMP.getString();
                        break;
                }
            }
        }

        return notificationTypeValue;
    }

    public static void showNotification(Context context, Device device, AppNotificationType appNotificationType) {
        showNotification(context, device, new Notification(device, appNotificationType));
    }

    public static void showNotification(Context context, Device device, Event event) {
        Notification notification = NotificationPublisher.getNotification(device, event);
        if(notification != null) {
            showNotification(context, device, notification);

        } else if(event.getEventType() == EventType.READY && EventChecker.isMillingEndEvent(device.getLastEvent().getPreEvent())) {
            device.clearNotiCountMonitoring();
        }
    }

    public static void showNotification(Context context, Device device, Notification notification) {
        if (notification != null) {

            long notiRowId = -1;
            if (device != null && notification.isRecordNoti()) {
                notiRowId = DbQuery.insertNotification(device.getSerial(), notification);
                notification.setNotiRowId(notiRowId);
                device.plusNotiCount();
                device.plusNotiCountMonitoring();
            }

            switch (notification.getNotificationKindType()) {
                case EVENT:
                case CUSTOM_EVENT:
                    if(!Preference.getPreferenceValue(Preference.PreferenceType.NOTI_MILLING)) {
                        return;
                    }
                    break;
                case APP:
                    switch (notification.getAppNotificationType()) {
                        case BUR_LEFT_WARNING:
                        case BUR_RIGHT_WARNING:
                            if(!Preference.getPreferenceValue(Preference.PreferenceType.NOTI_BUR)) {
                                return;
                            }
                            break;
                        case WATER_TURBID:
                            if(!Preference.getPreferenceValue(Preference.PreferenceType.NOTI_WATER)) {
                                return;
                            }
                            break;
                    }
                    break;
            }

            if(notification.getTitle() == null) {
                notification.setTitle(context.getString(R.string.app_name));
            }

            if (ForegroundManager.getInstance().isBackground()) {
                if (notification.isBackNoti()) {
                    NotificationPublisher.showBackgroundNotification(context, notification);
                    NotificationPublisher.showLockScreenNotification(context, notification);
                }

            } else {
                if (notification.isToastNoti()) {
                    ViewUtil.showToast(context, notification.getIconToastId(), notification.getMsgResId());
                }
                if (notification.isPopNoti()) {
                    Activity activity = ForegroundManager.getInstance().getCurrentActivity();
                    if (activity != null) {

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        activity.runOnUiThread(() -> {
                            AlertDialog.Builder builder = ViewUtil.getDialogBuilder(activity, notification.getTitle(), notification.getMsgResId(), notification.getIconResId());
                            builder.setPositiveButton(R.string.txt_confirm, (dialog, i) -> {
                                if (notification.isRecordNoti()) {
                                    if(device != null) {
                                        device.minusNotiCount();
                                        device.minusNotiCountMonitoring();
                                    }

                                    setCheckedNotification(notification.getNotiRowId());

                                    if (activity instanceof MainActivity) {
                                        MainActivity mainActivity = (MainActivity) activity;
                                        if (mainActivity.isDeviceView()) {
                                            mainActivity.updateNotiCount(device);
                                        } else {
                                            mainActivity.updateNotiCount(null);
                                        }
                                    }
                                }

                                String action = NotificationPublisher.getIntentAction(notification);
                                if (action != null) {
                                    Intent notificationIntent = new Intent(context, MainActivity.class);
                                    notificationIntent.setAction(action);
                                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    if (notification.getDevice() != null) {
                                        notificationIntent.putExtra(Constants.INTENT_EXTRA_NAME_SERIAL, notification.getDevice().getSerial());
                                    }
                                    notificationIntent.putExtra(Constants.INTENT_EXTRA_NAME_NOTI_ROW_ID, notification.getNotiRowId());
                                    context.startActivity(notificationIntent);
                                }
                                dialog.dismiss();

                            });

                            dialog = ViewUtil.showDialog(activity, builder);
                        });
                    }
                }
            }
        }
    }

    public static void setCheckedNotification(long notiRowId) {
        if(notiRowId > 0) {
            DbQuery.updateCheckNotification(notiRowId);
        }
    }

    public static void setCheckedAllNotification(Device device) {
        DbQuery.updateCheckAllNotification();
    }

    public static Notification getNotification(Device device, Event event) {
        Notification notification = null;

        if (event != null) {
            String notificationTypeString = NotificationPublisher.getNotificationTypeString(event);

            EventType eventType = EventType.getType(notificationTypeString);
            if (eventType != EventType.NULL) {
                notification = new Notification(device, event);

            } else {
                CustomEventType customEventType = CustomEventType.getType(notificationTypeString);
                if (customEventType != CustomEventType.NULL) {
                    notification = new Notification(device, customEventType);
                }
            }
        }

        return notification;
    }

    public static void showLockScreenNotification(Context context, Notification notification) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null && !powerManager.isInteractive()) {
            Intent notificationIntent = new Intent(context, LockScreenNotificationActivity.class);
            String action = NotificationPublisher.getIntentAction(notification);
//            if (action == null) {
//                action = Constants.INTENT_ACTION_GOTO_DASHBOARD;
//            }
            notificationIntent.setAction(action);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (notification.getDevice() != null) {
                notificationIntent.putExtra("serial", notification.getDevice().getSerial());
            }
            notificationIntent.putExtra("title", notification.getTitle());
            notificationIntent.putExtra("msg", context.getString(notification.getMsgResId()));
            notificationIntent.putExtra("iconId", notification.getIconResId());

            context.startActivity(notificationIntent);
        }
    }

    public static void showBackgroundNotification(Context context, Notification notification) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        String action = NotificationPublisher.getIntentAction(notification);
//        if(action == null) {
//            action = Constants.INTENT_ACTION_GOTO_DASHBOARD;
//        }
        notificationIntent.setAction(action);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (notification.getDevice() != null) {
            notificationIntent.putExtra(Constants.INTENT_EXTRA_NAME_SERIAL, notification.getDevice().getSerial());
        }
        notificationIntent.putExtra(Constants.INTENT_EXTRA_NAME_IS_RECORD_NOTI, notification.isRecordNoti());
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.DDS_NOTIFICATION_CHANNEL_NOTI_ID)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), notification.getIconResId()))
                .setContentTitle(notification.getTitle())
                .setContentText(context.getString(notification.getMsgResId()))
                // 더 많은 내용이라서 일부만 보여줘야 하는 경우 아래 주석을 제거하면 setContentText에 있는 문자열 대신 아래 문자열을 보여줌
                //.setStyle(new NotificationCompat.BigTextStyle().bigText("더 많은 내용을 보여줘야 하는 경우..."))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent) // 사용자가 노티피케이션을 탭시 ResultActivity로 이동하도록 설정
                .setAutoCancel(true);

        //OREO API 26 이상에서는 채널 필요
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            builder.setSmallIcon(notification.getIconResId()); //R.drawable.icon_info_menu_01
            CharSequence channelName = Constants.DDS_NOTIFICATION_CHANNEL_NOTI_NAME;
            String description = Constants.DDS_NOTIFICATION_CHANNEL_NOTI_NAME;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(Constants.DDS_NOTIFICATION_CHANNEL_NOTI_ID, channelName, importance);
            channel.setDescription(description);

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);

        } else {
            builder.setSmallIcon(R.mipmap.icon_info_menu_01); // Oreo 이하에서 mipmap 사용하지 않으면 Couldn't create icon: StatusBarIcon 에러남
        }

        assert notificationManager != null;
        notificationManager.notify(Constants.DDS_NOTIFICATION_ID_NOTI, builder.build()); // 고유숫자로 노티피케이션 동작시킴

    }

    public static Set<String> searchNotificationTypeString(Context context, String searchStr) {
        Set<String> set = new ArraySet<>();
        if(searchStr != null) {
            String[] splitSearchStr = searchStr.trim().toLowerCase().split("\\s");
            for (String s : splitSearchStr) {
                if(s.isEmpty()) {
                    continue;
                }
                for (EventType type : EventType.values()) {
                    if (type.getMsgResId() > 0) {
                        if (context.getString(type.getMsgResId()).toLowerCase().contains(s)) {
                            set.add(type.getString());
                        }
                    }
                }
            }
            for (String s : splitSearchStr) {
                if(s.isEmpty()) {
                    continue;
                }
                for (CustomEventType type : CustomEventType.values()) {
                    if (type.getMsgResId() > 0) {
                        if (context.getString(type.getMsgResId()).toLowerCase().contains(s)) {
                            set.add(type.getString());
                        }
                    }
                }
            }
            for (String s : splitSearchStr) {
                if(s.isEmpty()) {
                    continue;
                }
                for (AppNotificationType type : AppNotificationType.values()) {
                    if (type.getMsgResId() > 0) {
                        if (context.getString(type.getMsgResId()).toLowerCase().contains(s)) {
                            set.add(type.getString());
                        }
                    }
                }
            }
        }
        return set;
    }
}
