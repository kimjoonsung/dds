package com.uxstory.dds.speedplus.device;

public class WorkInfo {
    private String patientName;
    private String block;
    private String restoration;
    private String leftBurName;
    private String rightBurName;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getRestoration() {
        return restoration;
    }

    public void setRestoration(String restoration) {
        this.restoration = restoration;
    }

    public String getLeftBurName() {
        return leftBurName;
    }

    public void setLeftBurName(String leftBurName) {
        this.leftBurName = leftBurName;
    }

    public String getRightBurName() {
        return rightBurName;
    }

    public void setRightBurName(String rightBurName) {
        this.rightBurName = rightBurName;
    }
}
