package com.uxstory.dds.speedplus.activity.adpater;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.uxstory.dds.speedplus.activity.fragment.GuideFragment;

import java.util.List;

public class ViewPagerFragmentStateAdapter extends FragmentStateAdapter {

    private List<Fragment> list;

    public ViewPagerFragmentStateAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    public void setList(List<Fragment> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
