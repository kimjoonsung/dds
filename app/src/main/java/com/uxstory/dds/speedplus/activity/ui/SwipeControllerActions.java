package com.uxstory.dds.speedplus.activity.ui;

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}

}
