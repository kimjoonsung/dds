package com.uxstory.dds.speedplus.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.EventChecker;
import com.uxstory.dds.speedplus.model.DataCountModel;
import com.uxstory.dds.speedplus.model.NotificationModel;
import com.uxstory.dds.speedplus.model.type.BurType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.notification.Notification;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.NotificationKindType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.ArrayList;
import java.util.Set;

public class DbQuery {

    final static String TAG = DbQuery.class.getSimpleName();

    public static void insertEventHistory(String serial, ArrayList<Event> eventList) {
        Event millingStartEvent = null;
        for (Event event : eventList) {
            if (EventChecker.isMillingStartEvent(event)) {
                millingStartEvent = event;

            } else if (EventChecker.isMillingReStartEvent(event) && millingStartEvent != null) {
                millingStartEvent.setMillingStartTime(event.getMillingStartTime());

            } else if (EventChecker.isMillingEndEvent(event) && millingStartEvent != null) {
                event.setPatientName(millingStartEvent.getPatientName());
                event.setBlock(millingStartEvent.getBlock());
                event.setRestoration(millingStartEvent.getRestoration());
                event.setLeftBurName(millingStartEvent.getLeftBurName());
                event.setRightBurName(millingStartEvent.getRightBurName());
                event.setMillingStartTime(millingStartEvent.getTime());
            }
            if (EventChecker.isMillingStopEvent(event)) {
                millingStartEvent = null;
            }

            long result = insertEvent(serial, event, true);

            //TODO test
//            if (result > -1) {
//                Notification notification = NotificationPublisher.getNotification(null, event);
//                if (notification != null) {
//                    insertNotification(serial, notification);
//                }
//            }
        }
    }

    public static long insertEvent(String serial, Event event, boolean fromHistory) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_URI, event.getUri());
        values.put(Databases.CR_BOOT_INDEX, event.getBootIndex());
        values.put(Databases.CR_WORK_STEP, event.getWorkStep());
        values.put(Databases.CR_SERIAL, serial);
        values.put(Databases.CR_TYPE, event.getType());
        values.put(Databases.CR_PATIENT_NAME, event.getPatientName());
        values.put(Databases.CR_BLOCK, event.getBlock());
        values.put(Databases.CR_RESTORATION, event.getRestoration());
        values.put(Databases.CR_LEFT_BUR_NAME, event.getLeftBurName());
        values.put(Databases.CR_RIGHT_BUR_NAME, event.getRightBurName());
        values.put(Databases.CR_ELAPSED_TIME, event.getElapsedTime());
        values.put(Databases.CR_MILLING_START_TIME, event.getMillingStartTime());
        values.put(Databases.CR_EVENT_TIME, event.getTime());

        if (fromHistory) {
            values.put(Databases.CR_HISTORY, "Y");
        }
        return DbOpenHelper.DB.insertWithOnConflict(Databases.TABLE_EVENT_HISTORY, null, values, SQLiteDatabase.CONFLICT_IGNORE);
//        DbOpenHelper.DB.insert(Databases.TABLE_EVENT_HISTORY, null, values);
    }

    public static Device selectDevice(String serial) {
        String[] columns = new String[]{Databases.CR_NAME, Databases.CR_SERIAL, Databases.CR_IP, Databases.CR_PORT, Databases.CR_SESSION, Databases.CR_ENCRYPT_KEY};
        String selection = Databases.CR_SERIAL + "=" + serial;
        Cursor cursor = DbOpenHelper.DB.query(Databases.TABLE_DEVICE, columns, selection, null, null, null, null);

        Device device = null;
        while (cursor.moveToNext()) {
            device = new Device();
            device.setName(cursor.getString(cursor.getColumnIndex(Databases.CR_NAME)));
            device.setSerial(cursor.getString(cursor.getColumnIndex(Databases.CR_SERIAL)));
            device.setIp(cursor.getString(cursor.getColumnIndex(Databases.CR_IP)));
            device.setPort(cursor.getInt(cursor.getColumnIndex(Databases.CR_PORT)));
            device.setSession(cursor.getString(cursor.getColumnIndex(Databases.CR_SESSION)));
            device.setEncryptKey(cursor.getString(cursor.getColumnIndex(Databases.CR_ENCRYPT_KEY)));
        }
        return device;
    }

    public static ArrayList<Device> selectDeviceList(boolean excludeUnpairedDevice) {
        String query = "SELECT * FROM " + Databases.TABLE_DEVICE;
        if (excludeUnpairedDevice) {
            query += " WHERE " + Databases.CR_SESSION + " is not NULL and " + Databases.CR_ENCRYPT_KEY + " is not NULL ";
        }
        query += " ORDER BY " + Databases.CR_REG_TIME + " desc;";
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<Device> deviceList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Device device = new Device();
            device.setName(cursor.getString(cursor.getColumnIndex(Databases.CR_NAME)));
            device.setSerial(cursor.getString(cursor.getColumnIndex(Databases.CR_SERIAL)));
            device.setIp(cursor.getString(cursor.getColumnIndex(Databases.CR_IP)));
            device.setPort(cursor.getInt(cursor.getColumnIndex(Databases.CR_PORT)));
            device.setSession(cursor.getString(cursor.getColumnIndex(Databases.CR_SESSION)));
            device.setEncryptKey(cursor.getString(cursor.getColumnIndex(Databases.CR_ENCRYPT_KEY)));
            device.setLastEventHistorySyncDate(cursor.getString(cursor.getColumnIndex(Databases.CR_SYNC_TIME)));
            deviceList.add(device);
        }
        cursor.close();

        return deviceList;
    }

    public static long updateDevice(Device device, boolean updateRegTime) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_NAME, device.getName());
        values.put(Databases.CR_SERIAL, device.getSerial());
        values.put(Databases.CR_IP, device.getIp());
        values.put(Databases.CR_PORT, device.getPort());
        values.put(Databases.CR_SESSION, device.getSession());
        values.put(Databases.CR_ENCRYPT_KEY, device.getEncryptKey());
        values.put(Databases.CR_SYNC_TIME, device.getLastEventHistorySyncDate());
        if(updateRegTime) {
            values.put(Databases.CR_REG_TIME, StringUtil.getCurrentDateTime());
        }
        long result = DbOpenHelper.DB.insertWithOnConflict(Databases.TABLE_DEVICE, null, values, SQLiteDatabase.CONFLICT_IGNORE);

        if (result < 1) {
            result = DbOpenHelper.DB.updateWithOnConflict(Databases.TABLE_DEVICE, values, Databases.CR_SERIAL + "=?", new String[]{device.getSerial()}, SQLiteDatabase.CONFLICT_REPLACE);
        }

        return result;
    }

    public static long updateDeviceName(String serial, String deviceName) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_NAME, deviceName);

        return DbOpenHelper.DB.updateWithOnConflict(Databases.TABLE_DEVICE, values, Databases.CR_SERIAL + "=?", new String[]{serial}, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static int deleteDevice(Device device) {
        return DbOpenHelper.DB.delete(Databases.TABLE_DEVICE, Databases.CR_SERIAL + "=?", new String[]{device.getSerial()});
    }

    public static int deleteHistory(Event event) {
        return DbOpenHelper.DB.delete(Databases.TABLE_EVENT_HISTORY, Databases.CR_URI + "=?", new String[]{event.getUri()});
    }

    public static String selectLastEventHistoryDate(String serial) {
        String date = "";
        String query = "select max(pairing_time) from device " +
                "where serial='" + serial + "' " +
                "and pairing_time is not null;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);
        if (cursor.moveToNext()) {
            date = cursor.getString(0);
        }
        cursor.close();

        return date;
    }

    public static ArrayList<DataCountModel> selectRestorationCountList(String date) {
        date = StringUtil.convertDateToRawFormat(date);
        String query = "select count(restoration) count, restoration from event_history " +
                "where type='milling_finish' " +
                "and restoration is not null " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "group by upper(substr(restoration,0,3)) order by count desc, restoration asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setRestorationType(RestorationType.getType(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION))));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static int selectLastMillingFinishCount(int preDay) {
        int count = 0;
        String query = "select count(*) from event_history " +
                " where type='milling_finish' " +
                " and block is not null " +
                " and strftime('%Y-%m-%d', event_time) = strftime('%Y-%m-%d', date('now', '-" + preDay + " day', 'localtime'));"; //or type='milling_stop'

        LogU.d(TAG, "query=" + query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();

        return count;
    }

    public static String selectLastEventTime() {
        int count = 0;
        String query = "select max(event_time) from event_history;";

        LogU.d(TAG, "query=" + query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        String time = null;
        if (cursor.moveToNext()) {
            time = cursor.getString(0);
        }
        cursor.close();

        return time;
    }

    public static int selectMillingFinishCount(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
        int count = 0;
        String query = "select count(*)  from event_history " +
                " where serial='" + serial + "' " +
                " and type='milling_finish' " +
                " and block is not null " +
                " and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "';"; //or type='milling_stop'

        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();

        return count;
    }

    public static int selectMillingFinishCountFromDateTime(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
        int count = 0;
        String query = "select count(*) from event_history " +
                " where serial='" + serial + "' " +
                " and type='milling_finish' " +
                " and block is not null " +
                " and event_time >= '" + date + "';"; //or type='milling_stop'

        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();

        return count;
    }

    public static ArrayList<DataCountModel> selectLastBurInfo(String serial) {
        String query = "select left_bur_name, right_bur_name from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_start' " +
                "and left_bur_name is not null " +
                "and right_bur_name is not null " +
                "order by event_time desc limit 1;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setName(cursor.getString(0));
            list.add(data);

            data = new DataCountModel();
            data.setName(cursor.getString(1));
            list.add(data);
        }

        cursor.close();

        return list;
    }

    public static ArrayList<Event> selectLastChangeBurDate(String serial) {
        String query = "select type, max(event_time) from event_history " +
                "where serial='" + serial + "' " +
                "and (type='change_left_bur' or type='change_right_bur' or type='change_both_bur')  " +
                "group by type;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<Event> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            Event data = new Event();
            data.setType(cursor.getString(0));
            data.setTime(cursor.getString(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static long deleteWorkEndHistory(String uri) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_DELETED, "Y");

        return DbOpenHelper.DB.updateWithOnConflict(Databases.TABLE_EVENT_HISTORY, values, Databases.CR_URI + "=?", new String[]{uri}, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static ArrayList<Event> selectWorkEndHistoryList(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
        String query = "SELECT * from event_history " +
                " where serial='" + serial + "' " +
//                " and " + Databases.CR_MILLING_START_TIME + " is not NULL and " + Databases.CR_MILLING_START_TIME + " != '' "+
                " and (type='milling_finish' or type='milling_stop') " +
                " and (deleted is null or deleted != 'Y') " +
                " and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                " ORDER BY " + Databases.CR_EVENT_TIME + " DESC;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<Event> list = new ArrayList<>();
        while (cursor.moveToNext()) {

            Event event = new Event();
            event.setType(cursor.getString(cursor.getColumnIndex(Databases.CR_TYPE)));
            event.setPatientName(cursor.getString(cursor.getColumnIndex(Databases.CR_PATIENT_NAME)));
            event.setBlock(cursor.getString(cursor.getColumnIndex(Databases.CR_BLOCK)));
            event.setRestoration(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION)));
            event.setElapsedTime(cursor.getString(cursor.getColumnIndex(Databases.CR_ELAPSED_TIME)));
            event.setMillingStartTime(cursor.getString(cursor.getColumnIndex(Databases.CR_MILLING_START_TIME)));
            event.setTime(cursor.getString(cursor.getColumnIndex(Databases.CR_EVENT_TIME)));
            event.setUri(cursor.getString(cursor.getColumnIndex(Databases.CR_URI)));

            list.add(event);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectBlockCountListFromDate(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
        String query = "select count(block) count, block from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= '" + date + "' " +
                "group by block order by count desc, block asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishRestorationCounts(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
        String query = "select count(restoration) count, restoration from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and restoration is not null " +
                "and event_time >= '" + date + "' " +
                "group by upper(substr(restoration,0,3)) order by count desc, restoration asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setRestorationType(RestorationType.getType(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION))));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectMillingFinishBlocks(String serial, String startDate, String endDate) {
        startDate = StringUtil.convertDateToRawFormat(startDate);
        endDate = StringUtil.convertDateToRawFormat(endDate);
        endDate = (endDate.length() <= 10) ? endDate +" 23:59:59" : endDate;

        String query = "select block from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= '" + startDate + "' " +
                "and event_time <= '" + endDate + "' " +
                "group by block order by block;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectMillingFinishBlocks(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-36 month" : preDaysCondition;

        String query = "select block from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by block order by block;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishBlockCounts(String serial, String startDate, String endDate) {
        startDate = StringUtil.convertDateToRawFormat(startDate);
        endDate = StringUtil.convertDateToRawFormat(endDate);
        endDate = (endDate.length() <= 10) ? endDate +" 23:59:59" : endDate;

        String query = "select count(*), block, strftime('%Y-%m-%d', event_time) date from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= '" + startDate + "' " +
                "and event_time <= '" + endDate + "' " +
                "group by date, block order by date asc, block;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            data.setTime(cursor.getString(2));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishBlockCounts(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-36 month" : preDaysCondition;

        String query = "select count(*), block, strftime('%Y-%m', event_time) date from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by date, block order by date asc, block;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            data.setTime(cursor.getString(2));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishDatesCount(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-1 month" : preDaysCondition;

        String query = "select strftime('%Y-%m-%d', event_time) event_date, count(*) from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setTime(cursor.getString(0));
            data.setCount(cursor.getInt(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishDatesCount(String serial, String startDate, String endDate) {
        startDate = StringUtil.convertDateToRawFormat(startDate);
        endDate = StringUtil.convertDateToRawFormat(endDate);
        endDate = (endDate.length() <= 10) ? endDate +" 23:59:59" : endDate;

        String query = "select strftime('%Y-%m-%d', event_time) event_date, count(*) from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= '" + startDate + "' " +
                "and event_time <= '" + endDate + "' " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setTime(cursor.getString(0));
            data.setCount(cursor.getInt(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishMonthsCount(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-36 month" : preDaysCondition;

        String query = "select strftime('%Y-%m', event_time) event_date, count(*) from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setTime(cursor.getString(0));
            data.setCount(cursor.getInt(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectMillingFinishDates(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-1 month" : preDaysCondition;

        String query = "select strftime('%Y-%m-%d', event_time) event_date from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectMillingFinishDates(String serial, String startDate, String endDate) {
        startDate = StringUtil.convertDateToRawFormat(startDate);
        endDate = StringUtil.convertDateToRawFormat(endDate);
        endDate = (endDate.length() <= 10) ? endDate +" 23:59:59" : endDate;

        String query = "select strftime('%Y-%m-%d', event_time) event_date from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= '" + startDate + "' " +
                "and event_time <= '" + endDate + "' " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectMillingFinishMonths(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-36 month" : preDaysCondition;

        String query = "select strftime('%Y-%m', event_time) event_date from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and event_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by event_date order by event_date asc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectBlockCountList(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);

        String query = "select count(block) count, block from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and block is not null " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "group by block order by count desc, block asc;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectRestorationCountList(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);

        String query = "select count(restoration) count, restoration from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and restoration is not null " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "group by upper(substr(restoration,0,3)) order by count desc, restoration asc;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setRestorationType(RestorationType.getType(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION))));
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectBurCountList(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);

        String query = "select count(left_bur_name) count, left_bur_name from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and left_bur_name is not null " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "group by left_bur_name order by count desc, left_bur_name;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();

        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            data.setBurType(BurType.LEFT);
            list.add(data);
        }
        cursor.close();

        query = "select count(right_bur_name) count, right_bur_name from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "and right_bur_name is not null " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "group by right_bur_name order by count desc, right_bur_name;";
        LogU.d(TAG, query);
        cursor = DbOpenHelper.DB.rawQuery(query, null);

        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            data.setBurType(BurType.RIGHT);
            list.add(data);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<DataCountModel> selectMillingFinishDeviceCounts(String date) {
        date = StringUtil.convertDateToRawFormat(date);

        String query = "select count(event_history.uri) count, device.name " +
                "from device left outer join event_history on device.serial=event_history.serial " +
                "and type='milling_finish' " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', event_time) = '" + date + "' " +
                "where device.session is not null and device.session != '' " +
                "group by device.serial order by device.reg_time desc, device.name desc;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<DataCountModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            DataCountModel data = new DataCountModel();
            data.setCount(cursor.getInt(0));
            data.setName(cursor.getString(1));
            list.add(data);
        }
        cursor.close();

        return list;
    }


    public static Event selectLastMillingFinishEvent(String serial) {
        String query = "select * from event_history " +
                "where serial = '" + serial + "' " +
                "and type='milling_finish' " +
                "order by event_time desc limit 1;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        Event event = null;
        if (cursor.moveToNext()) {
            event = new Event();
            event.setType(cursor.getString(cursor.getColumnIndex(Databases.CR_TYPE)));
            event.setPatientName(cursor.getString(cursor.getColumnIndex(Databases.CR_PATIENT_NAME)));
            event.setBlock(cursor.getString(cursor.getColumnIndex(Databases.CR_BLOCK)));
            event.setRestoration(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION)));
            event.setMillingStartTime(cursor.getString(cursor.getColumnIndex(Databases.CR_MILLING_START_TIME)));
            event.setElapsedTime(cursor.getString(cursor.getColumnIndex(Databases.CR_ELAPSED_TIME)));
            event.setTime(cursor.getString(cursor.getColumnIndex(Databases.CR_EVENT_TIME)));
            event.setLeftBurName(cursor.getString(cursor.getColumnIndex(Databases.CR_LEFT_BUR_NAME)));
            event.setRightBurName(cursor.getString(cursor.getColumnIndex(Databases.CR_RIGHT_BUR_NAME)));
        }
        cursor.close();

        return event;
    }

    public static ArrayList<Event> selectLastEventList(String serial) {
        String query = "SELECT * FROM event_history " +
                " where serial = '" + serial + "' " +
                " and _ROWID_ >= (select max(_ROWID_) from event_history where serial = '" + serial + "' and type='ready') " +
                " ORDER BY _ROWID_;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<Event> list = new ArrayList<>();
        while (cursor.moveToNext()) {

            Event event = new Event();
            event.setType(cursor.getString(cursor.getColumnIndex(Databases.CR_TYPE)));
            event.setPatientName(cursor.getString(cursor.getColumnIndex(Databases.CR_PATIENT_NAME)));
            event.setBlock(cursor.getString(cursor.getColumnIndex(Databases.CR_BLOCK)));
            event.setRestoration(cursor.getString(cursor.getColumnIndex(Databases.CR_RESTORATION)));
            event.setMillingStartTime(cursor.getString(cursor.getColumnIndex(Databases.CR_MILLING_START_TIME)));
            event.setElapsedTime(cursor.getString(cursor.getColumnIndex(Databases.CR_ELAPSED_TIME)));
            event.setTime(cursor.getString(cursor.getColumnIndex(Databases.CR_EVENT_TIME)));
            event.setLeftBurName(cursor.getString(cursor.getColumnIndex(Databases.CR_LEFT_BUR_NAME)));
            event.setRightBurName(cursor.getString(cursor.getColumnIndex(Databases.CR_RIGHT_BUR_NAME)));

            list.add(event);
        }
        cursor.close();

        return list;
    }

    public static long insertNotification(String serial, Notification notification) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_SERIAL, serial);
        values.put(Databases.CR_NOTI_TYPE, notification.getNotificationTypeString());
        values.put(Databases.CR_NOTI_KIND, notification.getNotificationKindType().toString());
        if (notification.getTime() != null) {
            values.put(Databases.CR_REG_TIME, notification.getTime());
        }

        return DbOpenHelper.DB.insertWithOnConflict(Databases.TABLE_NOTIFICATION, null, values, SQLiteDatabase.CONFLICT_IGNORE);
//        DbOpenHelper.DB.insert(Databases.TABLE_EVENT_HISTORY, null, values);
    }

    public static void updateCheckNotification(long notiRowId) {
        //nothing to do
    }

    public static void updateCheckAllNotification() {
        //nothing to do
    }

    public static ArrayList<NotificationModel> selectNotiDeviceList(String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-1 month" : preDaysCondition;

        String query = "select notification.serial, device.name, max(notification.reg_time) noti_time " +
                "from device inner join notification on device.serial=notification.serial " +
                "and notification.reg_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) " +
                "group by device.serial order by noti_time desc, device.name;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<NotificationModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            NotificationModel noti = new NotificationModel();
            noti.setSerial(cursor.getString(0));
            noti.setName(cursor.getString(1));
            noti.setHeader(true);
            list.add(noti);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<String> selectNotiDates(String serial, Set<String> notiTypeList, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-1 month" : preDaysCondition;

        String query = "select strftime('%Y.%m.%d', reg_time) reg_date from notification " +
                "where serial='" + serial + "' " +
                "and reg_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) ";

        if (notiTypeList != null && notiTypeList.size() > 0) {
            String searchQuery = "and noti_type in (";
            int i = 0;
            for (String notiType : notiTypeList) {
                if (i > 0) {
                    searchQuery += ", ";
                }
                searchQuery += "'" + notiType + "'";
                i++;
            }
            searchQuery += ") ";
            query += searchQuery;
        }
        query += "group by reg_date order by reg_date desc;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public static ArrayList<NotificationModel> selectNotiMessageList(String serial, String date, Set<String> notiTypeList) {
        date = StringUtil.convertDateToRawFormat(date);

        String query = "select noti_type, noti_kind, strftime('%H:%M', reg_time) reg_date from notification " +
                "where serial='" + serial + "' " +
                "and strftime('" + StringUtil.getDateDbFormat(date) + "', reg_time)='" + date + "' ";

        if (notiTypeList != null && notiTypeList.size() > 0) {
            String searchQuery = "and noti_type in (";
            int i = 0;
            for (String notiType : notiTypeList) {
                if (i > 0) {
                    searchQuery += ", ";
                }
                searchQuery += "'" + notiType + "'";
                i++;
            }
            searchQuery += ") ";
            query += searchQuery;
        }
        query += "order by reg_time desc;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<NotificationModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            NotificationModel noti = new NotificationModel();
            noti.setNotificationTypeString(cursor.getString(0));
            noti.setNotificationKindType(NotificationKindType.getType(cursor.getString(1)));
            noti.setRegTime(cursor.getString(2));
            noti.setSerial(serial);
            list.add(noti);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<NotificationModel> selectNotiMessageList(String serial, String preDaysCondition) {
        preDaysCondition = (preDaysCondition == null) ? "-1 month" : preDaysCondition;

        String query = "select noti_type, noti_kind, reg_time from notification " +
                "where serial='" + serial + "' " +
                "and reg_time >= strftime('%Y-%m-%d', date('now', '" + preDaysCondition + "', 'localtime')) ";

        query += "order by reg_time desc;";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        ArrayList<NotificationModel> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            NotificationModel noti = new NotificationModel();
            noti.setNotificationTypeString(cursor.getString(0));
            noti.setNotificationKindType(NotificationKindType.getType(cursor.getString(1)));
            noti.setRegTime(cursor.getString(2));
            noti.setSerial(serial);
            list.add(noti);
        }
        cursor.close();

        return list;
    }

    public static ArrayList<NotificationModel> selectLastNotiMessageList(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);

        String startFinishDate = null;

        ArrayList<NotificationModel> list = new ArrayList<>();

        String query = "select IFNULL(max(event_time), '') from event_history " +
                "where serial='" + serial + "' " +
                "and event_time < '" + date + "' " +
                "and (type='milling_finish' or type='milling_stop');";
        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        if(cursor.moveToNext() && !cursor.getString(0).isEmpty()) {
            startFinishDate = cursor.getString(0);
        }

        query = "select noti_type, noti_kind, reg_time from notification " +
                "where serial='" + serial + "' " +
                "and reg_time <= '" + date +"' ";
        if(startFinishDate != null) {
            query += "and reg_time > '"+ startFinishDate +"' ";
        }
        query += " order by reg_time desc;";

        LogU.d(TAG, query);

        cursor = DbOpenHelper.DB.rawQuery(query, null);

        while (cursor.moveToNext()) {
            NotificationModel noti = new NotificationModel();
            noti.setNotificationTypeString(cursor.getString(0));
            noti.setNotificationKindType(NotificationKindType.getType(cursor.getString(1)));
            noti.setRegTime(cursor.getString(2));
            noti.setSerial(serial);
            list.add(noti);
        }
        cursor.close();

        return list;
    }

    public static void insertWaterChange(String serial) {
        ContentValues values = new ContentValues();
        values.put(Databases.CR_SERIAL, serial);

        DbOpenHelper.DB.insertWithOnConflict(Databases.TABLE_WATER_CHANGE_HISTORY, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public static String selectLastWaterChangeDate(String serial) {
        String query = "select max(change_time) from water_change_history " +
                "where serial = '" + serial + "' " +
                "and change_time is not null;";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        String date = null;
        while (cursor.moveToNext()) {
            date = cursor.getString(0);
        }
        cursor.close();

        return date;
    }

    public static long selectWorkingTimeAfterWaterChange(String serial, String date) {
        date = StringUtil.convertDateToRawFormat(date);
//        String date = selectLastWaterChangeDate(serial);

        date = (date == null) ? "" : date;

        String query = "select sum(strftime('%s', elapsed_time) - strftime('%s', '00:00:00')) from event_history " +
                "where serial = '" + serial + "' " +
                "and elapsed_time is not null " +
                "and type='milling_finish' " +
                "and event_time >= '" + date + "';";

        LogU.d(TAG, query);
        Cursor cursor = DbOpenHelper.DB.rawQuery(query, null);

        long time = 0;
        while (cursor.moveToNext()) {
            time = cursor.getLong(0);
        }
        cursor.close();

        return time;
    }
}
