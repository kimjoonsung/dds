package com.uxstory.dds.speedplus.activity.ui;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.uxstory.dds.speedplus.activity.LockScreenNotificationActivity;
import com.uxstory.dds.speedplus.activity.MainActivity;
import com.uxstory.dds.speedplus.activity.PairingActivity;

public class ForegroundManager implements Application.ActivityLifecycleCallbacks {
    private static ForegroundManager instance;
    private Activity currentActivity;
    private Activity mainActivity;

    public static void init(Application app) {
        if (instance == null) {
            instance = new ForegroundManager();
            app.registerActivityLifecycleCallbacks(instance);
        }
    }

    public static ForegroundManager getInstance() {
        return instance;
    }

    private ForegroundManager() {
    }

    private AppStatus mAppStatus;

    public AppStatus getAppStatus() {
        return mAppStatus;
    }

    // check if app is return foreground
    public boolean isBackground() {
        return mAppStatus == null || mAppStatus.ordinal() == AppStatus.BACKGROUND.ordinal() || currentActivity instanceof LockScreenNotificationActivity;
    }

    public boolean isForground() {
        if(mAppStatus == null) {
            return false;
        }
        return mAppStatus.ordinal() > AppStatus.BACKGROUND.ordinal() && !(currentActivity instanceof LockScreenNotificationActivity);
    }

    public Activity getMainActivity() {
        return mainActivity;
    }

    public enum AppStatus {
        BACKGROUND, // app is background
        RETURNED_TO_FOREGROUND, // app returned to foreground(or first launch)
        FOREGROUND; // app is foreground
    }


    // running activity count
    private int running = 0;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        currentActivity = activity;
        if(activity instanceof MainActivity) {
            this.mainActivity = activity;
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;
        if (++running == 1) {
            mAppStatus = AppStatus.RETURNED_TO_FOREGROUND;
        } else if (running > 1) {
            mAppStatus = AppStatus.FOREGROUND;
        } else {
            mAppStatus = AppStatus.BACKGROUND;
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (--running == 0) {
            mAppStatus = AppStatus.BACKGROUND;
            if(currentActivity instanceof LockScreenNotificationActivity) {
                currentActivity = null;
            }
        }
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }
}
