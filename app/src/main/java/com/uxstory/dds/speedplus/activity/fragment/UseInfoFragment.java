package com.uxstory.dds.speedplus.activity.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.ui.OnSingleClickListener;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class UseInfoFragment extends AbstractBaseFragment {

    private final static String TAG = UseInfoFragment.class.getSimpleName();

    @BindView(R.id.use_device)
    View useDeivce;

    @BindView(R.id.use_app)
    View useApp;

    @BindView(R.id.use_help)
    View useHelp;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_use_info);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        useDeivce.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addFragment(getActivity(), new UseInfoDeviceFragment());
            }
        });

        useApp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addFragment(getActivity(), new UseInfoAppFragment());
            }
        });

        useHelp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addFragment(getActivity(), new UseInfoContactFragment());
            }
        });
    }

    @Override
    protected void onUpdateView() {
        getMainAcivity().updateToolbarMain(R.id.menu_info);
    }
}
