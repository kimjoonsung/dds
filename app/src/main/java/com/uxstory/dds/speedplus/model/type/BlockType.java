package com.uxstory.dds.speedplus.model.type;

import com.uxstory.dds.speedplus.R;

public enum BlockType {

    BLOCK_1("DMAXzir-55", R.color.colorChart1),
    BLOCK_2("VITA Mark II-I8", R.color.colorChart2),
    BLOCK_3("VITA Mark II-I12", R.color.colorChart3),
    BLOCK_4("VITA Mark II-I14", R.color.colorChart4),
    BLOCK_5("MazicDuro-12", R.color.colorChart5),
    BLOCK_6("MazicDuro-14L", R.color.colorChart6),
    BLOCK_7("EmpressCAD-I8", R.color.colorChart7),
    BLOCK_8("EmpressCAD-C14", R.color.colorChart8),
    BLOCK_9("EmpressCAD-C32", R.color.colorChart9),
    BLOCK_10("e.maxCAD-I12", R.color.colorChart10),
    BLOCK_11("e.maxCAD-C14", R.color.colorChart11),
    BLOCK_12("e.maxCAD-C32", R.color.colorChart12),
    ;

    private String name;
    private int colorResId;

    BlockType(String name, int colorResId) {
        this.name = name;
        this.colorResId = colorResId;
    }

    public String getName() {
        return name;
    }

    public int getColorResId() {
        return colorResId;
    }

    public static int getColorResId(String blockName) {
        for (BlockType type : BlockType.values()) {
            if (blockName.toUpperCase().equals(type.getName().toUpperCase())) {
                return type.getColorResId();
            }
        }
        return 0;
    }
}
