package com.uxstory.dds.speedplus.activity.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.fragment.AbstractBaseFragment;
import com.uxstory.dds.speedplus.activity.fragment.MonitoringFragment;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.MillingProgress;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.model.type.RestorationType;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceRecyclerAdapter extends RecyclerView.Adapter<DeviceRecyclerAdapter.ViewHolder> {

    final static String TAG = DeviceRecyclerAdapter.class.getSimpleName();

    private Context context;
    private List<Device> items;
    AbstractBaseFragment baseFragment;

    public DeviceRecyclerAdapter(Context context, List<Device> items) {
        this.context = context;
        this.items = items;
    }

    public void setList(List<Device> items) {
        this.items = items;
    }

    public AbstractBaseFragment getBaseFragment() {
        return baseFragment;
    }

    public void setBaseFragment(AbstractBaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device, parent, false);
        v.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DeviceRecyclerAdapter.ViewHolder holder, int position) {
        LogU.d(TAG, "onBindViewHolder() position=" + position);
        final Device device = items.get(position);
        holder.textDeviceTitle.setText(device.getName());

        LogU.d(TAG, "device.isConnected()="+device.isConnected());
        LogU.d(TAG, "device.getLastEvent()="+device.getLastEvent());

        holder.iconNetwork.setEnabled(device.isConnected());

        WorkStateType workStateType = device.getWorkStateType();
        if(workStateType == WorkStateType.MILLING_STOP) {
            holder.imgWorkErr.setVisibility(View.VISIBLE);
            holder.txtState.setTextColor(context.getColor(R.color.colorRed));
        } else {
            holder.imgWorkErr.setVisibility(View.GONE);
            holder.txtState.setTextColor(context.getColor(R.color.colorDark));
        }
        holder.txtState.setText(workStateType.getNameResId());

//        if(device.isBurWarning()) {
//            holder.iconStateError1.setVisibility(View.VISIBLE);
//        } else {
//            holder.iconStateError1.setVisibility(View.GONE);
//        }
//        if(device.isWaterWarning()) {
//            holder.iconStateError2.setVisibility(View.VISIBLE);
//        } else {
//            holder.iconStateError2.setVisibility(View.GONE);
//        }

        if(device.isBurWarning() && device.isWaterWarning()) {
            holder.iconStateError1.setImageResource(R.drawable.icon_state_bur_e);
            holder.iconStateError2.setImageResource(R.drawable.icon_state_water_e);

        } else if(device.isBurWarning()) {
            holder.iconStateError1.setImageDrawable(null);
            holder.iconStateError2.setImageResource(R.drawable.icon_state_bur_e);

        } else if(device.isWaterWarning()) {
            holder.iconStateError1.setImageDrawable(null);
            holder.iconStateError2.setImageResource(R.drawable.icon_state_water_e);

        } else {
            holder.iconStateError1.setImageDrawable(null);
            holder.iconStateError2.setImageDrawable(null);
        }

        holder.txtPatientName.setText(StringUtil.nullToHyphen(device.getPatientName()));
        holder.txtRestoration.setText(RestorationType.getRestorationString(context, device.getRestoration()));
        RestorationType restorationType = device.getRestorationType();
        holder.iconRestoration.setImageResource(restorationType.getIconNormalResId());
        holder.txtBlock.setText(StringUtil.nullToHyphen(device.getBlock()));

        MillingProgress millingProgress = device.getMillingProgress();
        if(millingProgress != null) {
            holder.progressBar.setProgress(millingProgress.getLocalPercent(device.getWorkStateType()));
            holder.txtProgress.setText(millingProgress.getLocalPercent(device.getWorkStateType())+"0");
        } else {
            holder.progressBar.setProgress(0);
            holder.txtProgress.setText("00");
        }

        setClosedCardView(device, holder);

//        holder.btnClose.setOnClickListener(view -> {
//            closeCardView(device, holder);
//        });
        holder.layoutTopBar.setOnClickListener(view -> {
            closeCardView(device, holder);
        });

        holder.btnDetail.setOnClickListener(view -> {
            goToMonitoring(device);
        });
        holder.layoutDevice.setOnClickListener(view -> {
            goToMonitoring(device);
        });

        int millingFinishCount = device.getMillingFinishCount();
        if(device.getWorkStateType().ordinal() >= WorkStateType.READY.ordinal() && device.getWorkStateType().ordinal() < WorkStateType.MILLING_FINISH.ordinal()) {
            millingFinishCount++;
        }
        holder.txtMilling.setText(String.format(context.getString(R.string.txt_milling), String.valueOf(millingFinishCount)));

        ViewUtil.requestLayout((ViewGroup)holder.itemView);
    }

    private void closeCardView(Device device, ViewHolder holder) {
        device.setClosedCardView(!device.isClosedCardView());
        setClosedCardView(device, holder);
    }

    private void goToMonitoring(Device device) {
        MonitoringFragment monitoringFragment = new MonitoringFragment();
        monitoringFragment.setDevice(device);
        monitoringFragment.setLayoutId(R.layout.fragment_monitoring);
        baseFragment.addFragment((Activity) context, monitoringFragment);
    }

    private void setClosedCardView(Device device, ViewHolder holder) {
        if(device.isClosedCardView()) {
            holder.layoutTopBar.setSelected(false);
            holder.viewNoData.setVisibility(View.GONE);
            holder.layoutDevice.setVisibility(View.GONE);
        } else {
            holder.layoutTopBar.setSelected(true);
            setCardView(device, holder);
        }
    }

    private void setCardView(Device device, ViewHolder holder) {
        if(device.getLastEvent() == null) {
            holder.viewNoData.setVisibility(View.VISIBLE);
            holder.layoutDevice.setVisibility(View.GONE);
        } else {
            holder.viewNoData.setVisibility(View.GONE);
            holder.layoutDevice.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.view_no_data) View viewNoData;
        @BindView(R.id.layout_device) View layoutDevice;
        @BindView(R.id.layout_top_bar) View layoutTopBar;

        @BindView(R.id.txt_device_title) TextView textDeviceTitle;

        @BindView(R.id.img_work_err) ImageView imgWorkErr;
        @BindView(R.id.progress_bar) ProgressBar progressBar;
        @BindView(R.id.txt_progress) TextView txtProgress;
        @BindView(R.id.txt_milling) TextView txtMilling;
        @BindView(R.id.txt_status) TextView txtState;
        @BindView(R.id.txt_patient_name) TextView txtPatientName;
        @BindView(R.id.txt_restoration) TextView txtRestoration;
        @BindView(R.id.txt_block) TextView txtBlock;


        @BindView(R.id.icon_type_02_e) ImageView iconRestoration;
        @BindView(R.id.icon_block_02) ImageView iconBlock;

        @BindView(R.id.icon_link_n) ImageView iconNetwork;
        @BindView(R.id.btn_accordion_01_close) ImageView btnClose;
        @BindView(R.id.btn_detail_01_n) ImageView btnDetail;

        @BindView(R.id.icon_state_error_1) ImageView iconStateError1;
        @BindView(R.id.icon_state_error_2) ImageView iconStateError2;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
