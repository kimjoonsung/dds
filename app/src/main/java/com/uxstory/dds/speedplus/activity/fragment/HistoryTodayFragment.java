package com.uxstory.dds.speedplus.activity.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.adpater.TodayHistoryRecycleAdapter;
import com.uxstory.dds.speedplus.activity.ui.SwipeController;
import com.uxstory.dds.speedplus.activity.ui.SwipeControllerActions;
import com.uxstory.dds.speedplus.activity.ui.ViewUtil;
import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HistoryTodayFragment extends AbstractBaseFragment {

    private final static String TAG = HistoryFragment.class.getSimpleName();

    @BindView(R.id.recycler_history)
    RecyclerView recyclerView;

    @BindView(R.id.txt_today)
    TextView txtToday;

    @BindView(R.id.view_no_milling_history)
    View noData;

    private List<Event> eventList = new ArrayList<>();
    private TodayHistoryRecycleAdapter recyclerAdapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setLayoutId(R.layout.fragment_history_today);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        updateView();
    }

//    @Override
//    protected void addBackStackChangedListener() {
////        super.addBackStackChangedListener();
//    }
//
//    @Override
//    protected void registerRxBus() {
////        super.registerRxBus();
//    }

//    @Override
//    protected void setOnKeyBackPressedListener() {
////        super.setOnKeyBackPressedListener();
//    }


    private void confirmDeleteHistory(int position) {
        AlertDialog.Builder builder = ViewUtil.getDialogBuilder(getContext(), getDevice().getName(), R.string.txt_msg_delete_history, R.drawable.icon_noti_00);
        builder.setPositiveButton(R.string.txt_yes, (dialog, i) -> {
            DbQuery.deleteWorkEndHistory(eventList.get(position).getUri());
            eventList.remove(position);
            recyclerAdapter.notifyItemRemoved(position);
//            recyclerAdapter.notifyItemRangeChanged(position, 1);
        });
        builder.setNegativeButton(R.string.txt_no, (dialog, i) -> dialog.dismiss());
        ViewUtil.showDialog(getContext(), builder);
    }

    private void updateView() {
        if(getDevice() != null) {
            String date = StringUtil.getCurrentDateDisplayFormat();

            eventList = DbQuery.selectWorkEndHistoryList(getDevice().getSerial(), date);
            if (getDevice().getWorkStateType().isMillingState()) {
                Event event = new Event();
                event.setMillingStartTime(getDevice().getMillingStartTime());
                event.setPatientName(getDevice().getPatientName());
                event.setRestoration(getDevice().getRestoration());
                event.setBlock(getDevice().getBlock());
                event.setType(getDevice().getWorkStateType().getString());
                eventList.add(0, event);
            }

            recyclerAdapter = new TodayHistoryRecycleAdapter(getActivity(), eventList);
            recyclerView.setAdapter(recyclerAdapter);

            SwipeController swipeController = new SwipeController(getContext().getDrawable(R.drawable.btn_del), new SwipeControllerActions() {
                @Override
                public void onRightClicked(int position) {
                    confirmDeleteHistory(position);
                }
            });

            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
            itemTouchhelper.attachToRecyclerView(recyclerView);

            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                    swipeController.onDraw(c);
                }
            });

            txtToday.setText(date);
            ViewUtil.setVisibility(noData, eventList.size() <= 0);
//        ViewUtil.setVisibility(recyclerView, eventList.size() > 0);
        }
    }

    @Override
    protected void onUpdateView() {

    }
}
