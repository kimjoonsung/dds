package com.uxstory.dds.speedplus.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper {

    private static final String DATABASE_NAME = "DDS_SPEED_PLUS2.db";
    private static final int DATABASE_VERSION = 17;
    public static SQLiteDatabase DB;
    private Context ctx;

    private class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Databases.CREATE_TABLE_EVENT_HISTORY);
            db.execSQL(Databases.CREATE_TABLE_WATER_CHANGE);
            db.execSQL(Databases.CREATE_TABLE_DEVICE);
            db.execSQL(Databases.CREATE_TABLE_APP_NOTIFICATION);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //TODO 디비 구조 변경으로 DATABASE_VERSION 변경 시 oldVersion에 따라 테이블 및 컬럼 추가/삭제/변경 등 필요.
//            switch (oldVersion) {
//                case 0:
//                    break;
//            }

            String dropTable = "DROP TABLE IF EXISTS ";
            db.execSQL(dropTable + Databases.TABLE_EVENT_HISTORY);
            db.execSQL(dropTable + Databases.TABLE_WATER_CHANGE_HISTORY);
            db.execSQL(dropTable + Databases.TABLE_DEVICE);
            db.execSQL(dropTable + Databases.TABLE_NOTIFICATION);
            onCreate(db);

//            db.execSQL(dropTable + Databases.TABLE_NOTIFICATION);
//            db.execSQL(Databases.CREATE_TABLE_APP_NOTIFICATION);
        }
    }

    public DbOpenHelper(Context context) {
        this.ctx = context;
    }

    public boolean open() {
        DatabaseHelper dbHelper = new DatabaseHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION);
        DB = dbHelper.getWritableDatabase();
        return DbOpenHelper.DB != null && DbOpenHelper.DB.isOpen();
    }

//    public void create(){
//        dbHelper.onCreate(DB);
//    }

    public static void close() {
        if (DB != null) {
            DB.close();
            DB = null;
        }
    }
}
