package com.uxstory.dds.speedplus.database;

final class Databases {

    static final String TABLE_EVENT_HISTORY = "event_history";
    static final String CR_URI = "uri";
    static final String CR_SERIAL = "serial";
    static final String CR_BOOT_INDEX = "boot_index";
    static final String CR_WORK_STEP = "work_step";
    static final String CR_TYPE = "type";
    static final String CR_DELETED = "deleted";
    static final String CR_PATIENT_NAME = "patient_name";
    static final String CR_BLOCK = "block";
    static final String CR_RESTORATION = "restoration";
    static final String CR_LEFT_BUR_NAME = "left_bur_name";
    static final String CR_RIGHT_BUR_NAME = "right_bur_name";
    static final String CR_ELAPSED_TIME = "elapsed_time";
    static final String CR_MILLING_START_TIME = "milling_start_time";
    static final String CR_HISTORY = "history";
    static final String CR_EVENT_TIME = "event_time";
    static final String CR_RECEIVE_TIME = "receive_time";

    static final String CREATE_TABLE_EVENT_HISTORY = "create table if not exists " + TABLE_EVENT_HISTORY + "("
            + CR_URI + " text not null primary key, "
            + CR_SERIAL + " text not null, "
            + CR_BOOT_INDEX + " integer not null, "
            + CR_WORK_STEP + " integer not null, "
            + CR_TYPE + " text not null, "
            + CR_DELETED + " text, "
            + CR_PATIENT_NAME + " text, "
            + CR_BLOCK + " text, "
            + CR_RESTORATION + " text, "
            + CR_LEFT_BUR_NAME + " text, "
            + CR_RIGHT_BUR_NAME + " text, "
            + CR_ELAPSED_TIME + " text, "
            + CR_MILLING_START_TIME + " text, "
            + CR_HISTORY + " text, "
            + CR_EVENT_TIME + " text not null, "
            + CR_RECEIVE_TIME + " text not null DEFAULT (datetime('now', 'localtime'))); "
            + "create index event_history_serial_type_time_index on " + TABLE_EVENT_HISTORY +" ("+ CR_SERIAL +","+ CR_TYPE +","+ CR_EVENT_TIME +"));";


    static final String TABLE_WATER_CHANGE_HISTORY = "water_change_history";
    static final String CR_CHANGE_TIME = "change_time";

    static final String CREATE_TABLE_WATER_CHANGE = "create table if not exists " + TABLE_WATER_CHANGE_HISTORY + " ("
            + CR_SERIAL + " text not null, "
            + CR_CHANGE_TIME + " text not null DEFAULT (datetime('now', 'localtime')));";

    static final String TABLE_DEVICE = "device";
    static final String CR_NAME = "name";
    static final String CR_IP = "ip";
    static final String CR_PORT = "port";
    static final String CR_SESSION = "session";
    static final String CR_ENCRYPT_KEY = "encrypt_key";
    static final String CR_SYNC_TIME = "sync_time";
    static final String CR_REG_TIME = "reg_time";

    static final String CREATE_TABLE_DEVICE = "create table if not exists " + TABLE_DEVICE + " ("
            + CR_SERIAL + " text not null primary key, "
            + CR_NAME + " text not null, "
            + CR_IP + " text not null, "
            + CR_PORT + " text not null, "
            + CR_SESSION + " text, "
            + CR_ENCRYPT_KEY + " text, "
            + CR_SYNC_TIME + " text, "
            + CR_REG_TIME + " text not null DEFAULT (datetime('now', 'localtime')));";


    static final String TABLE_NOTIFICATION = "notification";
    static final String CR_NUM = "num";
    static final String CR_CHECKED = "checked";
    static final String CR_NOTI_TYPE = "noti_type";
    static final String CR_NOTI_KIND = "noti_kind";

    static final String CREATE_TABLE_APP_NOTIFICATION = "create table if not exists " + TABLE_NOTIFICATION + " ("
            + CR_NUM + " integer not null primary key autoincrement, "
            + CR_SERIAL + " text not null, "
            + CR_NOTI_TYPE + " text not null, "
            + CR_NOTI_KIND + " text not null, "
            + CR_CHECKED + " text, "
            + CR_REG_TIME + " text not null DEFAULT (datetime('now', 'localtime')));"
            + "create index notification_serial_time_index on " + TABLE_NOTIFICATION +  " ("+ CR_SERIAL +","+ CR_REG_TIME +"));";
}
