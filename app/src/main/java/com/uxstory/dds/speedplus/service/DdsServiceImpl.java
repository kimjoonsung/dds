package com.uxstory.dds.speedplus.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.R;
import com.uxstory.dds.speedplus.activity.MainActivity;
import com.uxstory.dds.speedplus.communicator.Communicator;
import com.uxstory.dds.speedplus.communicator.CommunicatorEvent;
import com.uxstory.dds.speedplus.communicator.CommunicatorEventListener;
import com.uxstory.dds.speedplus.communicator.CommunicatorImpl;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.config.Preference;
import com.uxstory.dds.speedplus.database.DbQuery;
import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.EventChecker;
import com.uxstory.dds.speedplus.message.MessageProcessor;
import com.uxstory.dds.speedplus.message.type.EventHistorySyncType;
import com.uxstory.dds.speedplus.message.type.EventType;
import com.uxstory.dds.speedplus.message.type.RequestType;
import com.uxstory.dds.speedplus.message.type.ResponseType;
import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.notification.NotificationPublisher;
import com.uxstory.dds.speedplus.notification.type.AppNotificationType;
import com.uxstory.dds.speedplus.util.DateUtil;
import com.uxstory.dds.speedplus.util.LogU;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pe.warrenth.rxbus2.RxBus;

public class DdsServiceImpl extends Service implements DdsService, CommunicatorEventListener {

    private final static String TAG = DdsServiceImpl.class.getSimpleName();

    private final IBinder binder = new ServiceBinder();

    HashMap<String, Device> deviceMap = new HashMap<>();
    ArrayList<Device> devicesList = null;

    private static boolean isRunning;

    public class ServiceBinder extends Binder {
        public DdsServiceImpl getService() {
            return DdsServiceImpl.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

//            Device device = new Device();
//            device.setName("Speed+ 1");
//            device.setSerial("1234-5678");
//            device.setIp("192.168.100.3");
//            device.setSession("asjfdlasdflkjlasdflkalfsdj");
//            device.setEncryptKey("asjfdlasdflkjlasdflkalfsdj");
//            device.setPort(3000);
//            DbQuery.updateDevice(device);

        isRunning = true;
        startForegroundService();

        loadDeviceList();

        new Handler().postDelayed(this::checkLongTimeNoSee, 60 * 1000);
    }

    public static boolean isRunning() {
        return isRunning;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogU.d(TAG, "onStartCommand()");
//        startForegroundService();

        if (BuildConfig.DEBUG) {
            return START_NOT_STICKY;
//            return START_STICKY;
//            return START_REDELIVER_INTENT;
        } else {
            return START_STICKY;
//            return START_REDELIVER_INTENT;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        DeviceNetworkChecker.getInstance(this).stop();
//        EventBusProvider.getInstance().unregister(this);
    }

    @Override
    public void onCommunicatorEvent(final CommunicatorEvent communicatorEvent) {
        LogU.d(TAG, "onCommunicatorEvent() getType=" + communicatorEvent.getResponseType());

        Device device = communicatorEvent.getDevice();
        WorkStateType preWorkStateType = device.getWorkStateType();

        switch (communicatorEvent.getResponseType()) {
            case CONNECTED:
                switch (preWorkStateType) {
                    case RETRY_CONNECTING:
                        break;
                    case PAIRING:
                        addDevice(device);
                        break;
                }
                device.setWorkStateType(WorkStateType.CONNECTED);
                device.clearRetryConnectingCount();
                requestHistory(device);
                break;
            case SESSION:
                switch (preWorkStateType) {
                    case RETRY_CONNECTING:
                        NotificationPublisher.showNotification(this, device, AppNotificationType.CONFIRM_PAIRING);
                        break;
                    case PAIRING:
                        //activity에서 이벤트 받아 처리.
                        break;
                }
                device.clearRetryConnectingCount();
                break;
            case SYNC_EVENT_HISTORY:
                MessageProcessor.setWorkStateFromHistory(device);
                device.setLastEventHistorySyncDate(StringUtil.getCurrentDateTime());
                device.setEventHistorySyncType(EventHistorySyncType.COMPLETE);
                setMillingFinishCount(device, device.getLastEvent());
                DbQuery.updateDevice(device, false);
                requestMillingProgress(device);
                checkBurState(device, false);
                checkWaterTurbidState(device, false);
                break;
            case SYNC_EVENT_HISTORY_ERROR:
                device.setEventHistorySyncType(EventHistorySyncType.FAILED);
                break;
            case UNPAIRING:
                device.getCommunicator().disconnect();
                removeDevice(device);
                break;
            case UNPAIRING_ERROR:
                break;
            case DISCONNECTED:
                if (device.getWorkStateType() != WorkStateType.RETRY_CONNECTING) {
                    if (device.getSession() != null && preWorkStateType.ordinal() >= WorkStateType.CONNECTED.ordinal()) {
                        NotificationPublisher.showNotification(this, device, AppNotificationType.DEVICE_DISCONNECTED);
                    }
                    Event lastFinishEvent = DbQuery.selectLastMillingFinishEvent(device.getSerial());
                    if(lastFinishEvent != null) {
                        device.clearNotiCountMonitoring();
                    }
                    device.setLastMillingFinishInfo(lastFinishEvent);
                    setMillingFinishCount(device, lastFinishEvent);
                }
                device.setWorkStateType(WorkStateType.DISCONNECTED);
                device.setEventHistorySyncType(EventHistorySyncType.NULL);
                device.setCommunicator(null);
                break;
            case EVENT:
                Event event = communicatorEvent.getEvent();
                if (event.getEventType() != EventType.NULL) {
                    device.setLastEvent(event);
                    device.setWorkStateType(WorkStateType.getType(event.getType()));
                    NotificationPublisher.showNotification(this, device, event);
                }
                switch (event.getEventType()) {
                    case INSERT_BUR_AND_BLOCK:
                        checkBurState(device, true);
                        checkWaterTurbidState(device, true);
                        break;
                    case MILLING_FINISH:
                        setMillingFinishCount(device, event);
                        break;
                }
                break;
        }

        if (preWorkStateType.ordinal() >= WorkStateType.PAIRING.ordinal()) {
            RxBus.get().send(Constants.RX_BUS_EVENT_DEVICE, communicatorEvent);
        }
    }

    private void setMillingFinishCount(Device device, Event event) {
        if (device != null && event != null) {
            int count = DbQuery.selectMillingFinishCount(device.getSerial(), StringUtil.convertDateTimeToDate(event.getTime()));
            device.setMillingFinishCount(count);
        }
    }

    private void checkLongTimeNoSee() {
        if(Preference.getPreferenceValue(Preference.PreferenceType.NOTI_LONG_NO_USE)) {

            boolean isShowNoti = false;
            long currentTime = System.currentTimeMillis();

            SharedPreferences pref = getSharedPreferences(Preference.PREFER_NAME, Context.MODE_PRIVATE);
            long preUseTime = pref.getLong(Preference.PREFER_USE_APP_TIME, 0L);
            if (preUseTime > 0) {
                long term =  (currentTime - preUseTime) / 1000;
                if (term >= 30 * 24 * 60 * 60) {
                    NotificationPublisher.showNotification(this, null, AppNotificationType.LONG_TIME_NO_USE_APP);
                    isShowNoti = true;
                }
            }

            String lastEventTime = DbQuery.selectLastEventTime();
            if(!isShowNoti && lastEventTime != null) {
                try {
                    long term = DateUtil.getDiff(DateUtil.getDateFromString(lastEventTime)
                            , DateUtil.getDateFromString(StringUtil.convertLongToDateTime(currentTime))
                            , DateUtil.TIME_UNIT.DAYS);

                    if(term >= 60) {
                        NotificationPublisher.showNotification(this, null, AppNotificationType.LONG_TIME_NO_USE_DEVICE_1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        new Handler().postDelayed(this::checkLongTimeNoSee, 24 * 60 * 60 * 1000);
    }

    @Override
    public void checkBurState(Device device, boolean noti) {
        if (device == null) {
            return;
        }

        List<Event> list = DbQuery.selectLastChangeBurDate(device.getSerial());

        String leftBurChangeDate = "";
        String rightBurChangeDate = "";
        String bothBurChageDate = "";

        for (Event event : list) {
            switch (event.getEventType()) {
                case CHANGE_LEFT_BUR:
                    leftBurChangeDate = event.getTime();
                    break;
                case CHANGE_RIGHT_BUR:
                    rightBurChangeDate = event.getTime();
                    break;
                case CHANGE_BOTH_BUR:
                    bothBurChageDate = event.getTime();
                    break;
            }
        }

        long leftChangedTime = StringUtil.convertDateToTimeMillis(leftBurChangeDate);
        long rightChangedTime = StringUtil.convertDateToTimeMillis(rightBurChangeDate);
        long bothChangedTime = StringUtil.convertDateToTimeMillis(bothBurChageDate);

        leftBurChangeDate = (leftChangedTime > bothChangedTime) ? leftBurChangeDate : bothBurChageDate;
        rightBurChangeDate = (rightChangedTime > bothChangedTime) ? rightBurChangeDate : bothBurChageDate;

        int leftCount = DbQuery.selectMillingFinishCountFromDateTime(device.getSerial(), leftBurChangeDate);
        device.setLeftBurUsingCount(leftCount);
        if (leftCount > Constants.BUR_WARNING_COUNT && noti) {
            NotificationPublisher.showNotification(this, device, AppNotificationType.BUR_LEFT_WARNING);
        }

        int rightCount = DbQuery.selectMillingFinishCountFromDateTime(device.getSerial(), rightBurChangeDate);
        device.setRightBurUsingCount(rightCount);
        if (rightCount > Constants.BUR_WARNING_COUNT && noti) {
            NotificationPublisher.showNotification(this, device, AppNotificationType.BUR_RIGHT_WARNING);
        }
    }

    @Override
    public void checkWaterTurbidState(Device device, boolean noti) {
        if (device == null) {
            return;
        }

        String date = DbQuery.selectLastWaterChangeDate(device.getSerial());
        date = (date == null) ? "" : date;
        long time = DbQuery.selectWorkingTimeAfterWaterChange(device.getSerial(), date);
        device.setWaterWorkingTime(time);
        long bad = Constants.WATER_WARNING_TIME * 3600;
        if (time > bad && noti) {
            NotificationPublisher.showNotification(this, device, AppNotificationType.WATER_TURBID);
        }
    }

    private void checkDeviceNetwork() {
        DeviceNetworkChecker.getInstance(this).start();
    }

    public void startForegroundService() {
        LogU.d(TAG, "startForegroundService()");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.DDS_NOTIFICATION_CHANNEL_SERVICE_ID);
        builder.setSmallIcon(R.drawable.icon_info_menu_01);
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.bigText(Constants.DDS_NOTIFICATION_CHANNEL_SERVICE_NAME);
        style.setBigContentTitle(null);
        style.setSummaryText("Service is running");
        builder.setStyle(style);
//        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setContentTitle(null);
        builder.setContentText("");
        builder.setOngoing(true);
        builder.setWhen(0);
        builder.setShowWhen(false);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        builder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (manager != null) {
            String serviceName = getApplicationContext().getString(R.string.app_name) + " Network Communication Service";
            manager.createNotificationChannel(new NotificationChannel(Constants.DDS_NOTIFICATION_CHANNEL_SERVICE_ID, serviceName, NotificationManager.IMPORTANCE_HIGH));
        }
        Notification notification = builder.build();
        startForeground(Constants.DDS_NOTIFICATION_ID_SERVICE, notification);
    }

    @Override
    public synchronized void loadDeviceList() {
        devicesList = DbQuery.selectDeviceList(true);
        for (Device device : devicesList) {
            Event event = DbQuery.selectLastMillingFinishEvent(device.getSerial());
            device.setLastMillingFinishInfo(event);
            setMillingFinishCount(device, event);
            device.setWorkStateType(WorkStateType.CONNECTING);
            startCommunicator(device);
            deviceMap.put(device.getSerial(), device);
        }

        checkDeviceNetwork();
    }

    @Override
    public synchronized void startCommunicator(Device device) {
        CommunicatorImpl communicator = device.getCommunicator();
        if (communicator != null) {
            communicator.disconnect();
            device.setCommunicator(null);
        }
        communicator = new CommunicatorImpl(device, this);
        communicator.start();
        device.setCommunicator(communicator);
    }

    @Override
    public void requestPairing(Device device) {
        device.setWorkStateType(WorkStateType.PAIRING);
        startCommunicator(device);
    }

    @Override
    public synchronized void addDevice(Device device) {
        if (device != null && deviceMap.get(device.getSerial()) == null) {
//            Device d = deviceMap.get(device.getSerial());
//            if(d != null && d.getCommunicator() != null) {
//                d.getCommunicator().setCommunicatorEventListener(null);
//                deviceMap.remove(device.getSerial());
//                devicesList.remove(d);
//            }
            device.getCommunicator().setCommunicatorEventListener(this);
            deviceMap.put(device.getSerial(), device);
            devicesList.add(0, device);
            onCommunicatorEvent(new CommunicatorEvent(ResponseType.CHANGED_DEVICE_LIST, device, null));

            LogU.d(TAG, ">> addDevice() : " + device.getSerial() + ", " + device + ", communicator=" + device.getCommunicator());
        }
    }

    @Override
    public void updateDeviceName(String serial, String name) {
        for (Device device : devicesList) {
            if (device.getSerial().equals(serial)) {
                device.setName(name);
                return;
            }
        }
    }

    @Override
    public synchronized void removeDevice(Device device) {
        if (device != null) {
//            Communicator communicator = device.getCommunicator();
//            if (communicator != null) {
//                communicator.disconnect();
//            }
            Device orgDevice = deviceMap.get(device.getSerial());
            if (orgDevice != null) {
                deviceMap.remove(orgDevice.getSerial());
                devicesList.remove(orgDevice);
            }
            onCommunicatorEvent(new CommunicatorEvent(ResponseType.CHANGED_DEVICE_LIST, device, null));
        }
    }

    @Override
    public ArrayList<Device> getDeviceList() {
        return devicesList;
    }

    @Override
    public Device getPairedDevice(String serial) {
        for (Device device : getDeviceList()) {
            if (device.getSerial().equals(serial)) {
                return device;
            }
        }
        return null;
    }

    @Override
    public boolean requestUnPairing(Device device) {
        Communicator communicator = device.getCommunicator();
        if (device.isConnected() && communicator != null) {
            communicator.request(RequestType.UNPAIRING);
            return true;
        }
        return false;
    }

    @Override
    public boolean requestWaterSensing(Device device) {
        Communicator communicator = device.getCommunicator();
        if (device.isConnected() && communicator != null && EventChecker.isPossibleRequestWaterSensingEvent(device.getLastEvent())) {
            communicator.request(RequestType.WATER_SENSING);
            return true;
        }
        return false;
    }

    @Override
    public boolean requestMillingProgress(Device device) {
        Communicator communicator = device.getCommunicator();
        if (device.isConnected() && communicator != null && EventChecker.isPossibleRequestMillingProgressEvent(device.getLastEvent())) {
            communicator.request(RequestType.MILLING_PROGRESS);
            return true;
        }
        return false;
    }

    @Override
    public boolean requestHistory(Device device) {
        Communicator communicator = device.getCommunicator();
        if (device.isConnected() && communicator != null) {
            device.setEventHistorySyncType(EventHistorySyncType.REQUEST);
            communicator.request(RequestType.EVENT_HISTORY);
            return true;
        }
        return false;
    }

    @Override
    public boolean requestBurInfo(Device device) {
        Communicator communicator = device.getCommunicator();
        if (device.isConnected() && communicator != null && EventChecker.isPossibleRequestBurInfoEvent(device.getLastEvent())) {
            communicator.request(RequestType.BUR_INFO);
            return true;
        }
        return false;
    }


}
