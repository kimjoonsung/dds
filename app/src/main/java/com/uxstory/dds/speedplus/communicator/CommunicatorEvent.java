package com.uxstory.dds.speedplus.communicator;

import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.device.Event;
import com.uxstory.dds.speedplus.message.type.ResponseType;

public class CommunicatorEvent {

    private ResponseType responseType;
    private Device device;
    private Event event;

    public CommunicatorEvent(ResponseType responseType) {
        this.responseType = responseType;
    }

    public CommunicatorEvent(ResponseType responseType, Device device, Event event) {
        this.responseType = responseType;
        this.device = device;
        this.event = event;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
