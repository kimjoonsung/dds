package com.uxstory.dds.speedplus.device;

import android.util.Log;

import com.uxstory.dds.speedplus.message.type.WorkStateType;
import com.uxstory.dds.speedplus.util.StringUtil;

public class MillingProgress {

    private int percent;
    private int remainingTime;
    private long receivedTime;

    private int lastPercent;
    private String lastRemainingTimeStr;

    private long pauseTime;

    public int getLocalPercent(WorkStateType workStateType) {
        switch (workStateType) {
            case MILLING_START:
            case MILLING_RESTART:
            case MILLING_CONTINUE:
                int duration = (int) ((System.currentTimeMillis() - receivedTime) / 1000);
                int remain = remainingTime;
                if (remain > 0) {
                    int progress = percent + (duration * (100 - percent) / remain);
                    lastPercent = (progress > 100) ? 100 : progress;
                } else {
                    lastPercent = 0;
                }
                break;
        }
        return lastPercent;
    }

    public void setPercent(int percent) {
        Log.d("percent", "setPercent = " + percent);
        this.percent = percent;
        this.lastPercent = percent;
    }

    /**
     * @param workStateType
     * @return mm:ss
     */
    public String getLocalRemainingTime(WorkStateType workStateType) {
        switch (workStateType) {
            case MILLING_START:
            case MILLING_RESTART:
            case MILLING_CONTINUE:
                int duration = (int) ((System.currentTimeMillis() - receivedTime) / 1000);
                int remain = remainingTime - duration;
                if (remain > 0) {
                    lastRemainingTimeStr = StringUtil.convertSecToMinSec(remain);
                } else {
                    lastRemainingTimeStr = "00:00";
                }
                break;
        }
        return lastRemainingTimeStr;
    }

    public void setRemainingTime(String remainingTime) {
        this.remainingTime = StringUtil.convertTimeSec(remainingTime);
        this.lastRemainingTimeStr = StringUtil.convertSecToMinSec(this.remainingTime);
        this.receivedTime = System.currentTimeMillis();
    }

    /**
     * TODO Just test
     */
    public void setPauseTime() {
        this.pauseTime = System.currentTimeMillis();
    }

    /**
     * TODO Just test
     */
    public void setContinueTime() {
        this.receivedTime += (System.currentTimeMillis() - this.pauseTime);
    }

}
