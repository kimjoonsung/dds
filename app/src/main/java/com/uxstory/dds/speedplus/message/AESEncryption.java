package com.uxstory.dds.speedplus.message;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESEncryption {
	private static final int keySize = 16;
	
	private byte[] key;
	private byte[] iv;
	
	private final String textIv = "67dfc4386ebea6aed40fwf6dcede0915";
	
	private Cipher encryptCipher; //Little-endian
	private Cipher decryptCipher;
	private Encoder encoder = Base64.getEncoder();
	private Decoder decoder = Base64.getDecoder();
	
	public AESEncryption(String textKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		key = hex2byte(textKey.getBytes());
		iv = hex2byte(textIv.getBytes());
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		encryptCipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
		decryptCipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		decryptCipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
	}
	//Little-endian
	public String encrypt(String plainText) {
		try {
			byte[] plainByte = plainText.getBytes();
			byte[] cipherByte = encryptCipher.doFinal(plainByte);
			return encoder.encodeToString(cipherByte);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String decrypt(String chipherText) {
		try {
			byte[] chipherByte = decoder.decode(chipherText);
			byte[] plainByte = decryptCipher.doFinal(chipherByte);
			return new String(plainByte);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static byte[] hex2byte(byte[] in) {
		byte[] result = new byte[keySize];
		Arrays.fill(result, (byte)0x00);
		for(int i = 0; i < in.length; i += 2) {
			byte c0 = in[i];
			byte c1 = in[i + 1];
			byte c = (byte)(
					(((c0 & 0x40) != 0 ? ((c0 & 0x20) != 0 ? c0-0x57 : c0-0x37) : c0-0x30) << 4) |
					(((c1 & 0x40) != 0 ? ((c1 & 0x20) != 0 ? c1-0x57 : c1-0x37) : c1-0x30))
					);
			if((i / 2) < 16) {
				result[i / 2] = c;
			}else {}
		}
		return result;
	}
}
