package com.uxstory.dds.speedplus.activity.ui;

import com.uxstory.dds.speedplus.device.Device;
import com.uxstory.dds.speedplus.util.StringUtil;

import java.util.Comparator;

public class ComparatorDeviceLastEventTime implements Comparator<Device> {

    @Override
    public int compare(Device o1, Device o2) {
        long time1 = (o1.getLastEvent() != null) ? StringUtil.convertDateToTimeMillis(o1.getLastEvent().getTime()) : 0L;
        long time2 = (o2.getLastEvent() != null) ? StringUtil.convertDateToTimeMillis(o2.getLastEvent().getTime()) : 0L;
        return Long.compare(time2, time1);
    }
}
