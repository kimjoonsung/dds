package com.uxstory.dds.speedplus.message.type;

public enum EventHistorySyncType {
    NULL,
    READY,
    FAILED,
    REQUEST,
    COMPLETE,
    ;
}
