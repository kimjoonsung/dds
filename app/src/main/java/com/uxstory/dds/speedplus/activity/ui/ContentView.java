package com.uxstory.dds.speedplus.activity.ui;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.View;

import com.uxstory.dds.speedplus.application.GlobalApplication;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.util.LogU;

import pe.warrenth.rxbus2.RxBus;

public class ContentView extends View implements View.OnSystemUiVisibilityChangeListener {

    private final static String TAG = ContentView.class.getSimpleName();

    Runnable mNavHider = this::hideNavi;

    public ContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnSystemUiVisibilityChangeListener(this);
//        setNavVisibility(false);
    }

    @Override
    public void onSystemUiVisibilityChange(int visibility) {
        LogU.d(TAG, "onSystemUiVisibilityChange() visibility = " + visibility);
        LogU.d(TAG, "onSystemUiVisibilityChange() getDisplay().getRotation() = " + getDisplay().getRotation());
        if (visibility == View.SYSTEM_UI_FLAG_VISIBLE) {
//            RxBus.get().send(Constants.RX_BUS_EVENT_SHOW_NAVI, VISIBLE);
            Handler handler = getHandler();
            if(handler != null) {
                handler.removeCallbacks(mNavHider);
                handler.postDelayed(mNavHider, 3000);
            }
        }
    }

    void hideNavi() {
        if (getDisplay() != null
                && (getDisplay().getRotation() == Surface.ROTATION_270 || getDisplay().getRotation() == Surface.ROTATION_90)) {

            int uiOptions = GlobalApplication.getDefualtUiOptions();
            uiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            uiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            uiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            setSystemUiVisibility(uiOptions);

//            RxBus.get().send(Constants.RX_BUS_EVENT_SHOW_NAVI, GONE);
        }
    }
}
