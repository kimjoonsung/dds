package com.uxstory.dds.speedplus.upnp;

import android.net.Uri;

import com.uxstory.dds.speedplus.BuildConfig;
import com.uxstory.dds.speedplus.config.Constants;
import com.uxstory.dds.speedplus.util.LogU;

import org.cybergarage.http.HTTPHeader;
import org.cybergarage.upnp.device.NotifyListener;
import org.cybergarage.upnp.device.SearchResponseListener;
import org.cybergarage.upnp.event.EventListener;
import org.cybergarage.upnp.ssdp.SSDPPacket;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ControlPointListener implements NotifyListener, EventListener, SearchResponseListener {

    final static String TAG = ControlPointListener.class.getSimpleName();

    private DeviceBinder deviceBinder;
    private HashSet<String> serialSet = new HashSet<>();

    public interface DeviceBinder {
        void onAdd(String ip, int port, String serial);
        void onRemove(String serial);
    }

    public void setDeviceBinder(DeviceBinder binder) {
        deviceBinder = binder;
        serialSet.clear();
    }

    private void logSSDPPacket(SSDPPacket ssdpPacket) {
        LogU.d(TAG, "logSSDPPacket()------------------------");
        LogU.d(TAG, ssdpPacket.toString());
    }

    private String getHeaderValue(SSDPPacket ssdpPacket, String head) {
        return HTTPHeader.getValue(ssdpPacket.getData(), head);
    }

    private synchronized void processPacket(SSDPPacket ssdpPacket) {
        logSSDPPacket(ssdpPacket);

        String nt = ssdpPacket.getNT();
        LogU.d(TAG, "uPNP nt = " + nt);

        String pattern = "^"+Constants.DDS_URI_PREFIX+BuildConfig.DDS_SERIAL_PATTERN+"$";
        LogU.d(TAG, "uPNP pattern = " + pattern);

        if(Pattern.matches(pattern, nt)) {
            LogU.d(TAG, "uPNP found device.");
            if(deviceBinder != null) {
                Uri uri = Uri.parse(nt);
                String serial = uri.getLastPathSegment();
                deviceBinder.onAdd(ssdpPacket.getRemoteAddress(), Constants.DEVICE_PORT, serial);
            }
        }
    }

    @Override
    public void deviceSearchResponseReceived(SSDPPacket ssdpPacket) {
        LogU.d(TAG, "deviceSearchResponseReceived()");
        processPacket(ssdpPacket);
    }

    @Override
    public void deviceNotifyReceived(SSDPPacket ssdpPacket) {
        LogU.d(TAG, "deviceNotifyReceived()");
        processPacket(ssdpPacket);
    }

    @Override
    public void eventNotifyReceived(String uuid, long seq, String varName, String value) {
    }
}
